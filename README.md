# Household Budget Survey

# Architecture

- The architecture of this app uses a `feature-based structure`. Code that is shared among features is put in the `shared` folder.
- The file types are common within this code base:
  - `DAOs`: Data access objects, which define methods to directly interact with the drift database.
  - `DTOs`: Designed to transfer data betweeen different parts of the application. Consists out of simple properties (getters, setters, serializers, mappers, etc), does NOT contain business logic (like a Model would).
  - `Repositories`: Classes that provide an abstraction layer between the app's data sources (cache, database, api, etc) and the rest of the app.
  - `Screens`: Widgets that represent full-screen views in the app.
  - `Widgets`: Reusable components that are used to build the UI in the app.
  - `Notifiers`: Notifiers are used for managing state in the app, and can notify any widgets that depend on their state when it changes.
  - `Utils`: Classes that contain (small) generic helper functions or other utility methods that can be used throughout the app. For example capitalizing Strings.
  - `Services`: Classes that provide functionality or perform tasks for the app, such as data processing, these tasks are typically more complex than utils, and are less generic than utils. For example, string matching.
  - `Enums`: Classes that represent a fixed set of values.

- For persistent on-device storage there are two methods used:

  - For simple key-value storage the `shared_preferences` package is used.
  - More complex data is stored in a SQLite database, which is accesed through object relational mapping (ORM) package `drift`.

- File names end with a postfix, which indicate what filetype they are. The reason for this postfix is such that not all files need to be put into folders. Especially, for example when there is just one notifier in a certain component, it adds effort if you have to click through folders for just a single file.

- Most SQL tables include an auto incremented ID, for typical reasons, however important user created data is identified by a uuid, this reduces the chance of irreplaceable data loss in case of mistakes happening on the server side.

- For depenecy injection, two libraries are used: get_it and provider.
  - get_it is used to register global dependencies
  - provider is used to inject dependencies at specific points in the widget tree. This has some potential benefits over using riverpod:
    + You're able to easily align the life cycle of notifiers to the life cycle of a widget.
    + This also allows for more flexibility when it comes to composing notifiers that depend on other notifiers
    + Reusing the same widget to create multiple instances of a widget + notifier is (subjectively) easier.
    + Everything is 'less global', t his can be helpful when turning existing widgets into shareable plugins to be used across different apps.
    - Having said that Riverpod is still a viable alternative, it can do most of the above (even if it is more work sometimes) and it also comes with some benefits over provider.
  
    - Creating a plugin probably requires more work either way, as you're still dependant on things such as translations. As such, it may not be that helpful to to have everything non-global.
  
  An advantage of this approach is that you're able to align the life cycle of notifiers to the life cycle of a widget (e.g. receiptnotifier and filternotifier). This also impacts how you build notifiers that may be dependent on each other. Another Also, from a subjective standpoint, the process of creating multiple widget and notifier, for example in a list of widgets, is simpler. And finally, you're able to use regular Flutter widgets as opposed to riverpod widgets. Having said that, riverpod is also a viable option with some benefits worth refactoring to.
  
- State management, we mostly use ChangeNotifier, as this is approach is simpler/cleaner than most alternatives, and appears to be sufficient.


# Adding a new country, instructions:

To add a new country to the app you'll have to add a .csv file to these folders:

assets/coicop_hierarchy/
assets/coicop_search/
assets/shops/
assets/translations/

And you'll have to modify configuration.csv

Please be aware that:

- The .csv files use a semicolon ";" as seperator, this helps to avoid collisions with the "," which appears more often in text.
- The size of translations matter, if they're too long, they may not properly fit in the app's user interface.
- Make sure that the order of columns in your .csv file is the same as the order of the other files
- Not all special characters are supported. Flutter uses UTF-8 encoding. It is advices to use Google Sheets to export the .csv file, as to avoid issues.
- The first row of the .csv file is used as an header

# Syncing mechanism

For a description of the syncing mechanism see [sync_mechanism.md](docs/sync_mechanism.md)

# Refactor improvements

- Architecture is much more consistent
- Applied some SOLID princples to synchronization mechanism


# Discussion points:

- Maybe still too much is done in DTO's, for example TreeNodeDto and ReceiptManualDto (I do limit it to returning aggregated data, I don't do any setting, validating or other complex logic)