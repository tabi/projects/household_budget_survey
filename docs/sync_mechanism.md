# Synchronization Method Overview

This document describes a synchronization method used to keep data consistent across multiple devices which may periodically be offline.

## Entity Definitions

* `User Account`: Represents a set of login credentials that can be shared by multiple individuals.
* `Device`: Each user account can be associated with multiple devices, such as phones and tablets.
* `Backend`: Acts as the central repository for all data. Devices synchronize with this backend server to update and retrieve data.
* `Shared Item`: An object that requires synchronization across devices.


## Data Structure

### Device Properties

* `deviceSyncOrder`: Starts at 0. Increments with each creation or update of a data item. It tracks changes made by the user, assisting the backend server in determining if updates are needed.
* `backendSyncOrder`: Also starts at 0. It updates with each change on the backend server, helping devices decide when to synchronize.

### Backend Properties

* `deviceSyncOrder (for each device)`: Begins at 0, tracking the last successful synchronization for each device.
* `backendSyncOrder`: Starts at 0 and increases with every creation or update of data items on the backend.

&nbsp;
> **Note:** Thus, each time a new device is added to the the User Account group, a new set of the following properties is created:
>* device.deviceSyncOrder 
>* device.backendSyncOrder 
>* backend.deviceSyncOrder 

### Shared Item Properties

* `syncOrder`: 
    * **On the device**: this value reflects the `deviceSyncOrder` at the time of the item's last update. 
    * **On the backend**: this value reflects the `backendSyncOrder` at the time of the item's last update
* `createdOn`: Set by a user/device, to the current time, when an item is created.
* `groupUuid`: Whenever an object is updated, a new object is created with updated values, but with the same `groupUuid`.
* `isDeleted`: Indicates if the item has been deleted. 
* `deviceUuid` (only on backend): A unique identifier for the device on which the change was made.


1) Shared items are synchronized in a singular transaction, to avoid leaving data in a partially updated state. As such, each shared item has only one instance of the above properties, regardless of whether this shared item is eventually stored in seperate tables/rows.

2) Because we want to keep track of shared item permutations, we don't update existing properties, instead we set isDeleted to true and we add a new item with the updated properties.

## Synchronization Process

### Pushing Updates (Device to Backend):

* `Device`:
    * Asks the backend for the `backend.deviceSyncOrder` that is related to the device.
    * Identifies items whose `syncOrder` exceed the `backend.deviceSyncOrder`.
    * Pushes these items to the backend in ascending `syncOrder`.
* `Backend`: 
    * Updates `backend.deviceSyncOrder` to the incoming item's `syncOrder`. 
    * If the item is new or its createdOn is more recent, `backend.backendSyncOrder` increments, and the item is updated or added to the database. 
    * Updates each synchronized shared item's `syncOrder` to the current `backend.backendSyncOrder` and records the originating `deviceUuid`.


### Pulling Updates (Backend to Device):

* `Device`: 
    * Sends a pull request, including its `device.backendSyncOrder` and `device.UUID`.
* `Backend`: 
    * Identifies items which have a `syncOrder` greater than the device's `device.backendSyncOrder` and which have a **different** `device.UUID`. 
    * Sends these items to the device in ascending order of syncOrder.
* `Device`: 
    * Updates its `device.backendSyncOrder` to match the incoming items. 
    * If an item is new or if it's `createdOn` value is more recent, it upserts the data to the local database.

