# Login

```mermaid
sequenceDiagram
    participant Client
    participant Server
    participant Auth
    participant Phoenix

    Client->>Server: login(username, deviceInfo)
    Server ->>Auth: 1: login()
    alt is success
        Auth->>Server: Send back auth JWT tokens
        Note over Server: Store JWT tokens in local db
        Server->>Phoenix: getUserInfo()
        Phoenix->>Server: Return UserInfo (Name, SurveyDate)
        Note over Server: Store userInfo in local db
        Server->>Client: send back budgetServer JWT token + Name + SurveyDate
        Client->>Server: send selected SurveyDate
        Note over Server: Store surveyDate in local db
    else incorrect credentials
        Auth->>Server: Send back 401
        Server->>Client: send back 401
     else server issue
        Auth->>Server: Send back 500
        Server->>Client: send back 500
    end
```

1: If not already available due to a prior login with another device. If already available, send back known info.
