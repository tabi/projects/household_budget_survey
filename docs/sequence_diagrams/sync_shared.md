# Syncing Shared Resources

This document illustrates how receipts are pushed and pulled to and from the backend. <br/>
Receipts are used as an example, but this method can be applied to any type of shared resource. <br/>
A shared resource refers to any data that requires synchronization across various devices belonging to a single user. <br/>

**Key Considerations**

1. When syncing shared resources, it is important that related/dependant resources are synced in a singular transaction (e.g. receipt + products).<br/>
2. Because order of synchronization is important, any data type that shares a set of syncOrder counters, needs to be synced sequentially. For example, if syncOrder is used for receipts and logs, then they would both need to be synced at the same time. If that is not desirably, then seperate counters need to be kept track of, in which case you might want to replace `syncOrder` with something like `receiptSyncOrder`.
3. To follow the steps below, it might help to remember that:
* `backendSyncOrder` is used to track whether devices need to be updated (if the backendSyncOrder is greater than what the device knows it to be)
* `deviceSyncOrder` is used to track whether the backend needs to be updated (if the deviceSyncOrder is greater than what the backend knows it to be)


    These variables exist both on the backend as well as on each device, therefor the following variables exist: <br/>

    * `backend.backendSyncOrder`
    * `backend.deviceSyncOrder`
    * `device.backendSyncOrder`
    * `device.deviceSyncOrder`

4. This method accounts for creates, updates, and reads. But not for deletes. If objects ought to be deletable, do so by setting a delete=true flag.

## Pushing Updates (Client to Backend):


```mermaid
sequenceDiagram
    participant Client
    participant Server

    Note left of Client: deviceUuid = getDeviceUuid()<br/>authToken = getAutToken()
    Client->>Server: getBackendDeviceSyncOrder(deviceUuid, authToken)
    Note right of Server: Routing<br/>Authentication<br/>Authorization<br/>getDeviceSyncOrder(deviceUuid)
    Server->>Client: return backend.deviceSyncOrder
    Note left of Client: 1: unsyncedReceipts = getUnsycedReceipts(backend.deviceSyncOrder)
    Client->>Server: 2: unsyncedReceipts.forEach(push_receipts)
    Note right of Server: Routing<br/>Authentication<br/>Authorization<br/>3: upsertReceipt(receipt)
```


1: The client checks, which receipts have a syncOrder that's greater than the backendDeviceSyncOrder

2: It updates the receipts in ascending syncOrder

3: upsertReceipt(receipt): <br/>

If the item is new OR its createdOn is more recent: 
* backend.backendSyncOrder += 1
* backend.deviceSyncOrder = receipt.syncOrder
* add receipt to database, with:
    - receipt.syncOrder = backendSyncOrder
    - receipt.device = sender.deviceUuid


## Pulling Updates (Backend to Client):


```mermaid
sequenceDiagram
    participant Client
    participant Server

    Note left of Client: authToken = getAutToken()<br/>device.backendSyncOrder = getBackendSyncOrder() 
    Client->>Server: getReceipts(authToken, device.backendSyncOrder)
    Note right of Server: Routing<br/>Authentication<br/>Authorization<br/>backend.backendSyncOrder = getBackendSyncOrder(authToken.username)<br/>1: receipts = getReceipts(device.backendSyncOrder, backend.backendSyncOrder, deviceUuid, username)
    Server->>Client: 2: return <list> [receipts]
    Note left of Client: 3: upsertReceipts(receipts)
    
```

1: Identifies items which have a syncOrder greater than the device's device.backendSyncOrder and which have a **different** device.UUID. <br/>
2: Sends the list back in ascending syncOrder. <br/>
3: upsertReceipt(receipt):


* If item is new or if createdOn value is more recent:
    - add receipt to database, with:
        - receipt.syncOrder = device.deviceSyncOrder (without updating the syncOrder!)

* In both cases:
    - device.backendSyncOrder = receipt.syncOrder




 
