# Syncing Unshared Resources

Unshared resources are device specific data that do not need to be shared between devices of the same user.

There are 2 types of unshared data:
1. Those that are synced during the login process. As such, we can assume that they've been synced. For example, AccessTokens and DeviceInfo.  
2. Items that need continuesly need to be checked, to see if there are items that need to be synced. For example, ErrorLogs and ParaData.
    - These should be all synced when the final data is send, and then also stopped
    - They should be all synnced when:
        - lastSyncTime is longer than 5 minutes ago AND
        -  the user navigates in the app?


```mermaid
sequenceDiagram
    participant Client
    participant Server

    Note left of Client: unsharedItems = getUnsyncedItems()
    Client->>Server: syncItems(unsharedItems)
    Server->>Client: return statusCodes
    Note left of Client: Update sync status based on statusCode
```
