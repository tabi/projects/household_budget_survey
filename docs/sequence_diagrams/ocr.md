# OCR

### doOcr

```mermaid
sequenceDiagram
    participant Client
    participant Server
    participant FileDrive
    participant MessageBus
    participant OCR_microservice
    participant COICOP_microservice
  
    Note over Server: start OcrResult Consumer
    Note over OCR_microservice: start OcrRequest Consumer
    Note over Client: photo, uuid = takePhoto()<br/>rotadedPhoto = rotatePhoto(photo)
    Client->>Server: doOcr(rotadedPhoto, uuid)
    Server->>FileDrive: savePhoto(rotatedPhoto, uuid)
    Server->>MessageBus: doOcr(uuid)
    Server->>Client: return successCode
    MessageBus->>OCR_microservice: OcrRequestConsumer.consume(uuid)
    FileDrive->>OCR_microservice: loadPhoto(uuid)
    OCR_microservice->>MessageBus: returnOCRResult(ocrResult)
    MessageBus->>Server: 1: OcrResultConsumer.consume(ocrResult)
    Note over Server: Store the OCR result in db<br/>Send OCRResult to subcriber
```

1: This consumer/background service should also let getOcr calls subscribe to ocrResult updates (e.g. observer patten).


### getOcrResult

```mermaid
sequenceDiagram
    participant Client
    participant Server

    Client->>Server: getOcrResult(uuid)
    Note over Server: set stopwatch to timeout after 10 seconds
    Note over Server: register resultListener 
    Note over Server: check if database has result
    alt ready
        Server->>Client: Send back ocrResult
    else notReady
        Server->>Client: Send back notReady
    end

    
```