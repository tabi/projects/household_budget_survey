import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:get_it/get_it.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'dependency_registrar.dart';
import 'infrastructure/notifiers/app_tour_notifier.dart';
import 'infrastructure/notifiers/auth_notifier.dart';
import 'infrastructure/notifiers/configuration_notifier.dart';
import 'infrastructure/notifiers/filter_notifier.dart';
import 'infrastructure/notifiers/login_notifier.dart';
import 'infrastructure/notifiers/onboarding_notifier.dart';
import 'infrastructure/notifiers/overview_notifiers/overview_notifier.dart';
import 'infrastructure/notifiers/overview_notifiers/surveylist_notifier.dart';
import 'infrastructure/notifiers/receipt_list_notifier.dart';
import 'infrastructure/notifiers/settings_notifier.dart';
import 'infrastructure/notifiers/translation_notifier.dart';
import 'infrastructure/utils/color_pallette.dart';
import 'infrastructure/utils/ui_scalers.dart';
import 'presentation/app_tour/app_tour.dart';
import 'presentation/category_selection/category_screen.dart';
import 'presentation/category_selection/widgets/search_product_screen.dart';
import 'presentation/loading/loading_screen.dart';
import 'presentation/receipt_edit/receipt_edit_screen.dart';
import 'presentation/receipt_edit/widgets/search_product_screen.dart';
import 'presentation/receipt_edit/widgets/search_store_screen.dart';
import 'presentation/receipt_photo/photo_instruction_screen.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  registerDependencies();
  initializeDateFormatting().then((_) => runApp(const RootWidget()));
}

class RootWidget extends StatelessWidget {
  const RootWidget();

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => GetIt.I<ConfigurationNotifier>(), lazy: false),
        ChangeNotifierProvider(create: (_) => GetIt.I<TranslationNotifier>(), lazy: false),
        ChangeNotifierProvider(create: (_) => GetIt.I<SettingsNotifier>(), lazy: false),
        ChangeNotifierProvider(create: (_) => GetIt.I<OverviewNotifier>(), lazy: false),
        ChangeNotifierProvider(create: (_) => GetIt.I<SurveyListNotifier>(), lazy: false),
        ChangeNotifierProvider(create: (_) => GetIt.I<LoginNotifier>(), lazy: false),
        ChangeNotifierProvider(create: (_) => GetIt.I<AppTourNotifier>(), lazy: false),
        ChangeNotifierProvider(create: (_) => GetIt.I<FilterNotifier>(), lazy: false),
        ChangeNotifierProvider(create: (_) => GetIt.I<ReceiptListNotifier>(), lazy: false),
        ChangeNotifierProvider(create: (_) => GetIt.I<OnboardingNotifier>(), lazy: false),
        ChangeNotifierProvider(create: (_) => GetIt.I<AuthNotifier>(), lazy: false),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        localizationsDelegates: const <LocalizationsDelegate>[
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
        ],
        supportedLocales: GetIt.I<TranslationNotifier>().locales,
        routes: {
          'addManualReceipt': (context) => const ReceiptEditScreen(),
          'addPhotoReceipt': (context) => const PhotoInstructionScreen(),
          'searchStoreScreen': (context) => const SearchStoreScreen(),
          'searchProductScreen': (context) => const SearchProductScreen(),
          'searchCategoryScreen': (context) => const SearchCategoryScreen(),
          'selectCategoryScreen': (context) => const CategorySelectionScreen(),
        },
        home: Scaffold(
          backgroundColor: ColorPalette.backgroundWhite,
          body: FutureBuilder(
            future: SharedPreferences.getInstance(),
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              initUIScalers(context);
              if (snapshot.hasData) {
                var isLoggedIn =
                    (snapshot.data.getBool('isLoggedIn') == null) ? false : snapshot.data.getBool('isLoggedIn');
                if (isLoggedIn) {
                  return LoadingScreen();
                } else {
                  return AppTourScreen();
                }
              } else {
                return Center(
                  child: CircularProgressIndicator(
                    color: ColorPalette.brand,
                  ),
                );
              }
            },
          ),
        ),
      ),
    );
  }
}
