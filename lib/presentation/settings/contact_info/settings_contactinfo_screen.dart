import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../infrastructure/notifiers/login_notifier.dart';
import '../../../infrastructure/notifiers/translation_notifier.dart';
import '../../../infrastructure/utils/color_pallette.dart';
import '../../../infrastructure/utils/text_style_palette.dart';

class ContactInfoScreen extends StatelessWidget {
  const ContactInfoScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final translationNotifier = context.watch<TranslationNotifier>();
    final loginNotifier = context.watch<LoginNotifier>();

    return Scaffold(
      backgroundColor: ColorPalette.backgroundWhite,
      appBar: AppBar(
        bottom: PreferredSize(
          child: Container(
            color: ColorPalette.grey200,
            height: 1.0,
          ),
          preferredSize: Size.fromHeight(4.0),
        ),
        title: Text(
          translationNotifier.getText('contact'),
          style: TextStylePalette.brandSoho16w500,
        ),
        scrolledUnderElevation: 0,
        backgroundColor: ColorPalette.backgroundWhite,
        centerTitle: true,
        leading: IconButton(
          icon: Icon(
            Icons.keyboard_arrow_left_rounded,
            color: ColorPalette.brand,
          ),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  translationNotifier.getText('settingsContactOne'),
                  style: TextStylePalette.primaryAkko16w400,
                ),
                Container(
                  height: 16,
                ),
                Text(
                  translationNotifier.getText('settingsContactTwo'),
                  style: TextStylePalette.primaryAkko16w400,
                ),
                Container(
                  height: 24,
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        padding: EdgeInsets.all(12),
                        child: Icon(
                          Icons.phone_rounded,
                          color: Colors.white,
                        ),
                        decoration: BoxDecoration(color: ColorPalette.brand, borderRadius: BorderRadius.circular(4)),
                      ),
                      Container(
                        width: 16,
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            translationNotifier.getText('callus'),
                            style: TextStylePalette.primaryAkko16w400,
                          ),
                          RichText(
                            text: TextSpan(
                              text: translationNotifier.getText('telephone'),
                              style: TextStylePalette.aquaAkko14w400underlined,
                              recognizer: TapGestureRecognizer()
                                ..onTap = () {
                                  loginNotifier.launchPhone();
                                },
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
                Divider(),
                Padding(
                  padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        padding: EdgeInsets.all(12),
                        child: Icon(
                          Icons.mail_rounded,
                          color: Colors.white,
                        ),
                        decoration: BoxDecoration(color: ColorPalette.brand, borderRadius: BorderRadius.circular(4)),
                      ),
                      Container(
                        width: 16,
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            translationNotifier.getText('sendmail'),
                            style: TextStylePalette.primaryAkko16w400,
                          ),
                          RichText(
                            text: TextSpan(
                              text: translationNotifier.getText('mail'),
                              style: TextStylePalette.aquaAkko14w400underlined,
                              recognizer: TapGestureRecognizer()
                                ..onTap = () {
                                  loginNotifier.launchMail();
                                },
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                ),
                Divider(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
