import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../infrastructure/notifiers/translation_notifier.dart';
import '../../../infrastructure/utils/color_pallette.dart';
import '../../../infrastructure/utils/text_style_palette.dart';

class NotificationsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final translationNotifier = context.watch<TranslationNotifier>();
    return Scaffold(
      backgroundColor: ColorPalette.backgroundOffWhite,
      appBar: AppBar(
        bottom: PreferredSize(
          child: Container(
            color: ColorPalette.grey200,
            height: 1.0,
          ),
          preferredSize: Size.fromHeight(4.0),
        ),
        title: Text(
          translationNotifier.getText('nofitications'),
          style: TextStylePalette.brandSoho16w500,
        ),
        scrolledUnderElevation: 0,
        backgroundColor: ColorPalette.backgroundWhite,
        centerTitle: true,
        leading: IconButton(
          icon: Icon(
            Icons.keyboard_arrow_left_rounded,
            color: ColorPalette.brand,
          ),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Icon(
              Icons.construction_rounded,
              size: 64,
              color: ColorPalette.grey300,
            ),
            Text(
              translationNotifier.getText('underconstruction'),
              style: TextStylePalette.primaryAkko16w400,
            )
          ],
        ),
      ),
    );
  }
}
