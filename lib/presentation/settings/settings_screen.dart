import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../infrastructure/notifiers/translation_notifier.dart';
import '../../infrastructure/utils/color_pallette.dart';
import '../../infrastructure/utils/text_style_palette.dart';
import 'sub_menus/other_sub_menu.dart';
import 'sub_menus/service_sub_menu.dart';
import 'sub_menus/settings_logo.dart';
import 'sub_menus/settings_sub_menu.dart';

class SettingsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final translationNotifier = context.watch<TranslationNotifier>();

    return Scaffold(
      appBar: AppBar(
        scrolledUnderElevation: 0.0,
        centerTitle: false,
        backgroundColor: ColorPalette.backgroundWhite,
        title: Text(
          translationNotifier.getText('more'),
          style: TextStylePalette.brandSoho26w600,
        ),
      ),
      backgroundColor: ColorPalette.backgroundOffWhite,
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SettingsSubMenu(),
                    Container(
                      height: 8,
                      color: ColorPalette.backgroundOffWhite,
                    ),
                    ServiceSubMenu(),
                    Container(
                      height: 8,
                      color: ColorPalette.backgroundOffWhite,
                    ),
                    OtherSubMenu(),
                    SettingsLogo(),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
