import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../../infrastructure/notifiers/translation_notifier.dart';
import '../../../../infrastructure/utils/color_pallette.dart';
import '../../../infrastructure/utils/text_style_palette.dart';
import '../language_screen/settings_language_screen.dart';
import 'settings_tile.dart';

class SettingsSubMenu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final translationNotifier = context.watch<TranslationNotifier>();

    return Container(
      padding: EdgeInsets.only(left: 16, right: 16),
      color: ColorPalette.backgroundWhite,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 16.0),
            child: Text(
              translationNotifier.getText('settings'),
              style: TextStylePalette.primarySoho16w600,
            ),
          ),
          SettingsTile(
            buttonText: translationNotifier.getText('language'),
            trailingIcon: Icons.keyboard_arrow_right_rounded,
            navigationDestination: 'TaalPage',
            onTap: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) {
                  return SettingsLanguageScreen();
                },
              ),
            ),
          ),
          Divider(
            height: 1,
            color: ColorPalette.backgroundOffWhite,
          ),
          // SettingsTile(
          //   buttonText: translationNotifier.getText('nofitications'),
          //   trailingIcon: Icons.keyboard_arrow_right_rounded,
          //   navigationDestination: 'MeldingenPage',
          //   onTap: () => Navigator.push(
          //     context,
          //     MaterialPageRoute(
          //       builder: (context) {
          //         return NotificationsScreen();
          //       },
          //     ),
          //   ),
          // ),
        ],
      ),
    );
  }
}
