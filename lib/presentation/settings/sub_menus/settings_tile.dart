import 'package:flutter/material.dart';

import '../../../infrastructure/utils/color_pallette.dart';
import '../../../infrastructure/utils/text_style_palette.dart';

class SettingsTile extends StatelessWidget {
  final String buttonText;
  final String navigationDestination;
  final IconData? trailingIcon;
  final Function() onTap;

  SettingsTile({required this.buttonText, required this.navigationDestination, this.trailingIcon, required this.onTap});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        InkWell(
            child: ListTile(
              contentPadding: EdgeInsets.all(0),
              title: Text(buttonText, style: TextStylePalette.primaryAkko16w500),
              trailing: Icon(
                trailingIcon,
                color: ColorPalette.textPrimary,
              ),
            ),
            onTap: () => onTap()),
      ],
    );
  }
}
