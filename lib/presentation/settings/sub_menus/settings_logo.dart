import 'package:flutter/material.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:provider/provider.dart';

import '../../../../infrastructure/notifiers/translation_notifier.dart';
import '../../../infrastructure/utils/text_style_palette.dart';

class SettingsLogo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final translationNotifier = context.watch<TranslationNotifier>();

    double sideWidth = MediaQuery.of(context).size.width / 4;

    return FutureBuilder<PackageInfo>(
      future: PackageInfo.fromPlatform(),
      builder: (BuildContext context, AsyncSnapshot<PackageInfo> snapshot) {
        if (!snapshot.hasData) {
          return const CircularProgressIndicator();
        } else if (snapshot.hasError) {
          return Container();
        } else {
          PackageInfo packageInfo = snapshot.data!;
          return Container(
            color: Color(0xFFFFFFFF),
            padding: EdgeInsets.only(left: sideWidth, right: sideWidth, top: 16, bottom: 16),
            width: double.infinity,
            child: Column(
              children: [
                Image.asset(
                  'assets/images/cbs_logo.png',
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text(
                    translationNotifier.getText('appVersion') + ' ' + packageInfo.version,
                    style: TextStylePalette.primaryAkko14w400,
                  ),
                )
              ],
            ),
          );
        }
      },
    );
  }
}
