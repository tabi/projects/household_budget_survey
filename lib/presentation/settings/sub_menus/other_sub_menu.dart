import 'package:drift_db_viewer/drift_db_viewer.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:provider/provider.dart';

import '../../../../infrastructure/notifiers/translation_notifier.dart';
import '../../../../infrastructure/utils/color_pallette.dart';
import '../../../infrastructure/database/database.dart';
import '../../../infrastructure/notifiers/settings_notifier.dart';
import '../../../infrastructure/utils/text_style_palette.dart';
import 'settings_tile.dart';

class OtherSubMenu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final translationNotifier = context.watch<TranslationNotifier>();
    final settingsNotifier = context.watch<SettingsNotifier>();
    return Container(
      padding: EdgeInsets.only(left: 16, right: 16),
      color: ColorPalette.backgroundWhite,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 16.0),
            child: Text(
              translationNotifier.getText('other'),
              style: TextStylePalette.primarySoho16w600,
            ),
          ),
          kDebugMode
              ? SettingsTile(
                  buttonText: 'Debug Database',
                  trailingIcon: Icons.keyboard_arrow_right_rounded,
                  navigationDestination: 'PrivacyPage',
                  onTap: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) => DriftDbViewer(
                          GetIt.I<Database>(),
                        ),
                      ),
                    );
                  },
                )
              : Container(),
          kDebugMode
              ? Divider(
                  height: 1,
                  color: ColorPalette.backgroundOffWhite,
                )
              : Container(),
          SettingsTile(
              buttonText: translationNotifier.getText('privacyagreement'),
              trailingIcon: Icons.link_rounded,
              navigationDestination: 'PrivacyPage',
              onTap: () => settingsNotifier.launchPrivacy()),
          Divider(
            height: 1,
            color: ColorPalette.backgroundOffWhite,
          ),
          // SettingsTile(
          //     buttonText: translationNotifier.getText('useragreement'),
          //     trailingIcon: Icons.keyboard_arrow_right_rounded,
          //     navigationDestination: 'UserAgreementPage',
          //     onTap: () {}),
          // Divider(
          //   height: 1,
          //   color: ColorPalette.backgroundOffWhite,
          // ),
          // SettingsTile(
          //   buttonText: translationNotifier.getText('rateapp'),
          //   trailingIcon: Icons.keyboard_arrow_right_rounded,
          //   navigationDestination: 'RatingPage',
          //   onTap: () => Navigator.push(
          //     context,
          //     MaterialPageRoute(
          //       builder: (context) {
          //         return ReviewScreen();
          //       },
          //     ),
          //   ),
          // ),
          // Divider(
          //   height: 1,
          //   color: ColorPalette.backgroundOffWhite,
          // ),
          // SettingsTile(
          //     buttonText: translationNotifier.getText('apptour'),
          //     trailingIcon: Icons.keyboard_arrow_right_rounded,
          //     navigationDestination: 'TourPage',
          //     onTap: () {}),
          // Divider(
          //   height: 1,
          //   color: ColorPalette.backgroundOffWhite,
          // ),
          // SettingsTile(
          //     buttonText: translationNotifier.getText('logout'), navigationDestination: 'LogOut', onTap: () {}),
          // Divider(
          //   height: 1,
          //   color: ColorPalette.backgroundOffWhite,
          // ),
        ],
      ),
    );
  }
}
