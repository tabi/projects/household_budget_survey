import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../../infrastructure/notifiers/translation_notifier.dart';
import '../../../../infrastructure/utils/color_pallette.dart';
import '../../../infrastructure/notifiers/settings_notifier.dart';
import '../../../infrastructure/utils/text_style_palette.dart';
import '../contact_info/settings_contactinfo_screen.dart';
import 'settings_tile.dart';

class ServiceSubMenu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final translationNotifier = context.watch<TranslationNotifier>();

    final settingsNotifier = context.watch<SettingsNotifier>();
    return Container(
      padding: EdgeInsets.only(left: 16, right: 16),
      color: Color(0xFFFFFFFF),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 16.0),
            child: Text(
              translationNotifier.getText('service'),
              style: TextStylePalette.primarySoho16w600,
            ),
          ),
          SettingsTile(
            buttonText: translationNotifier.getText('contact'),
            trailingIcon: Icons.keyboard_arrow_right_rounded,
            navigationDestination: 'ContactPage',
            onTap: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) {
                  return ContactInfoScreen();
                },
              ),
            ),
          ),
          Divider(
            height: 1,
            color: ColorPalette.backgroundOffWhite,
          ),
          SettingsTile(
              buttonText: translationNotifier.getText('faq'),
              trailingIcon: Icons.link_rounded,
              navigationDestination: 'FAQPage',
              onTap: () => settingsNotifier.launchFAQ()),
        ],
      ),
    );
  }
}
