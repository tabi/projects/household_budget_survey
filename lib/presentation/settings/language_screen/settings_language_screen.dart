import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../infrastructure/notifiers/settings_notifier.dart';
import '../../../infrastructure/notifiers/translation_notifier.dart';
import '../../../infrastructure/utils/color_pallette.dart';
import '../../../infrastructure/utils/text_style_palette.dart';

class SettingsLanguageScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final settingsNotifier = context.watch<SettingsNotifier>();
    final translationNotifier = context.watch<TranslationNotifier>();
    return Scaffold(
      backgroundColor: ColorPalette.backgroundWhite,
      appBar: AppBar(
        bottom: PreferredSize(
          child: Container(
            color: ColorPalette.grey200,
            height: 1.0,
          ),
          preferredSize: Size.fromHeight(4.0),
        ),
        title: Text(
          translationNotifier.getText('language'),
          style: TextStylePalette.brandSoho16w500,
        ),
        scrolledUnderElevation: 0,
        backgroundColor: ColorPalette.backgroundWhite,
        centerTitle: true,
        leading: IconButton(
          icon: Icon(
            Icons.keyboard_arrow_left_rounded,
            color: ColorPalette.brand,
          ),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                width: double.infinity,
                padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8),
                child: InkWell(
                    child: ListTile(
                      contentPadding: EdgeInsets.all(0),
                      title: Text(translationNotifier.getText('dutch'), style: TextStylePalette.primaryAkko16w400),
                      trailing: settingsNotifier.selectedLanguage == SupportedLanguages.nl
                          ? Icon(
                              Icons.check_rounded,
                              color: ColorPalette.aqua300,
                            )
                          : null,
                    ),
                    onTap: () {
                      settingsNotifier.selectedLanguage = SupportedLanguages.nl;
                      translationNotifier.setLocale(Locale('nl', 'NL'));
                    }),
              ),
              Divider(
                height: 1,
              ),
              // Container(
              //   padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8),
              //   width: double.infinity,
              //   child: InkWell(
              //       child: ListTile(
              //         contentPadding: EdgeInsets.all(0),
              //         title: Text(translationNotifier.getText('english'), style: TextStylePalette.primaryAkko16w400),
              //         trailing: settingsNotifier.selectedLanguage == SupportedLanguages.en
              //             ? Icon(Icons.check_rounded, color: ColorPalette.aqua300)
              //             : null,
              //       ),
              //       onTap: () {
              //         settingsNotifier.selectedLanguage = SupportedLanguages.en;
              //         translationNotifier.setLocale(Locale('en', 'EN'));
              //       }),
              // ),
              // Divider(
              //   height: 1,
              // ),
            ],
          ),
        ),
      ),
    );
  }
}
