import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';

import '../../infrastructure/notifiers/translation_notifier.dart';
import '../../infrastructure/utils/color_pallette.dart';
import '../../infrastructure/utils/text_style_palette.dart';
import '../../infrastructure/utils/ui_scalers.dart';
import 'camera_screen.dart';

class PhotoInstructionScreen extends StatelessWidget {
  const PhotoInstructionScreen();

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
      statusBarIconBrightness: Brightness.dark,
    ));

    final translationNotifier = context.watch<TranslationNotifier>();

    return SizedBox(
      child: Scaffold(
        backgroundColor: ColorPalette.backgroundWhite,
        appBar: AppBar(
          scrolledUnderElevation: 0,
          leading: IconButton(
            icon: Icon(
              Icons.close_rounded,
              color: ColorPalette.brand,
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          backgroundColor: ColorPalette.backgroundWhite,
        ),
        body: Column(
          children: [
            SvgPicture.asset('assets/images/receipt_scan_example.svg', height: 250 * y),
            SizedBox(height: 16),
            Row(
              children: [
                SizedBox(width: 16),
                Text(
                  translationNotifier.getText('thisIsHowReceiptScanningWorksTitle'),
                  style: TextStylePalette.brandSoho20w600,
                ),
              ],
            ),
            SizedBox(height: 16),
            Expanded(
              child: BulletPointList(
                textKeys: [
                  'pictureAngleBulletPoint',
                  'lightBulletPoint',
                  'oneReceiptPerPhoto',
                ],
              ),
            ),
            ElevatedButton(
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) => CameraWidget()));
              },
              style: ElevatedButton.styleFrom(
                backgroundColor: ColorPalette.aqua200,
                textStyle: TextStylePalette.whiteAkko16w400,
                minimumSize: Size(350, 50),
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
              ),
              child: Text(
                translationNotifier.getText('startScanMessage'),
                style: TextStylePalette.whiteAkko16w400,
              ),
            ),
            SizedBox(height: 35 * y)
          ],
        ),
      ),
    );
  }
}

class BulletPointList extends StatelessWidget {
  final List<String> textKeys;

  BulletPointList({required this.textKeys});

  @override
  Widget build(BuildContext context) {
    final translationNotifier = context.watch<TranslationNotifier>();
    return ListView.builder(
      itemCount: textKeys.length,
      itemBuilder: (context, index) {
        return Padding(
          padding: EdgeInsets.all(10.0 * f),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.only(left: 16, right: 18.0 * x, top: 6.0 * y),
                child: Icon(Icons.fiber_manual_record_rounded, size: 10 * x, color: ColorPalette.textPrimary),
              ),
              Container(
                width: MediaQuery.of(context).size.width * 0.8,
                child: Text(
                  translationNotifier.getText(textKeys[index]),
                  style: TextStylePalette.primaryAkko16w400,
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
