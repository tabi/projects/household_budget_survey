import 'dart:io';
import 'dart:math' as math;

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

import '../../infrastructure/dtos/product_dto.dart';
import '../../infrastructure/dtos/receipt_dto.dart';
import '../../infrastructure/notifiers/translation_notifier.dart';
import '../../infrastructure/utils/color_pallette.dart';
import '../../infrastructure/utils/text_style_palette.dart';
import '../../infrastructure/utils/ui_scalers.dart';

class PictureReviewScreen extends StatefulWidget {
  final XFile imageFile;
  const PictureReviewScreen({super.key, required this.imageFile});

  @override
  State<PictureReviewScreen> createState() => _PictureReviewScreenState();
}

class _PictureReviewScreenState extends State<PictureReviewScreen> {
  double _rotationAngle = 0;
  double _imageScale = 0.95;

  void _rotateImage() {
    setState(() {
      _rotationAngle = (_rotationAngle + 90) % 360;
      _imageScale = (_rotationAngle == 90 || _rotationAngle == 270) ? 0.5 : 0.95;
    });
  }

  @override
  Widget build(BuildContext context) {
    final translationNotifier = context.watch<TranslationNotifier>();
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(
        statusBarColor: Colors.black,
        statusBarIconBrightness: Brightness.dark,
      ),
    );

    return Scaffold(
      body: SafeArea(
        child: Container(
          color: ColorPalette.textBlack,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                padding: EdgeInsets.symmetric(vertical: 16 * y),
                color: ColorPalette.backgroundBlack,
                child: Container(
                  child: Text(
                    translationNotifier.getText('pictureReviewTurnMessage'),
                    textAlign: TextAlign.center,
                    style: TextStylePalette.whiteAkko16w400,
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  color: ColorPalette.backgroundBlack,
                  child: Transform(
                    alignment: Alignment.center,
                    transform: Matrix4.rotationZ(_rotationAngle * math.pi / 180)..scale(_imageScale),
                    child: Image.file(File(widget.imageFile.path)),
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(vertical: 16 * y),
                color: ColorPalette.backgroundBlack,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    IconButton(
                      onPressed: _rotateImage,
                      icon: Icon(
                        Icons.refresh_rounded,
                        color: Colors.white,
                        size: 30 * f,
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        InkWell(
                          onTap: () {
                            Navigator.of(context).pop();
                          },
                          child: Container(
                            width: 48,
                            height: 48,
                            child: Icon(
                              Icons.close_rounded,
                              color: ColorPalette.aqua200,
                              size: 30 * f,
                            ),
                            decoration: BoxDecoration(
                                color: ColorPalette.backgroundWhite,
                                border: Border.all(
                                  color: ColorPalette.backgroundWhite,
                                ),
                                borderRadius: BorderRadius.all(Radius.circular(4))),
                          ),
                        ),
                        SizedBox(width: 16 * x),
                        InkWell(
                          onTap: () {
                            ReceiptDto receiptDto = ReceiptDto.create(boughtOn: DateTime.now());
                            receiptDto.imagePath = 'assets/images/placeholder_receipt.jpg';
                            receiptDto.products.add(ProductDto.create('Trui', null, null, receiptDto.uuid, 1, 35.95));
                            receiptDto.products.add(ProductDto.create('Broek', null, null, receiptDto.uuid, 1, 64.95));
                            receiptDto.products.add(ProductDto.create('Shirt', null, null, receiptDto.uuid, 2, 12.99));
                            Navigator.pushNamed(context, 'selectCategoryScreen', arguments: receiptDto);
                          },
                          child: Container(
                            width: 48,
                            height: 48,
                            child: Icon(
                              Icons.check_rounded,
                              color: ColorPalette.textWhite,
                              size: 30 * f,
                            ),
                            decoration: BoxDecoration(
                                color: ColorPalette.aqua200,
                                border: Border.all(
                                  color: ColorPalette.aqua200,
                                ),
                                borderRadius: BorderRadius.all(Radius.circular(4))),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(width: 52 * x),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
