import 'dart:async';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

import '../../infrastructure/notifiers/translation_notifier.dart';
import '../../infrastructure/utils/color_pallette.dart';
import '../../infrastructure/utils/text_style_palette.dart';
import '../../infrastructure/utils/ui_scalers.dart';
import 'picture_review_screen.dart';

class CameraWidget extends StatefulWidget {
  const CameraWidget({Key? key}) : super(key: key);

  @override
  State<CameraWidget> createState() => _CameraWidgetState();
}

class _CameraWidgetState extends State<CameraWidget> {
  CameraController? controller;
  XFile? pictureFile;
  bool isFlashEnabled = false;
  bool showDarknessWarning = false;
  DateTime? lastProcessedTime;

  @override
  void initState() {
    super.initState();
    setupCamera();
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }

  Future<void> setupCamera() async {
    final cameras = await availableCameras();
    if (cameras.isNotEmpty) {
      setState(() {
        controller = CameraController(
          cameras.first,
          ResolutionPreset.veryHigh,
        );
      });
      await controller?.initialize().then((_) {
        if (!mounted) return;
        setState(() {});
        //controller!.startImageStream(analyzeCameraImage);
      }).catchError(
        (e) {
          if (e is CameraException) {
            switch (e.code) {
              case 'CameraAccessDenied':
                print('Camera Access Error Denied');
                // TODO: Camera Access Denied
                // return Scaffold(
                //   appBar: AppBar(
                //       leading: IconButton(
                //     icon: Icon(
                //       Icons.keyboard_arrow_left_rounded,
                //       size: 32,
                //       color: ColorPalette.brand,
                //     ),
                //     onPressed: () {
                //       Navigator.of(context).pop();
                //     },
                //   )),
                //   body: Padding(
                //     padding: const EdgeInsets.all(16.0),
                //     child: Center(
                //       child: Column(
                //         mainAxisAlignment: MainAxisAlignment.center,
                //         crossAxisAlignment: CrossAxisAlignment.center,
                //         children: [
                //           Icon(
                //             Icons.warning_rounded,
                //             size: 64,
                //             color: ColorPalette.grey300,
                //           ),
                //           Padding(
                //             padding: const EdgeInsets.all(16.0),
                //             child: Text(
                //               translationNotifier.getText('noCameraAccessWarning'),
                //               style: TextStylePalette.primaryAkko16w400,
                //               textAlign: TextAlign.center,
                //             ),
                //           ),
                //           SizedBox(
                //             height: 48,
                //             child: TextButton(
                //               style: TextButton.styleFrom(
                //                 shape: RoundedRectangleBorder(
                //                   borderRadius: BorderRadius.circular(4.0),
                //                 ),
                //                 foregroundColor: Colors.white,
                //                 backgroundColor: ColorPalette.aqua200,
                //                 minimumSize: Size.fromHeight(44),
                //               ),
                //               onPressed: () {
                //                 openAppSettings();
                //               },
                //               child: Text(
                //                 translationNotifier.getText('toAppSettings'),
                //                 style: TextStylePalette.whiteAkko16w400,
                //               ),
                //             ),
                //           ),
                //         ],
                //       ),
                //     ),
                //   ),
                // );
                break;
              default:
                print('Unknown error');
                // TODO: implement other error
                break;
            }
          }
        },
      );
    }
  }

  Future<void> analyzeCameraImage(CameraImage image) async {
    final currentTime = DateTime.now();
    if (lastProcessedTime == null || currentTime.difference(lastProcessedTime!).inMilliseconds >= 1000) {
      lastProcessedTime = currentTime;
      final bytes = image.planes.first.bytes;
      final darkness = getImageDarkness(bytes, image.width, image.height);
      print(darkness);

      setState(() {
        showDarknessWarning = darkness < 35; //iPhone gaat niet lager dan 100
      });
    }
  }

  double getImageDarkness(Uint8List bytes, int width, int height) {
    int sum = 0;
    for (int i = 0; i < bytes.length; i++) {
      sum += bytes[i];
    }
    final int pixelCount = width * height;
    final double averageBrightness = sum / pixelCount;
    return (averageBrightness / 255) * 100;
  }

  Future<void> takePicture() async {
    if (controller != null) {
      try {
        pictureFile = await controller!.takePicture();
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => PictureReviewScreen(imageFile: pictureFile!)),
        );
      } catch (e) {
        //To Implement
      }
    }
  }

  void toggleFlash() {
    setState(() {
      isFlashEnabled = !isFlashEnabled;
      controller?.setFlashMode(isFlashEnabled ? FlashMode.torch : FlashMode.off);
    });
  }

  @override
  Widget build(BuildContext context) {
    final translationNotifier = context.watch<TranslationNotifier>();
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.black,
      statusBarIconBrightness: Brightness.dark,
    ));

    final CameraController? cameraController = controller;

    if (cameraController == null || !cameraController.value.isInitialized) {
      return Scaffold(
          backgroundColor: ColorPalette.backgroundWhite,
          appBar: AppBar(
            scrolledUnderElevation: 0,
            leading: IconButton(
              icon: Icon(
                Icons.keyboard_arrow_left_rounded,
                size: 32,
                color: ColorPalette.brand,
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            backgroundColor: ColorPalette.backgroundWhite,
          ),
          body: Center(
              child: CircularProgressIndicator(
            color: ColorPalette.brand,
          )));
    }

    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            Container(
              color: Colors.black,
              child: Column(children: [
                SizedBox(height: 16 * y),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 16.0 * x),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      InkWell(
                        onTap: () {
                          Navigator.of(context).pop();
                        },
                        child: Icon(Icons.close_rounded, color: Colors.white, size: 26 * f),
                      ),
                      Icon(Icons.info_rounded, color: Colors.white, size: 26 * f),
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.symmetric(vertical: 16 * y),
                  child: Text(
                    translationNotifier.getText('cameraScreenContourMessage'),
                    textAlign: TextAlign.center,
                    style: TextStylePalette.whiteAkko16w400,
                  ),
                )
              ]),
            ),
            if (showDarknessWarning)
              Container(
                height: 30 * y,
                color: ColorPalette.warningRed,
                width: MediaQuery.of(context).size.width,
                child: Center(
                  child: Text(
                    translationNotifier.getText('cameraScreenLightWarning'),
                    style: TextStylePalette.whiteAkko16w400,
                  ),
                ),
              ),
            Expanded(
              child: Container(
                color: Colors.black,
                width: MediaQuery.of(context).size.width,
                child: Stack(
                  children: <Widget>[
                    Center(
                      child: CameraPreview(controller!),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(vertical: 16 * y),
              color: Colors.black,
              child: Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    IconButton(
                      onPressed: () {
                        toggleFlash();
                      },
                      icon: Icon(
                        isFlashEnabled ? Icons.flash_off_rounded : Icons.flash_on_rounded,
                        color: Colors.white,
                        size: 25 * f,
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        takePicture();
                      },
                      child: Container(
                        height: 52 * f,
                        width: 52 * f,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: ColorPalette.aqua200,
                        ),
                        child: Center(
                          child: Icon(
                            Icons.camera_alt_rounded,
                            color: Colors.white,
                            size: 25 * f,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 50 * x,
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
