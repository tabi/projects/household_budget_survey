import 'package:flutter/material.dart';

import '../../infrastructure/notifiers/overview_notifiers/overview_notifier.dart';
import '../../infrastructure/notifiers/translation_notifier.dart';
import '../../infrastructure/utils/color_pallette.dart';
import '../../infrastructure/utils/date_util.dart';
import '../../infrastructure/utils/text_style_palette.dart';

Future<void> getOverviewInfoDrawer(
    BuildContext context, OverviewNotifier overviewNotifier, TranslationNotifier translationNotifier) {
  return showModalBottomSheet<void>(
    backgroundColor: ColorPalette.backgroundWhite,
    showDragHandle: true,
    context: context,
    builder: (context) {
      return SafeArea(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(left: 16, right: 16, bottom: 16),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    translationNotifier.getText('aboutBudgetSurveyModal'),
                    style: TextStylePalette.primaryAkko16w600,
                  ),
                  Expanded(child: Container()),
                  Container(
                    width: 20,
                    child: InkWell(
                      child: Icon(
                        Icons.close_rounded,
                        size: 20,
                        color: ColorPalette.textPrimary,
                      ),
                      onTap: () => Navigator.pop(context),
                    ),
                  ),
                ],
              ),
            ),
            Divider(
              color: ColorPalette.grey200,
              height: 1,
            ),
            Padding(
              padding: EdgeInsets.all(16.0),
              child: RichText(
                text: TextSpan(
                  style: TextStylePalette.primaryAkko16w400h15,
                  children: [
                    TextSpan(
                      text: translationNotifier.getText('daycounterModalTextOne') + ' ',
                    ),
                    TextSpan(
                      text: getDateStringNoLocale(overviewNotifier.surveyDayList.first.date),
                      style: TextStylePalette.primaryAkko16w600h15,
                    ),
                    TextSpan(text: ' ' + translationNotifier.getText('until') + ' '),
                    TextSpan(
                      text: getDateStringNoLocale(overviewNotifier.surveyDayList.last.date),
                      style: TextStylePalette.primaryAkko16w600h15,
                    ),
                    TextSpan(text: translationNotifier.getText('dayCounterModalTextTwo')),
                  ],
                ),
              ),
            ),
          ],
        ),
      );
    },
  );
}
