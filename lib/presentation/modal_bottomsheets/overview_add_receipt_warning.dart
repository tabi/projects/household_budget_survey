import 'package:flutter/material.dart';

import '../../infrastructure/notifiers/overview_notifiers/overview_notifier.dart';
import '../../infrastructure/notifiers/translation_notifier.dart';
import '../../infrastructure/utils/color_pallette.dart';
import '../../infrastructure/utils/date_util.dart';
import '../../infrastructure/utils/text_style_palette.dart';

Future<void> getOverviewAddExpenseWarning(
    BuildContext context, OverviewNotifier overviewNotifier, TranslationNotifier translationNotifier) {
  bool _hasStopShowingToggled = false;
  return showModalBottomSheet<void>(
    backgroundColor: ColorPalette.backgroundWhite,
    showDragHandle: true,
    context: context,
    builder: (context) {
      return StatefulBuilder(
        builder: (BuildContext context, StateSetter setState) {
          return SafeArea(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        translationNotifier.getText('watchout'),
                        style: TextStylePalette.brandSoho20w600,
                      ),
                      Expanded(child: Container()),
                      Container(
                        width: 20,
                        child: InkWell(
                          child: Icon(
                            Icons.close_rounded,
                            size: 20,
                            color: ColorPalette.textPrimary,
                          ),
                          onTap: () => Navigator.pop(context),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                  child: RichText(
                    text: TextSpan(
                      style: TextStylePalette.blackAkko16w400h15,
                      children: [
                        TextSpan(
                          text: (translationNotifier.getText('addExpenseDrawerWarningTextOne') + ' '),
                        ),
                        TextSpan(
                          text: getDateStringNoLocale(overviewNotifier.surveyDayList.first.date),
                          style: TextStylePalette.blackAkko16w600h15,
                        ),
                        TextSpan(text: ' ' + translationNotifier.getText('addExpenseDrawerWarningTextTwo') + ' '),
                        TextSpan(
                          text: getDateStringNoLocale(overviewNotifier.surveyDayList.last.date),
                          style: TextStylePalette.blackAkko16w600h15,
                        ),
                        TextSpan(
                          text: ' ' + translationNotifier.getText('addExpenseDrawerWarningTextThree'),
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                  child: CheckboxListTile(
                    title: Text(
                      translationNotifier.getText('dontShowThisMessage'),
                      style: TextStylePalette.blackAkko16w500,
                    ),
                    contentPadding: EdgeInsets.zero,
                    value: _hasStopShowingToggled,
                    onChanged: (newValue) {
                      setState(() {
                        _hasStopShowingToggled = newValue!;
                      });
                    },
                    activeColor: ColorPalette.aqua300,
                    controlAffinity: ListTileControlAffinity.leading,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                  child: SizedBox(
                    height: 48,
                    child: TextButton(
                      style: TextButton.styleFrom(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(4.0),
                        ),
                        foregroundColor: Colors.white,
                        backgroundColor: ColorPalette.aqua200,
                        minimumSize: Size.fromHeight(44),
                      ),
                      onPressed: () {
                        overviewNotifier.closedOutsideSurveyPeriodWaring = _hasStopShowingToggled;
                        Navigator.pop(context);
                      },
                      child: Text(
                        translationNotifier.getText('understood'),
                        style: TextStylePalette.whiteAkko16w400,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          );
        },
      );
    },
  );
}
