import 'package:flutter/material.dart';

import '../../infrastructure/dtos/receipt_dto.dart';
import '../../infrastructure/notifiers/translation_notifier.dart';
import '../../infrastructure/utils/color_pallette.dart';
import '../../infrastructure/utils/text_style_palette.dart';

Future<void> getModalAddDrawer(BuildContext context, TranslationNotifier translationNotifier, [DateTime? boughtOn]) {
  return showModalBottomSheet<void>(
    backgroundColor: ColorPalette.backgroundWhite,
    context: context,
    builder: (BuildContext context) {
      return SafeArea(
        child: Container(
          child: SizedBox(
            child: Container(
              padding: EdgeInsets.all(16),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  InkWell(
                    child: Padding(
                      padding: const EdgeInsets.only(top: 16.0, bottom: 16.0),
                      child: Row(
                        children: [
                          Icon(
                            Icons.keyboard_rounded,
                            color: ColorPalette.textPrimary,
                          ),
                          Container(
                            width: 16,
                          ),
                          Text(
                            translationNotifier.getText('addModeManual'),
                            style: TextStylePalette.primaryAkko16w400,
                          )
                        ],
                      ),
                    ),
                    onTap: () {
                      Navigator.pop(context);
                      if (boughtOn != null) {
                        Navigator.pushNamed(context, 'addManualReceipt',
                            arguments: ReceiptDto.create(boughtOn: boughtOn));
                      } else {
                        Navigator.pushNamed(context, 'addManualReceipt');
                      }
                    },
                  ),
                  InkWell(
                    child: Padding(
                      padding: const EdgeInsets.only(top: 16.0, bottom: 16.0),
                      child: Row(
                        children: [
                          Icon(
                            Icons.camera_alt_rounded,
                            color: ColorPalette.textPrimary,
                          ),
                          Container(
                            width: 16,
                          ),
                          Text(
                            translationNotifier.getText('addModeCamera'),
                            style: TextStylePalette.primaryAkko16w400,
                          )
                        ],
                      ),
                    ),
                    onTap: () {
                      Navigator.pop(context);
                      Navigator.pushNamed(context, 'addPhotoReceipt');
                    },
                  ),
                  // InkWell(
                  //   child: Padding(
                  //     padding: const EdgeInsets.only(top: 16.0, bottom: 16.0),
                  //     child: Row(
                  //       children: [
                  //         Icon(
                  //           Icons.attachment_rounded,
                  //           color: ColorPalette.textPrimary,
                  //         ),
                  //         Container(
                  //           width: 16,
                  //         ),
                  //         Text(
                  //           translationNotifier.getText('addModeUpload'),
                  //           style: TextStylePalette.primaryAkko16w400,
                  //         )
                  //       ],
                  //     ),
                  //   ),
                  //   onTap: () {
                  //     Navigator.pop(context);
                  //   },
                  // ),
                ],
              ),
            ),
          ),
        ),
      );
    },
  );
}
