import 'package:flutter/material.dart';

import '../../infrastructure/dtos/product_dto.dart';
import '../../infrastructure/notifiers/transaction_notifier.dart';
import '../../infrastructure/notifiers/translation_notifier.dart';
import '../../infrastructure/utils/color_pallette.dart';
import '../../infrastructure/utils/text_style_palette.dart';
import '../receipt_edit/product_drawer/product_detail_screen.dart';

Future<void> getModalProductOption(BuildContext context, TranslationNotifier translationNotifier,
    TransactionManualNotifier transactionNotifier, ProductDto productDto) {
  return showModalBottomSheet<void>(
    backgroundColor: ColorPalette.backgroundWhite,
    context: context,
    builder: (BuildContext context) {
      return SafeArea(
        child: Container(
          child: SizedBox(
            child: Container(
              padding: EdgeInsets.all(16),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  InkWell(
                    child: Padding(
                      padding: const EdgeInsets.only(top: 16.0, bottom: 16.0),
                      child: Row(
                        children: [
                          Icon(
                            Icons.edit_rounded,
                            color: ColorPalette.textPrimary,
                          ),
                          Container(
                            width: 16,
                          ),
                          Text(
                            translationNotifier.getText('modifyProductButton'),
                            style: TextStylePalette.primaryAkko16w400,
                          )
                        ],
                      ),
                    ),
                    onTap: () async {
                      Navigator.pop(context);
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => ProductDetailScreen(transactionNotifier, productDto),
                        ),
                      );
                    },
                  ),
                  InkWell(
                    child: Padding(
                      padding: const EdgeInsets.only(top: 16.0, bottom: 16.0),
                      child: Row(
                        children: [
                          Icon(
                            Icons.delete_rounded,
                            color: ColorPalette.textPrimary,
                          ),
                          Container(
                            width: 16,
                          ),
                          Text(
                            translationNotifier.getText('deleteProductButton'),
                            style: TextStylePalette.primaryAkko16w400,
                          )
                        ],
                      ),
                    ),
                    onTap: () {
                      Navigator.pop(context);
                      transactionNotifier.removeProduct(productDto);
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      );
    },
  );
}
