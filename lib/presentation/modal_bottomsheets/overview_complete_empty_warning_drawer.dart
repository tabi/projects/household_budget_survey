import 'package:flutter/material.dart';
import '../../infrastructure/notifiers/overview_notifiers/overview_notifier.dart';
import '../../infrastructure/notifiers/translation_notifier.dart';
import '../../infrastructure/utils/color_pallette.dart';
import '../../infrastructure/utils/text_style_palette.dart';

enum NoExpenseReason { noExpense, vacation }

Future<void> getOverviewCompleteEmptyDayWarning(
    BuildContext context, OverviewNotifier overviewNotifier, TranslationNotifier translationNotifier) {
  return showModalBottomSheet<void>(
    backgroundColor: ColorPalette.backgroundWhite,
    showDragHandle: true,
    context: context,
    builder: (context) {
      NoExpenseReason? _reason = null;
      return StatefulBuilder(
        builder: (BuildContext context, StateSetter setState) {
          return SafeArea(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        translationNotifier.getText('completeDay'),
                        style: TextStylePalette.brandSoho20w600,
                      ),
                      Expanded(child: Container()),
                      Container(
                        width: 20,
                        child: InkWell(
                          child: Icon(
                            Icons.close_rounded,
                            size: 20,
                            color: ColorPalette.textPrimary,
                          ),
                          onTap: () => Navigator.pop(context),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                  child: Text(
                    translationNotifier.getText('completeDayDrawerText'),
                    style: TextStylePalette.blackAkko16w400,
                  ),
                ),
                Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                    child: Column(
                      children: [
                        RadioListTile<NoExpenseReason>(
                          title: Text(
                            translationNotifier.getText('completeDayDrawerOptionNoExpenses'),
                            style: TextStylePalette.blackAkko16w400,
                          ),
                          contentPadding: EdgeInsets.zero,
                          value: NoExpenseReason.noExpense,
                          activeColor: ColorPalette.aqua300,
                          groupValue: _reason,
                          onChanged: (NoExpenseReason? value) {
                            setState(() {
                              _reason = value;
                            });
                          },
                        ),
                        RadioListTile<NoExpenseReason>(
                          title: Text(
                            translationNotifier.getText('completeDayDrawerOptionVacation'),
                            style: TextStylePalette.blackAkko16w400,
                          ),
                          contentPadding: EdgeInsets.zero,
                          value: NoExpenseReason.vacation,
                          activeColor: ColorPalette.aqua300,
                          groupValue: _reason,
                          onChanged: (NoExpenseReason? value) {
                            setState(() {
                              _reason = value;
                            });
                          },
                        ),
                      ],
                    )),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                  child: SizedBox(
                    height: 48,
                    child: TextButton(
                      style: TextButton.styleFrom(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(4.0),
                        ),
                        foregroundColor: Colors.white,
                        backgroundColor: _reason != null ? ColorPalette.aqua200 : ColorPalette.grey300,
                        minimumSize: Size.fromHeight(44),
                      ),
                      onPressed: () {
                        if (_reason != null) {
                          overviewNotifier.completeDay(overviewNotifier.selectedDay, _reason.toString());
                          Navigator.pop(context);
                        }
                      },
                      child: Text(
                        translationNotifier.getText('completeDay'),
                        style: TextStylePalette.whiteAkko16w400,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          );
        },
      );
    },
  );
}
