import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

import '../../infrastructure/notifiers/app_tour_notifier.dart';
import '../../infrastructure/notifiers/translation_notifier.dart';
import '../../infrastructure/utils/color_pallette.dart';
import '../../infrastructure/utils/text_style_palette.dart';

class AppTourScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final appTourNotifier = context.watch<AppTourNotifier>();
    final translationNotifier = context.watch<TranslationNotifier>();

    return Scaffold(
      backgroundColor: ColorPalette.backgroundWhite,
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: [
              Expanded(
                child: PageView.builder(
                  controller: appTourNotifier.controller,
                  physics: BouncingScrollPhysics(),
                  itemCount: appTourNotifier.pages.length,
                  onPageChanged: (idx) {
                    appTourNotifier.index = idx;
                  },
                  itemBuilder: (_, idx) {
                    return appTourNotifier.pages[idx % appTourNotifier.pages.length];
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: SmoothPageIndicator(
                  controller: appTourNotifier.controller,
                  count: appTourNotifier.pages.length,
                  effect: const WormEffect(
                    dotHeight: 12,
                    dotWidth: 12,
                    spacing: 12,
                    radius: 12,
                    dotColor: ColorPalette.grey200,
                    activeDotColor: ColorPalette.aqua200,
                    type: WormType.thinUnderground,
                  ),
                ),
              ),
              !appTourNotifier.reachedEnd
                  ? Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Expanded(
                            child: TextButton(
                              style: TextButton.styleFrom(
                                padding: const EdgeInsets.all(12),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(4.0),
                                  side: BorderSide(
                                    color: Colors.transparent,
                                  ),
                                ),
                                foregroundColor: ColorPalette.aqua200,
                              ),
                              onPressed: () {
                                appTourNotifier.onSkipButton(context);
                              },
                              child: Text(translationNotifier.getText('skip'), style: TextStylePalette.aquaAkko16w400),
                            ),
                          ),
                          Container(
                            width: 16,
                          ),
                          Expanded(
                            child: OutlinedButton(
                              style: OutlinedButton.styleFrom(
                                padding: const EdgeInsets.all(12),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(4.0),
                                ),
                                foregroundColor: Color(0xFFFFFFFF),
                                backgroundColor: ColorPalette.aqua200,
                                side: BorderSide(
                                  color: ColorPalette.aqua200,
                                ),
                              ),
                              onPressed: () {
                                appTourNotifier.onNextButton();
                              },
                              child: Text(
                                translationNotifier.getText('next'),
                                style: TextStylePalette.whiteAkko16w400,
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                  : Container(
                      child: Row(
                        children: [
                          Expanded(
                            child: OutlinedButton(
                              style: OutlinedButton.styleFrom(
                                padding: EdgeInsets.all(12),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(4.0),
                                ),
                                foregroundColor: Color(0xFFFFFFFF),
                                backgroundColor: ColorPalette.aqua200,
                                side: BorderSide(
                                  color: ColorPalette.aqua200,
                                ),
                              ),
                              onPressed: () {
                                appTourNotifier.onLoginButton(context);
                              },
                              child: Text(
                                translationNotifier.getText('login'),
                                style: TextStylePalette.whiteAkko16w400,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
            ],
          ),
        ),
      ),
    );
  }
}
