import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../infrastructure/notifiers/onboarding_notifier.dart';
import '../../infrastructure/notifiers/overview_notifiers/overview_notifier.dart';
import '../../infrastructure/notifiers/translation_notifier.dart';
import '../../infrastructure/utils/color_pallette.dart';
import '../../infrastructure/utils/text_style_palette.dart';
import '../menu/menu_widget.dart';

class OnboardingScreen extends StatelessWidget {
  bool validateEmail(String value) {
    const pattern = r"(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'"
        r'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-'
        r'\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*'
        r'[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4]'
        r'[0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9]'
        r'[0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\'
        r'x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])';
    final regex = RegExp(pattern);

    if (value.isEmpty) {
      return false;
    } else {
      return regex.hasMatch(value);
    }
  }

  @override
  Widget build(BuildContext context) {
    final onboardingNotifier = context.watch<OnboardingNotifier>();
    final translationNotifier = context.watch<TranslationNotifier>();
    final overviewNotifier = context.watch<OverviewNotifier>();
    return Scaffold(
      backgroundColor: ColorPalette.backgroundWhite,
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: [
              Expanded(
                child: PageView.builder(
                  controller: onboardingNotifier.controller,
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: onboardingNotifier.pages.length,
                  onPageChanged: (idx) {
                    onboardingNotifier.index = idx;
                  },
                  itemBuilder: (_, idx) {
                    return onboardingNotifier.pages[idx % onboardingNotifier.pages.length];
                  },
                ),
              ),
              Container(
                child: Row(
                  children: [
                    Expanded(
                      child: OutlinedButton(
                        style: OutlinedButton.styleFrom(
                          padding: const EdgeInsets.all(12),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(4.0),
                          ),
                          foregroundColor: ColorPalette.textWhite,
                          backgroundColor: (onboardingNotifier.isSurveyStartPage() && !onboardingNotifier.surveyDateSelected())
                              ? ColorPalette.grey300
                              : ColorPalette.aqua200,
                          side: BorderSide(
                            color: (onboardingNotifier.isSurveyStartPage() && !onboardingNotifier.surveyDateSelected())
                                ? ColorPalette.grey300
                                : ColorPalette.aqua200,
                          ),
                        ),
                        onPressed: () async {
                          if (!onboardingNotifier.isSurveyStartPage()) {
                            if (onboardingNotifier.reachedEnd) {
                              Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) => MenuWidget()), (Route<dynamic> route) => false);
                            } else {
                              if (!onboardingNotifier.isEmailPage()) {
                                onboardingNotifier.onNextButton();
                              } else {
                                if (validateEmail(onboardingNotifier.emailController.text)) {
                                  onboardingNotifier.onNextButton();
                                }
                              }
                            }
                          } else {
                            if (onboardingNotifier.surveyDateSelected()) {
                              await onboardingNotifier.initializeDays(onboardingNotifier.selectedPeriod!.startDate,
                                  (onboardingNotifier.selectedPeriod!.endDate.difference(onboardingNotifier.selectedPeriod!.startDate).inHours / 24).ceil());
                              onboardingNotifier.onNextButton();
                              await overviewNotifier.loadSurveyDays();
                            }
                          }
                        },
                        child: Text(
                          translationNotifier.getText('moveOn'),
                          style: TextStylePalette.whiteAkko16w400,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
