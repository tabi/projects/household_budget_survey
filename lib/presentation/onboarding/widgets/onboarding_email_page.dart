import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../infrastructure/notifiers/onboarding_notifier.dart';
import '../../../infrastructure/notifiers/translation_notifier.dart';
import '../../../infrastructure/utils/color_pallette.dart';
import '../../../infrastructure/utils/text_style_palette.dart';

class OnboardingEmailPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final translationNotifier = context.watch<TranslationNotifier>();
    final onboardingNotifier = context.watch<OnboardingNotifier>();

    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            translationNotifier.getText('yourMailAddress'),
            style: TextStylePalette.brandSoho26w600,
          ),
          Container(
            height: 16,
          ),
          Text(
            translationNotifier.getText('whatIsYourMail'),
            style: TextStylePalette.brandSoho20w600,
          ),
          Container(
            height: 16,
          ),
          Padding(
            padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
            child: SizedBox(
              height: 48,
              child: TextField(
                onTapOutside: (event) => FocusScope.of(context).unfocus(),
                controller: onboardingNotifier.emailController,
                textAlignVertical: TextAlignVertical.center,
                cursorColor: ColorPalette.textBlack,
                style: TextStylePalette.blackAkko16w400,
                autocorrect: false,
                decoration: InputDecoration(
                  hintText: translationNotifier.getText('exampleMailHintText'),
                  contentPadding: EdgeInsets.all(14.0),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: ColorPalette.aqua300),
                    borderRadius: BorderRadius.circular(4),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: const BorderSide(
                      color: Colors.black,
                      width: 2,
                    ),
                    borderRadius: BorderRadius.circular(4),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
