import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../infrastructure/notifiers/translation_notifier.dart';
import '../../../infrastructure/utils/text_style_palette.dart';

class OnboardingSurveyExplanationPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final translationNotifier = context.watch<TranslationNotifier>();
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 10, vertical: 4),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Image.asset(
            'assets/images/onboarding/surveylist.png',
          ),
          Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  translationNotifier.getText('onboardingSurveyExplanationTitle'),
                  style: TextStylePalette.brandSoho20w600,
                ),
                Container(
                  height: 8,
                ),
                Text(
                  translationNotifier.getText('onboardingSurveyExplanationText'),
                  textAlign: TextAlign.center,
                  style: TextStylePalette.primaryAkko16w400,
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
