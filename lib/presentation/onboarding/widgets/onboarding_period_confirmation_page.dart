import 'package:flutter/material.dart';
import '../../../infrastructure/notifiers/onboarding_notifier.dart';
import '../../../infrastructure/utils/date_util.dart';
import 'package:provider/provider.dart';

import '../../../infrastructure/notifiers/translation_notifier.dart';
import '../../../infrastructure/utils/text_style_palette.dart';

class OnboardingPeriodConfirmationPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final translationNotifier = context.watch<TranslationNotifier>();
    final onboardingNotifier = context.watch<OnboardingNotifier>();
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 10, vertical: 4),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Image.asset(
            'assets/images/onboarding/success.png',
          ),
          Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  translationNotifier.getText('onboardingConfrimationText') +
                      getDateStringOnboarding(onboardingNotifier.selectedPeriod!.startDate, translationNotifier) +
                      '!',
                  textAlign: TextAlign.center,
                  style: TextStylePalette.brandSoho20w600,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
