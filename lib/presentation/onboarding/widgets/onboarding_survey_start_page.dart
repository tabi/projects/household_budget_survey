import 'package:flutter/material.dart';
import '../../../infrastructure/notifiers/onboarding_notifier.dart';
import '../../../infrastructure/utils/color_pallette.dart';
import '../../../infrastructure/utils/date_util.dart';
import '../../../infrastructure/utils/extensions.dart';
import 'package:provider/provider.dart';

import '../../../infrastructure/notifiers/translation_notifier.dart';
import '../../../infrastructure/utils/text_style_palette.dart';

class OnboardingSurveyStartPage extends StatefulWidget {
  @override
  State<OnboardingSurveyStartPage> createState() => _OnboardingSurveyStartPageState();
}

class _OnboardingSurveyStartPageState extends State<OnboardingSurveyStartPage> {
  bool? isAway;
  int? weekOption;

  @override
  Widget build(BuildContext context) {
    final translationNotifier = context.watch<TranslationNotifier>();
    final onboardingNotifier = context.watch<OnboardingNotifier>();

    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            translationNotifier.getText('onboardingSurveyStartTitle'),
            style: TextStylePalette.brandSoho26w600,
          ),
          Container(
            height: 16,
          ),
          Text(
            translationNotifier.getText('onboardingSurveyStartSubTitle') +
                ' ' +
                getDateStringOnboarding(onboardingNotifier.suggestedDates[0].startDate, translationNotifier) +
                ' ' +
                translationNotifier.getText('untilFull') +
                ' ' +
                getDateStringOnboarding(onboardingNotifier.suggestedDates[0].endDate, translationNotifier),
            style: TextStylePalette.brandSoho20w600,
          ),
          Container(
            height: 16,
          ),
          Text(
            translationNotifier.getText('onboardingSurveyStartTextOne'),
            style: TextStylePalette.primaryAkko16w400,
          ),
          RadioListTile(
            title: Text(
              translationNotifier.getText('yes'),
              style: TextStylePalette.blackAkko16w500,
            ),
            contentPadding: EdgeInsets.zero,
            value: true,
            groupValue: isAway,
            onChanged: (value) {
              setState(() {
                isAway = value;
                onboardingNotifier.selectedPeriod = null;
              });
            },
            activeColor: ColorPalette.aqua300,
          ),
          RadioListTile(
            title: Text(
              translationNotifier.getText('no'),
              style: TextStylePalette.blackAkko16w500,
            ),
            contentPadding: EdgeInsets.zero,
            value: false,
            groupValue: isAway,
            onChanged: (value) {
              setState(() {
                isAway = value;
                weekOption = null;
                onboardingNotifier.selectedPeriod = onboardingNotifier.suggestedDates.first;
              });
            },
            activeColor: ColorPalette.aqua300,
          ),
          if (isAway != null && isAway == true)
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  height: 16,
                ),
                Text(
                  translationNotifier.getText('onboardingSurveyStartTextTwo'),
                  style: TextStylePalette.primaryAkko16w400,
                ),
                Container(
                  height: 16,
                ),
                RadioListTile(
                  title: Text(
                    getDateStringOnboarding(onboardingNotifier.suggestedDates[1].startDate, translationNotifier)
                            .capitalize() +
                        ' ' +
                        translationNotifier.getText('untilFull') +
                        ' ' +
                        getDateStringOnboarding(onboardingNotifier.suggestedDates[1].endDate, translationNotifier),
                    style: TextStylePalette.blackAkko16w500,
                  ),
                  contentPadding: EdgeInsets.zero,
                  value: 0,
                  groupValue: weekOption,
                  onChanged: (value) {
                    setState(() {
                      weekOption = value;
                      onboardingNotifier.selectedPeriod = onboardingNotifier.suggestedDates[1];
                    });
                  },
                  activeColor: ColorPalette.aqua300,
                ),
                RadioListTile(
                  title: Text(
                    getDateStringOnboarding(onboardingNotifier.suggestedDates[2].startDate, translationNotifier)
                            .capitalize() +
                        ' ' +
                        translationNotifier.getText('untilFull') +
                        ' ' +
                        getDateStringOnboarding(onboardingNotifier.suggestedDates[2].endDate, translationNotifier),
                    style: TextStylePalette.blackAkko16w500,
                  ),
                  contentPadding: EdgeInsets.zero,
                  value: 1,
                  groupValue: weekOption,
                  onChanged: (value) {
                    setState(() {
                      weekOption = value;
                      onboardingNotifier.selectedPeriod = onboardingNotifier.suggestedDates[2];
                    });
                  },
                  activeColor: ColorPalette.aqua300,
                ),
                RadioListTile(
                  title: Text(
                    getDateStringOnboarding(onboardingNotifier.suggestedDates[3].startDate, translationNotifier)
                            .capitalize() +
                        ' ' +
                        translationNotifier.getText('untilFull') +
                        ' ' +
                        getDateStringOnboarding(onboardingNotifier.suggestedDates[3].endDate, translationNotifier),
                    style: TextStylePalette.blackAkko16w500,
                  ),
                  contentPadding: EdgeInsets.zero,
                  value: 2,
                  groupValue: weekOption,
                  onChanged: (value) {
                    setState(() {
                      weekOption = value;
                      onboardingNotifier.selectedPeriod = onboardingNotifier.suggestedDates[3];
                    });
                  },
                  activeColor: ColorPalette.aqua300,
                ),
                RadioListTile(
                  title: Text(
                    getDateStringOnboarding(onboardingNotifier.suggestedDates[4].startDate, translationNotifier)
                            .capitalize() +
                        ' ' +
                        translationNotifier.getText('untilFull') +
                        ' ' +
                        getDateStringOnboarding(onboardingNotifier.suggestedDates[4].endDate, translationNotifier),
                    style: TextStylePalette.blackAkko16w500,
                  ),
                  contentPadding: EdgeInsets.zero,
                  value: 3,
                  groupValue: weekOption,
                  onChanged: (value) {
                    setState(() {
                      weekOption = value;
                      onboardingNotifier.selectedPeriod = onboardingNotifier.suggestedDates[4];
                    });
                  },
                  activeColor: ColorPalette.aqua300,
                ),
                // RadioListTile(
                //   title: Text(
                //     translationNotifier.getText('noneOfThese').capitalize(),
                //     style: TextStylePalette.blackAkko16w500,
                //   ),
                //   contentPadding: EdgeInsets.zero,
                //   value: 4,
                //   groupValue: weekOption,
                //   onChanged: (value) {
                //     setState(() {
                //       weekOption = value;
                //       onboardingNotifier.selectedPeriod = null;
                //     });
                //   },
                //   activeColor: ColorPalette.aqua300,
                // ),
              ],
            )
        ],
      ),
    );
  }
}
