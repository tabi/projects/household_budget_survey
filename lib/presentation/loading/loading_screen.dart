import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../infrastructure/notifiers/overview_notifiers/overview_notifier.dart';
import '../../infrastructure/utils/color_pallette.dart';
import '../menu/menu_widget.dart';
import '../onboarding/onboarding_screen.dart';

class LoadingScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final overviewNotifier = context.watch<OverviewNotifier>();
    return Scaffold(
      backgroundColor: ColorPalette.backgroundWhite,
      body: overviewNotifier.surveyDayList.isEmpty ? OnboardingScreen() : MenuWidget(),
    );
  }
}
