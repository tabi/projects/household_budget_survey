import 'package:draggable_scrollbar/draggable_scrollbar.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../infrastructure/dtos/tree_node_dto.dart';
import '../../infrastructure/notifiers/insights_notifier.dart';
import '../../infrastructure/notifiers/translation_notifier.dart';
import '../../infrastructure/utils/color_pallette.dart';
import '../../infrastructure/utils/text_style_palette.dart';
import '../../infrastructure/utils/ui_scalers.dart';

class InsightsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final translationNotifier = context.watch<TranslationNotifier>();
    return Scaffold(
      appBar: AppBar(
        scrolledUnderElevation: 0.0,
        leadingWidth: 0,
        centerTitle: false,
        backgroundColor: ColorPalette.backgroundWhite,
        title: Text(
          'Inzichten',
          style: TextStylePalette.brandSoho26w600,
        ),
      ),
      backgroundColor: ColorPalette.backgroundOffWhite,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Icon(
              Icons.construction_rounded,
              size: 64,
              color: ColorPalette.grey300,
            ),
            Text(
              translationNotifier.getText('underconstruction'),
              style: TextStylePalette.primaryAkko16w400,
            )
          ],
        ),
      ),
    );
  }
}

class CategoryListWidget extends StatelessWidget {
  final _scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    final insightsNotifier = context.watch<InsightsNotifier>();
    final treeNodeDto = insightsNotifier.treeNodeDto;
    if (treeNodeDto == null) return SizedBox();
    return Column(
      children: [
        SizedBox(height: 30 * y),
        InkWell(
          onTap: () {
            insightsNotifier.updateActiveTreeNodeDto(treeNodeDto.parentNode);
          },
          child: Icon(Icons.arrow_back_rounded),
        ),
        SizedBox(
          height: 172 * y,
          width: 500 * x,
          child: DraggableScrollbar.rrect(
            alwaysVisibleScrollThumb: treeNodeDto.childNodes.length > 4,
            heightScrollThumb:
                treeNodeDto.childNodes.length > 10 ? 50.0 : 50.0 + (10 - treeNodeDto.childNodes.length) * 10.0,
            backgroundColor: Colors.grey,
            padding: EdgeInsets.only(right: 4.0 * x),
            controller: _scrollController,
            child: ListView.builder(
              controller: _scrollController,
              itemCount: treeNodeDto.childNodes.length,
              itemBuilder: (_, int index) {
                return CategoryTileWidget(treeNodeDto.childNodes.entries.elementAt(index).value);
              },
            ),
          ),
        ),
      ],
    );
  }
}

class CategoryTileWidget extends StatelessWidget {
  final TreeNodeDto _treeNodeDto;

  const CategoryTileWidget(this._treeNodeDto);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => context.read<InsightsNotifier>().updateActiveTreeNodeDto(_treeNodeDto),
      child: Row(
        children: [
          Text(_treeNodeDto.coicopName),
          Text(_treeNodeDto.getTotalCost.toString()),
        ],
      ),
    );
  }
}
