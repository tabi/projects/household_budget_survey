import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../infrastructure/notifiers/login_notifier.dart';
import '../../infrastructure/notifiers/translation_notifier.dart';
import '../../infrastructure/utils/color_pallette.dart';
import '../../infrastructure/utils/text_style_palette.dart';

class LoginLostScreen extends StatelessWidget {
  const LoginLostScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final translationNotifier = context.watch<TranslationNotifier>();
    final loginNotifier = context.watch<LoginNotifier>();

    return Scaffold(
      backgroundColor: ColorPalette.backgroundWhite,
      appBar: AppBar(
        scrolledUnderElevation: 0,
        leading: IconButton(
          icon: Icon(
            Icons.keyboard_arrow_left_rounded,
            size: 32,
            color: ColorPalette.brand,
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        backgroundColor: ColorPalette.backgroundWhite,
      ),
      body: SingleChildScrollView(
        child: SafeArea(
          minimum: EdgeInsets.only(bottom: 16, right: 16, left: 16),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                translationNotifier.getText('loginLostHeader'),
                style: TextStylePalette.brandSoho26w600,
              ),
              Container(
                height: 16,
              ),
              Text(
                translationNotifier.getText('loginLostBodyOne'),
                style: TextStylePalette.primaryAkko16w400,
              ),
              Container(
                height: 16,
              ),
              Text(
                translationNotifier.getText('loginLostBodyTwo'),
                style: TextStylePalette.primaryAkko16w400,
              ),
              Container(
                height: 24,
              ),
              Padding(
                padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      padding: EdgeInsets.all(12),
                      child: Icon(
                        Icons.phone_rounded,
                        color: Colors.white,
                      ),
                      decoration: BoxDecoration(color: ColorPalette.brand, borderRadius: BorderRadius.circular(4)),
                    ),
                    Container(
                      width: 16,
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          translationNotifier.getText('callus'),
                          style: TextStylePalette.primaryAkko16w400,
                        ),
                        RichText(
                          text: TextSpan(
                            text: translationNotifier.getText('telephone'),
                            style: TextStylePalette.aquaAkko14w400underlined,
                            recognizer: TapGestureRecognizer()
                              ..onTap = () {
                                loginNotifier.launchPhone();
                              },
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
              Divider(),
              Padding(
                padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      padding: EdgeInsets.all(12),
                      child: Icon(
                        Icons.mail_rounded,
                        color: Colors.white,
                      ),
                      decoration: BoxDecoration(color: ColorPalette.brand, borderRadius: BorderRadius.circular(4)),
                    ),
                    Container(
                      width: 16,
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          translationNotifier.getText('sendmail'),
                          style: TextStylePalette.primaryAkko16w400,
                        ),
                        RichText(
                          text: TextSpan(
                            text: translationNotifier.getText('mail'),
                            style: TextStylePalette.aquaAkko14w400underlined,
                            recognizer: TapGestureRecognizer()
                              ..onTap = () {
                                loginNotifier.launchMail();
                              },
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),
              Divider(),
            ],
          ),
        ),
      ),
    );
  }
}
