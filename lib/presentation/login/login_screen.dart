import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../infrastructure/notifiers/app_tour_notifier.dart';
import '../../infrastructure/notifiers/login_notifier.dart';
import '../../infrastructure/notifiers/overview_notifiers/overview_notifier.dart';
import '../../infrastructure/notifiers/translation_notifier.dart';
import '../../infrastructure/utils/color_pallette.dart';
import '../../infrastructure/utils/text_style_palette.dart';
import '../../infrastructure/utils/ui_scalers.dart';
import '../menu/menu_widget.dart';
import '../onboarding/onboarding_screen.dart';
import 'login_info_screen.dart';
import 'login_lost_screen.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final translationNotifier = context.watch<TranslationNotifier>();
    final loginNotifier = context.watch<LoginNotifier>();
    final appTourNotifier = context.watch<AppTourNotifier>();
    final overviewNotifier = context.watch<OverviewNotifier>();

    return Scaffold(
      backgroundColor: ColorPalette.backgroundWhite,
      appBar: AppBar(
        scrolledUnderElevation: 0,
        leading: IconButton(
          icon: Icon(
            Icons.keyboard_arrow_left_rounded,
            size: 32,
            color: ColorPalette.brand,
          ),
          onPressed: () {
            appTourNotifier.resetPage();
            Navigator.of(context).pop();
          },
        ),
        backgroundColor: ColorPalette.backgroundWhite,
      ),
      body: SafeArea(
        minimum: EdgeInsets.only(bottom: 16, right: 16, left: 16),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      translationNotifier.getText('login'),
                      style: TextStylePalette.brandSoho26w600,
                    ),
                    Container(
                      height: 8,
                    ),
                    if (!loginNotifier.isLoginCorrect)
                      Container(
                        decoration: BoxDecoration(
                          color: ColorPalette.warningRedOpacity,
                          borderRadius: BorderRadius.circular(4),
                        ),
                        padding: EdgeInsets.all(16),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(2),
                              child: Icon(
                                Icons.warning_rounded,
                                size: 20,
                                color: ColorPalette.warningRed,
                              ),
                            ),
                            Container(
                              width: 16,
                            ),
                            Expanded(
                              child: Text(
                                translationNotifier.getText('loginIncorrect'),
                                style: TextStylePalette.primaryAkko16w400,
                                softWrap: true,
                              ),
                            )
                          ],
                        ),
                      ),
                    Container(
                      height: 16,
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          translationNotifier.getText('username'),
                          style: TextStylePalette.blackAkko16w600,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                          child: SizedBox(
                            height: 48,
                            child: TextField(
                              onTapOutside: (event) => FocusScope.of(context).unfocus(),
                              controller: loginNotifier.usernameController,
                              textAlignVertical: TextAlignVertical.center,
                              cursorColor: ColorPalette.textBlack,
                              style: TextStylePalette.blackAkko16w400,
                              autocorrect: false,
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.all(14.0),
                                enabledBorder: OutlineInputBorder(
                                  borderSide: loginNotifier.hasUserFilled
                                      ? BorderSide(color: ColorPalette.aqua300)
                                      : BorderSide(
                                          color: ColorPalette.warningRed,
                                          width: 1,
                                        ),
                                  borderRadius: BorderRadius.circular(4),
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: const BorderSide(
                                    color: Colors.black,
                                    width: 2,
                                  ),
                                  borderRadius: BorderRadius.circular(4),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    if (!loginNotifier.hasUserFilled)
                      RichText(
                        text: TextSpan(
                          children: [
                            WidgetSpan(
                              child: Icon(
                                Icons.warning_rounded,
                                size: 16,
                                color: ColorPalette.warningRed,
                              ),
                            ),
                            WidgetSpan(
                                child: Container(
                              width: 6,
                            )),
                            TextSpan(
                              text: translationNotifier.getText('usernameError'),
                              style: TextStylePalette.redAkko16w400,
                            ),
                          ],
                        ),
                      ),
                    Container(
                      height: 16,
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          translationNotifier.getText('password'),
                          style: TextStylePalette.blackAkko16w600,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                          child: SizedBox(
                            height: 48,
                            child: TextField(
                                onTapOutside: (event) => FocusScope.of(context).unfocus(),
                                controller: loginNotifier.passwordController,
                                textAlignVertical: TextAlignVertical.center,
                                cursorColor: ColorPalette.textBlack,
                                style: TextStylePalette.blackAkko16w400,
                                autocorrect: false,
                                obscureText: !loginNotifier.passwordVisible,
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.all(14.0),
                                  enabledBorder: OutlineInputBorder(
                                    borderSide: loginNotifier.hasPassFilled
                                        ? BorderSide(color: ColorPalette.aqua300)
                                        : BorderSide(
                                            color: Colors.red,
                                            width: 1,
                                          ),
                                    borderRadius: BorderRadius.circular(4),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: const BorderSide(
                                      color: Colors.black,
                                      width: 2,
                                    ),
                                    borderRadius: BorderRadius.circular(4),
                                  ),
                                  suffixIcon: IconButton(
                                    icon: !loginNotifier.passwordVisible ? Icon(Icons.visibility_rounded) : Icon(Icons.visibility_off_rounded),
                                    onPressed: () => loginNotifier.passwordVisible = !loginNotifier.passwordVisible,
                                  ),
                                )),
                          ),
                        ),
                        loginNotifier.hasPassFilled
                            ? Container()
                            : RichText(
                                text: TextSpan(
                                  children: [
                                    WidgetSpan(
                                      child: Icon(
                                        Icons.warning_rounded,
                                        size: 16,
                                        color: ColorPalette.warningRed,
                                      ),
                                    ),
                                    WidgetSpan(
                                        child: Container(
                                      width: 6,
                                    )),
                                    TextSpan(
                                      text: translationNotifier.getText('passwordError'),
                                      style: TextStylePalette.redAkko16w400,
                                    ),
                                  ],
                                ),
                              ),
                        Padding(
                          padding: const EdgeInsets.only(top: 16.0, bottom: 16.0),
                          child: SizedBox(
                            height: 48,
                            child: TextButton(
                              style: TextButton.styleFrom(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(4.0),
                                ),
                                foregroundColor: Colors.white,
                                backgroundColor: ColorPalette.aqua200,
                                minimumSize: Size.fromHeight(44),
                              ),
                              onPressed: () async {
                                final loginSuccess = await loginNotifier.login();

                                if (loginSuccess) {
                                  final SharedPreferences prefs = await SharedPreferences.getInstance();
                                  await prefs.setBool('isLoggedIn', true);
                                  if (overviewNotifier.surveyDayList.isEmpty) {
                                    await Navigator.of(context)
                                        .pushAndRemoveUntil(MaterialPageRoute(builder: (context) => OnboardingScreen()), (Route<dynamic> route) => false);
                                  } else {
                                    await Navigator.of(context)
                                        .pushAndRemoveUntil(MaterialPageRoute(builder: (context) => MenuWidget()), (Route<dynamic> route) => false);
                                  }
                                }
                              },
                              child: loginNotifier.isLoading
                                  ? SizedBox(
                                      height: 24,
                                      width: 24,
                                      child: CircularProgressIndicator(
                                        color: Colors.white,
                                        backgroundColor: const Color.fromARGB(91, 221, 221, 221),
                                        strokeWidth: 2,
                                      ),
                                    )
                                  : Text(
                                      translationNotifier.getText('login'),
                                      style: TextStylePalette.whiteAkko16w400,
                                    ),
                            ),
                          ),
                        ),
                        Container(
                          height: 12,
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 16),
                          child: RichText(
                            text: TextSpan(
                                text: translationNotifier.getText('loginWhere'),
                                style: TextStylePalette.aquaAkko16w400underlined,
                                recognizer: TapGestureRecognizer()
                                  ..onTap = () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) {
                                          return LoginInfoScreen();
                                        },
                                      ),
                                    );
                                  }),
                          ),
                        ),
                        Container(
                          height: 8,
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 16),
                          child: RichText(
                            text: TextSpan(
                              text: translationNotifier.getText('loginLost'),
                              style: TextStylePalette.aquaAkko16w400underlined,
                              recognizer: TapGestureRecognizer()
                                ..onTap = () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) {
                                        return LoginLostScreen();
                                      },
                                    ),
                                  );
                                },
                            ),
                          ),
                        ),
                      ],
                    ),
                    Container(
                      height: 120 * x,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width * 0.25 * x,
                        ),
                        Expanded(
                          child: Image.asset(
                            'assets/images/cbs_logo.png',
                          ),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width * 0.25 * x,
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
