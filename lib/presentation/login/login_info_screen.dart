import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../infrastructure/notifiers/translation_notifier.dart';
import '../../infrastructure/utils/color_pallette.dart';
import '../../infrastructure/utils/text_style_palette.dart';

class LoginInfoScreen extends StatelessWidget {
  const LoginInfoScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final translationNotifier = context.watch<TranslationNotifier>();
    return Scaffold(
      backgroundColor: ColorPalette.backgroundWhite,
      appBar: AppBar(
        scrolledUnderElevation: 0,
        leading: IconButton(
          icon: Icon(
            Icons.keyboard_arrow_left_rounded,
            size: 32,
            color: ColorPalette.brand,
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        backgroundColor: ColorPalette.backgroundWhite,
      ),
      body: SingleChildScrollView(
        child: SafeArea(
          minimum: EdgeInsets.only(bottom: 16, right: 16, left: 16),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                translationNotifier.getText('loginInfoTitle'),
                style: TextStylePalette.brandSoho26w600,
              ),
              Container(
                height: 16,
              ),
              Text(
                translationNotifier.getText('loginInfoReceivedHeader'),
                style: TextStylePalette.primaryAkko16w600,
              ),
              Text(
                translationNotifier.getText('loginInfoReceivedBody'),
                style: TextStylePalette.primaryAkko16w400,
              ),
              Container(
                height: 16,
              ),
              Text(
                translationNotifier.getText('loginInfoNotReceivedHeader'),
                style: TextStylePalette.primaryAkko16w600,
              ),
              Text(
                translationNotifier.getText('loginInfoNotReceivedBody'),
                style: TextStylePalette.primaryAkko16w400,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
