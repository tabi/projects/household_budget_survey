import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../infrastructure/notifiers/search_notifier.dart';
import '../../../infrastructure/notifiers/translation_notifier.dart';
import '../../../infrastructure/utils/color_pallette.dart';
import '../../../infrastructure/utils/text_style_palette.dart';

class InputBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final translationNotifier = context.watch<TranslationNotifier>();
    final searchNotifier = context.watch<SearchNotifier>();

    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: SizedBox(
        height: 48,
        child: TextField(
          onTapOutside: (event) => FocusScope.of(context).unfocus(),
          // controller: transactionNotifier.shopEditingController,
          keyboardType: TextInputType.text,
          onChanged: searchNotifier.search,
          textAlignVertical: TextAlignVertical.center,
          cursorColor: ColorPalette.textBlack,
          style: TextStylePalette.blackAkko16w400,
          autocorrect: false,
          decoration: InputDecoration(
            hintText: searchNotifier.isStoreSearch
                ? translationNotifier.getText('findStorePrompt')
                : searchNotifier.isCategorySearch
                    ? 'Vind Categorie'
                    : translationNotifier.getText('findProductPrompt'),
            contentPadding: EdgeInsets.only(bottom: 24, left: 16),
            prefixIcon: Icon(Icons.search_rounded),
            enabledBorder: OutlineInputBorder(
              borderSide: const BorderSide(color: ColorPalette.aqua300),
              borderRadius: BorderRadius.circular(4),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: const BorderSide(color: ColorPalette.aqua300),
              borderRadius: BorderRadius.circular(4),
            ),
          ),
        ),
      ),
    );
  }
}
