import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../infrastructure/dtos/search_match_dto.dart';
import '../../../infrastructure/notifiers/search_notifier.dart';
import '../../../infrastructure/notifiers/translation_notifier.dart';
import '../../../infrastructure/utils/color_pallette.dart';
import '../../../infrastructure/utils/extensions.dart';
import '../../../infrastructure/utils/icon_map.dart';
import '../../../infrastructure/utils/text_style_palette.dart';
import '../../../infrastructure/utils/ui_scalers.dart';

class SearchResults extends StatelessWidget {
  List<SearchMatchDto> getSearchMatchDtos(SearchNotifier searchNotifier) {
    if (searchNotifier.matches.isNotEmpty) return searchNotifier.matches;
    if (searchNotifier.recentSuggestions) return searchNotifier.recent;
    return searchNotifier.frequent;
  }

  @override
  Widget build(BuildContext context) {
    final searchMatchDtos = getSearchMatchDtos(context.watch<SearchNotifier>());
    final translationNotifier = context.watch<TranslationNotifier>();
    final searchNotifier = context.watch<SearchNotifier>();
    final scrollControler = ScrollController();
    return searchMatchDtos.length > 0
        ? Expanded(
            child: ListView.builder(
              controller: scrollControler,
              itemCount: searchMatchDtos.length,
              itemBuilder: (_, index) => _SearchResultTile(searchMatchDtos[index]),
            ),
          )
        : Expanded(
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Icon(
                      searchNotifier.isStoreSearch
                          ? Icons.store_rounded
                          : searchNotifier.isCategorySearch
                              ? Icons.category_rounded
                              : Icons.shopping_bag_rounded,
                      size: 64,
                      color: ColorPalette.grey300,
                    ),
                    Text(
                      searchNotifier.isStoreSearch
                          ? translationNotifier.getText('storeSearchEmptyText')
                          : searchNotifier.isCategorySearch
                              ? translationNotifier.getText('categorySearchEmptyText')
                              : translationNotifier.getText('productSearchEmptyText'),
                      textAlign: TextAlign.center,
                      softWrap: true,
                      style: TextStylePalette.primaryAkko16w400,
                    )
                  ],
                ),
              ),
            ),
          );
  }
}

class _SearchResultTile extends StatelessWidget {
  final SearchMatchDto searchMatchDto;

  const _SearchResultTile(this.searchMatchDto);

  @override
  Widget build(BuildContext context) {
    final searchNotifier = context.watch<SearchNotifier>();
    return Column(
      children: [
        InkWell(
          onTap: () => Navigator.pop(context, searchMatchDto),
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                if (searchNotifier.isStoreSearch)
                  Container(
                    padding: EdgeInsets.all(12 * x),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: ColorPalette.brand,
                    ),
                    child: IconMap.getStoreIcon(searchMatchDto.category),
                  ),
                if (searchNotifier.isStoreSearch)
                  Container(
                    width: 16 * x,
                  ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        child: Text(
                          searchMatchDto.name.capitalize(),
                          style: TextStylePalette.primaryAkko16w500,
                        ),
                      ),
                      Container(
                        child: Text(
                          searchMatchDto.category.capitalize(),
                          overflow: TextOverflow.ellipsis,
                          style: TextStylePalette.greyAkko14w400,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
        Divider(
          height: 1,
        ),
      ],
    );
  }
}
