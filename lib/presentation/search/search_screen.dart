// Features I need:
// UI box that let's you enter a search string
import 'package:flutter/material.dart';
import 'package:household_budget_survey/infrastructure/utils/color_pallette.dart';

import 'widgets/input_bar.dart';
import 'widgets/search_results.dart';

class SearchScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorPalette.backgroundWhite,
      body: SafeArea(
        child: Column(
          children: [
            InputBar(),
            SearchResults(),
          ],
        ),
      ),
    );
  }
}
