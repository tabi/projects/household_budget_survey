import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../infrastructure/notifiers/translation_notifier.dart';
import '../../infrastructure/utils/color_pallette.dart';
import '../../infrastructure/utils/text_style_palette.dart';
import '../modal_bottomsheets/add_new_receipt_drawer.dart';
import 'widgets/receipt_list_widget.dart';
import 'widgets/search_filter_widget.dart';

class ExpensesScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final translationNotifier = context.watch<TranslationNotifier>();

    return Scaffold(
      appBar: AppBar(
        scrolledUnderElevation: 0.0,
        leadingWidth: 0,
        centerTitle: false,
        backgroundColor: ColorPalette.backgroundWhite,
        title: Text(
          translationNotifier.getText('allExpenses'),
          style: TextStylePalette.brandSoho26w600,
        ),
        actions: [
          IconButton(
            icon: Icon(
              Icons.add_rounded,
              color: ColorPalette.brand,
            ),
            onPressed: () => getModalAddDrawer(context, translationNotifier),
          ),
        ],
      ),
      backgroundColor: ColorPalette.backgroundWhite,
      body: Column(
        children: <Widget>[
          // SearchFilterWidget(),
          ReceiptListWidget(),
        ],
      ),
    );
  }
}
