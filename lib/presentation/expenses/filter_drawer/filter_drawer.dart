import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../infrastructure/notifiers/filter_notifier.dart';
import '../../../infrastructure/notifiers/translation_notifier.dart';
import '../../../infrastructure/utils/color_pallette.dart';
import '../../../infrastructure/utils/text_style_palette.dart';
import 'widgets/datefilter_widget.dart';
import 'widgets/expensetypefilter_widget.dart';
import 'widgets/pricefilter_widget.dart';
import 'widgets/sortingfilter_widget.dart';

class FilterDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final filterNotifier = context.watch<FilterNotifier>();
    final translationNotifier = context.watch<TranslationNotifier>();
    return Scaffold(
      backgroundColor: ColorPalette.backgroundWhite,
      appBar: AppBar(
        scrolledUnderElevation: 0.0,
        bottom: PreferredSize(
            child: Container(
              color: ColorPalette.grey200,
              height: 1.0,
            ),
            preferredSize: Size.fromHeight(4.0)),
        leading: IconButton(
          icon: Icon(
            Icons.close_rounded,
            color: ColorPalette.brand,
          ),
          onPressed: () {
            filterNotifier.undoFiltersOnClose();
            Navigator.of(context).pop();
          },
        ),
        centerTitle: true,
        backgroundColor: ColorPalette.backgroundWhite,
        title: Text(
          translationNotifier.getText('expensesFilterAndSort'),
          style: TextStylePalette.brandSoho16w600,
        ),
      ),
      body: SafeArea(
        child: Column(
          children: [
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    SortingFilterWidget(),
                    Divider(),
                    DateFilterWidget(),
                    Divider(),
                    PriceFilterWidget(),
                    Divider(),
                    ExpenseTypeFilterWidget(),
                  ],
                ),
              ),
            ),
            Divider(),
            Container(
              padding: EdgeInsets.all(16),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(
                    flex: 1,
                    child: SizedBox(
                      height: 48,
                      width: double.infinity,
                      child: OutlinedButton(
                        onPressed: () {
                          filterNotifier.clearAllFilters();
                        },
                        style: OutlinedButton.styleFrom(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(4.0),
                          ),
                          foregroundColor: ColorPalette.aqua300,
                          side: BorderSide(
                            color: ColorPalette.aqua300,
                          ),
                        ),
                        child: Text(
                          translationNotifier.getText('deleteFilters'),
                          style: TextStylePalette.aquaAkko16w400,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    width: 16,
                  ),
                  Expanded(
                    flex: 1,
                    child: SizedBox(
                      height: 48,
                      width: double.infinity,
                      child: TextButton(
                        style: TextButton.styleFrom(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(4.0),
                          ),
                          foregroundColor: ColorPalette.backgroundWhite,
                          backgroundColor: ColorPalette.aqua200,
                          minimumSize: Size.fromHeight(44),
                        ),
                        onPressed: () {
                          filterNotifier.saveFilters();
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          translationNotifier.getText('showResults'),
                          style: TextStylePalette.whiteAkko16w400,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
