import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../../infrastructure/notifiers/filter_notifier.dart';
import '../../../../infrastructure/notifiers/translation_notifier.dart';
import '../../../../infrastructure/utils/color_pallette.dart';
import '../../../../infrastructure/utils/text_style_palette.dart';

class DateFilterWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final filterNotifier = context.watch<FilterNotifier>();
    final translationNotifier = context.watch<TranslationNotifier>();

    return Container(
      padding: EdgeInsets.all(16),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            translationNotifier.getText('date'),
            style: TextStylePalette.primarySoho16w600,
          ),
          Container(
            height: 16,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                translationNotifier.getText('from'),
                style: TextStylePalette.blackAkko16w600,
              ),
              Container(
                height: 8,
              ),
              SizedBox(
                height: 48,
                child: TextField(
                  onTapOutside: (event) => FocusScope.of(context).unfocus(),
                  controller: filterNotifier.fromDateController,
                  onTap: () => filterNotifier.onTapFromDateFunction(context: context),
                  textAlignVertical: TextAlignVertical.center,
                  cursorColor: ColorPalette.textBlack,
                  style: TextStylePalette.blackAkko16w400,
                  autocorrect: false,
                  readOnly: true,
                  decoration: InputDecoration(
                    hintText: translationNotifier.getText('selectStartDate'),
                    suffixIcon: Icon(
                      Icons.calendar_month_rounded,
                      size: 22,
                      color: Colors.black,
                    ),
                    contentPadding: EdgeInsets.only(bottom: 24, left: 16),
                    enabledBorder: OutlineInputBorder(
                      borderSide: const BorderSide(color: ColorPalette.aqua300),
                      borderRadius: BorderRadius.circular(4),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: const BorderSide(color: ColorPalette.aqua300),
                      borderRadius: BorderRadius.circular(4),
                    ),
                  ),
                ),
              ),
              Container(
                height: 16,
              ),
              Text(
                translationNotifier.getText('to'),
                style: TextStylePalette.blackAkko16w600,
              ),
              Container(
                height: 8,
              ),
              SizedBox(
                height: 48,
                child: TextField(
                  controller: filterNotifier.toDateController,
                  onTap: () => filterNotifier.onTapToDateFunction(context: context),
                  textAlignVertical: TextAlignVertical.center,
                  cursorColor: ColorPalette.textBlack,
                  style: TextStylePalette.blackAkko16w400,
                  autocorrect: false,
                  readOnly: true,
                  decoration: InputDecoration(
                    hintText: translationNotifier.getText('selectEndDate'),
                    suffixIcon: Icon(
                      Icons.calendar_month_rounded,
                      size: 22,
                      color: Colors.black,
                    ),
                    contentPadding: EdgeInsets.only(bottom: 24, left: 16),
                    enabledBorder: OutlineInputBorder(
                      borderSide: const BorderSide(color: ColorPalette.aqua300),
                      borderRadius: BorderRadius.circular(4),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: const BorderSide(color: ColorPalette.aqua300),
                      borderRadius: BorderRadius.circular(4),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
