import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

import '../../../../infrastructure/notifiers/filter_notifier.dart';
import '../../../../infrastructure/notifiers/translation_notifier.dart';
import '../../../../infrastructure/utils/color_pallette.dart';
import '../../../../infrastructure/utils/text_style_palette.dart';

class PriceFilterWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final filterNotifier = context.watch<FilterNotifier>();
    final translationNotifier = context.watch<TranslationNotifier>();

    return Container(
      padding: EdgeInsets.all(16),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            translationNotifier.getText('priceCurrency'),
            style: TextStylePalette.primarySoho16w600,
          ),
          Container(
            height: 8,
          ),
          SizedBox(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                RangeSlider(
                  activeColor: ColorPalette.aqua300,
                  inactiveColor: ColorPalette.grey200,
                  min: filterNotifier.minPrice,
                  max: filterNotifier.maxPrice,
                  values: filterNotifier.priceValues,
                  onChanged: (values) {
                    filterNotifier.priceValues = values;
                  },
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 1,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            translationNotifier.getText('from'),
                            style: TextStylePalette.blackAkko16w600,
                          ),
                          Container(
                            height: 8,
                          ),
                          SizedBox(
                            height: 48,
                            child: TextField(
                              controller: filterNotifier.fromPriceController,
                              onTapOutside: (event) => FocusScope.of(context).unfocus(),
                              onChanged: (value) {
                                RangeValues oldRange = filterNotifier.priceValues;
                                if (double.tryParse(value) != null) {
                                  double parsedValue = double.parse(value);
                                  if (parsedValue >= filterNotifier.minPrice &&
                                      parsedValue <= filterNotifier.maxPrice &&
                                      filterNotifier.priceValues.start <= filterNotifier.priceValues.end) {
                                    RangeValues newRange = RangeValues(parsedValue, oldRange.end);
                                    filterNotifier.priceValues = newRange;
                                    filterNotifier.fromPriceController.text = value;
                                  } else if (parsedValue <= filterNotifier.minPrice) {
                                    RangeValues newRange = RangeValues(filterNotifier.minPrice, oldRange.end);
                                    filterNotifier.priceValues = newRange;
                                    filterNotifier.fromPriceController.text =
                                        filterNotifier.minPrice.round().toString();
                                  } else {
                                    filterNotifier.fromPriceController.text = oldRange.start.round().toString();
                                  }
                                }
                              },
                              textAlignVertical: TextAlignVertical.center,
                              cursorColor: ColorPalette.textBlack,
                              style: TextStylePalette.blackAkko16w400,
                              autocorrect: false,
                              readOnly: false,
                              keyboardType: TextInputType.numberWithOptions(signed: true, decimal: true),
                              inputFormatters: <TextInputFormatter>[FilteringTextInputFormatter.digitsOnly],
                              decoration: InputDecoration(
                                hintText: filterNotifier.minPrice.round().toString(),
                                contentPadding: EdgeInsets.only(bottom: 24, left: 16),
                                enabledBorder: OutlineInputBorder(
                                  borderSide: const BorderSide(color: ColorPalette.aqua300),
                                  borderRadius: BorderRadius.circular(4),
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: const BorderSide(color: ColorPalette.aqua300),
                                  borderRadius: BorderRadius.circular(4),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      width: 16,
                    ),
                    Expanded(
                      flex: 1,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            translationNotifier.getText('to'),
                            style: TextStylePalette.blackAkko16w600,
                          ),
                          Container(
                            height: 8,
                          ),
                          SizedBox(
                            height: 48,
                            child: TextField(
                              controller: filterNotifier.toPriceController,
                              onTapOutside: (event) => FocusScope.of(context).unfocus(),
                              onChanged: (value) {
                                RangeValues oldRange = filterNotifier.priceValues;
                                if (double.tryParse(value) != null) {
                                  double parsedValue = double.parse(value);
                                  if (parsedValue >= filterNotifier.minPrice &&
                                      parsedValue <= filterNotifier.maxPrice &&
                                      filterNotifier.priceValues.start >= filterNotifier.priceValues.end) {
                                    RangeValues newRange = RangeValues(oldRange.start, parsedValue);
                                    filterNotifier.priceValues = newRange;
                                    filterNotifier.toPriceController.text = value;
                                  } else if (parsedValue >= filterNotifier.maxPrice) {
                                    RangeValues newRange = RangeValues(oldRange.start, filterNotifier.maxPrice);
                                    filterNotifier.priceValues = newRange;
                                    filterNotifier.toPriceController.text = filterNotifier.maxPrice.round().toString();
                                  } else {
                                    filterNotifier.toPriceController.text = oldRange.end.round().toString();
                                  }
                                }
                              },
                              textAlignVertical: TextAlignVertical.center,
                              cursorColor: ColorPalette.textBlack,
                              style: TextStylePalette.blackAkko16w400,
                              autocorrect: false,
                              keyboardType: TextInputType.numberWithOptions(signed: true, decimal: true),
                              inputFormatters: <TextInputFormatter>[FilteringTextInputFormatter.digitsOnly],
                              decoration: InputDecoration(
                                hintText: filterNotifier.maxPrice.round().toString(),
                                contentPadding: EdgeInsets.only(bottom: 24, left: 16),
                                enabledBorder: OutlineInputBorder(
                                  borderSide: const BorderSide(color: ColorPalette.aqua300),
                                  borderRadius: BorderRadius.circular(4),
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: const BorderSide(color: ColorPalette.aqua300),
                                  borderRadius: BorderRadius.circular(4),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
