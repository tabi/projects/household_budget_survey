import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../../infrastructure/notifiers/filter_notifier.dart';
import '../../../../infrastructure/notifiers/translation_notifier.dart';
import '../../../../infrastructure/utils/color_pallette.dart';
import '../../../../infrastructure/utils/text_style_palette.dart';

class SortingFilterWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final filterNotifier = context.watch<FilterNotifier>();
    final translationNotifier = context.watch<TranslationNotifier>();
    return Container(
      padding: EdgeInsets.all(16),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            translationNotifier.getText('sortBy'),
            style: TextStylePalette.primarySoho16w600,
          ),
          Container(
            height: 8,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              RadioListTile(
                title: Text(
                  translationNotifier.getText('dateNewOld'),
                  style: TextStylePalette.blackAkko16w500,
                ),
                value: SortOrder.dateNewOld,
                groupValue: filterNotifier.selectedOrder,
                onChanged: (value) => filterNotifier.selectedOrder = value!,
                activeColor: ColorPalette.aqua300,
              ),
              RadioListTile(
                title: Text(
                  translationNotifier.getText('dateOldNew'),
                  style: TextStylePalette.blackAkko16w500,
                ),
                value: SortOrder.dateOldNew,
                groupValue: filterNotifier.selectedOrder,
                onChanged: (value) => filterNotifier.selectedOrder = value!,
                activeColor: ColorPalette.aqua300,
              ),
              RadioListTile(
                title: Text(
                  translationNotifier.getText('priceHighLow'),
                  style: TextStylePalette.blackAkko16w500,
                ),
                value: SortOrder.priceHighLow,
                groupValue: filterNotifier.selectedOrder,
                onChanged: (value) => filterNotifier.selectedOrder = value!,
                activeColor: ColorPalette.aqua300,
              ),
              RadioListTile(
                title: Text(
                  translationNotifier.getText('priceLowHigh'),
                  style: TextStylePalette.blackAkko16w500,
                ),
                value: SortOrder.priceLowHigh,
                groupValue: filterNotifier.selectedOrder,
                onChanged: (value) => filterNotifier.selectedOrder = value!,
                activeColor: ColorPalette.aqua300,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
