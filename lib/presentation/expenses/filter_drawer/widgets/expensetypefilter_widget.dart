import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../../infrastructure/notifiers/filter_notifier.dart';
import '../../../../infrastructure/notifiers/translation_notifier.dart';
import '../../../../infrastructure/utils/color_pallette.dart';
import '../../../../infrastructure/utils/text_style_palette.dart';

class ExpenseTypeFilterWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final filterNotifier = context.watch<FilterNotifier>();
    final translationNotifier = context.watch<TranslationNotifier>();

    return Container(
      padding: EdgeInsets.all(16),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            translationNotifier.getText('sortBy'),
            style: TextStylePalette.primarySoho16w600,
          ),
          Container(
            height: 8,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CheckboxListTile(
                title: Text(
                  translationNotifier.getText('manual'),
                  style: TextStylePalette.blackAkko16w500,
                ),
                value: filterNotifier.expenseManualChecked,
                onChanged: (newValue) {
                  filterNotifier.manualChecked = newValue!;
                },
                activeColor: ColorPalette.aqua300,
                controlAffinity: ListTileControlAffinity.leading,
              ),
              CheckboxListTile(
                title: Text(
                  translationNotifier.getText('scan'),
                  style: TextStylePalette.blackAkko16w500,
                ),
                value: filterNotifier.expenseScannedChecked,
                onChanged: (newValue) {
                  filterNotifier.scannedChecked = newValue!;
                },
                activeColor: ColorPalette.aqua300,
                controlAffinity: ListTileControlAffinity.leading,
              ),
              CheckboxListTile(
                title: Text(
                  translationNotifier.getText('digital'),
                  style: TextStylePalette.blackAkko16w500,
                ),
                value: filterNotifier.expenseDigitalChecked,
                onChanged: (newValue) {
                  filterNotifier.digitalChecked = newValue!;
                },
                activeColor: ColorPalette.aqua300,
                controlAffinity: ListTileControlAffinity.leading,
              )
            ],
          ),
        ],
      ),
    );
  }
}
