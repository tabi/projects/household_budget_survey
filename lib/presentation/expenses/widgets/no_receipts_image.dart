import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../infrastructure/notifiers/overview_notifiers/overview_notifier.dart';
import '../../../infrastructure/notifiers/receipt_list_notifier.dart';
import '../../../infrastructure/notifiers/translation_notifier.dart';
import '../../../infrastructure/utils/color_pallette.dart';
import '../../../infrastructure/utils/text_style_palette.dart';
import '../../modal_bottomsheets/add_new_receipt_drawer.dart';

class NoReceiptImage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final translationNotifier = context.watch<TranslationNotifier>();
    final searchFieldNotifier = context.watch<ReceiptListNotifier>();
    final overviewNotifier = context.watch<OverviewNotifier>();

    return Expanded(
      child: Container(
        color: ColorPalette.backgroundOffWhite,
        padding: const EdgeInsets.all(16.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Icon(
              Icons.receipt_rounded,
              size: 64,
              color: ColorPalette.textGrey,
            ),
            Container(height: 16),
            Text(
              translationNotifier.getText('noExpensesFoundTitle'),
              style: TextStylePalette.blackSoho16w600,
            ),
            Container(height: 8),
            Padding(
              padding: const EdgeInsets.only(left: 8.0, right: 8.0),
              child: Text(
                searchFieldNotifier.searchFieldController.text.isEmpty
                    ? translationNotifier.getText('noExpensesFoundMessage')
                    : translationNotifier.getText('noExpensesFoundWithStringMessage') +
                        ' \'${searchFieldNotifier.searchFieldController.text}\'.',
                textAlign: TextAlign.center,
                style: TextStylePalette.primaryAkko16w500,
              ),
            ),
            Container(height: 24),
            SizedBox(
              height: 48,
              child: TextButton(
                style: TextButton.styleFrom(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(4.0),
                  ),
                  foregroundColor: Colors.white,
                  backgroundColor: ColorPalette.aqua200,
                  minimumSize: Size.fromHeight(44),
                ),
                onPressed: () => getModalAddDrawer(
                  context,
                  translationNotifier,
                  overviewNotifier.selectedDay,
                ),
                child: Text(
                  translationNotifier.getText('addExpense'),
                  style: TextStylePalette.whiteAkko16w400,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
