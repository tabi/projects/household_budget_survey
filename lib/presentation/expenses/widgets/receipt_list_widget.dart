import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../infrastructure/dtos/receipt_dto.dart';
import '../../../infrastructure/notifiers/receipt_list_notifier.dart';
import '../../../infrastructure/utils/color_pallette.dart';
import '../../../infrastructure/utils/date_util.dart';
import 'date_header_widget.dart';
import 'no_receipts_image.dart';
import 'receipt_tile_widget.dart';

class ReceiptListWidget extends StatelessWidget {
  List<Widget> annotatedReceiptsWithDates(List<ReceiptDto> receiptDtos) {
    DateTime? _lastDate;
    return receiptDtos.expand((receiptDto) {
      final widgets = <Widget>[];
      if (_lastDate == null || isSameDay(_lastDate!, receiptDto.boughtOn) == false) {
        widgets.add(Container(
          height: 8,
        ));
        _lastDate = receiptDto.boughtOn;
        widgets.add(DateHeaderWidget(receiptDto.boughtOn));
      }
      widgets.add(ReceiptTileWidget(receiptDto));
      return widgets;
    }).toList();
  }

  @override
  Widget build(BuildContext context) {
    final receiptListNotifier = context.watch<ReceiptListNotifier>();
    if (receiptListNotifier.transactions.isEmpty) return NoReceiptImage();
    final annotatedReceipts = annotatedReceiptsWithDates(receiptListNotifier.transactions);
    return Expanded(
      child: Container(
        color: ColorPalette.backgroundOffWhite,
        child: ListView.builder(
          itemCount: annotatedReceipts.length,
          itemBuilder: (BuildContext context, int index) {
            return annotatedReceipts[index];
          },
        ),
      ),
    );
  }
}
