import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import '../../../infrastructure/notifiers/translation_notifier.dart';
import '../../../infrastructure/utils/color_pallette.dart';
import '../../../infrastructure/utils/date_util.dart';
import '../../../infrastructure/utils/text_style_palette.dart';

class DateHeaderWidget extends StatelessWidget {
  const DateHeaderWidget(this.date);

  final DateTime date;

  String getHeaderString(TranslationNotifier translationNotifier) {
    if (isSameDay(date, DateTime.now())) {
      return translationNotifier.getText('today');
    } else if (isSameDay(date, DateTime.now().subtract(const Duration(days: 1)))) {
      return translationNotifier.getText('yesterday');
    } else {
      return DateFormat('d MMMM', translationNotifier.languageCode).format(date);
    }
  }

  @override
  Widget build(BuildContext context) {
    final translationNotifier = context.watch<TranslationNotifier>();
    return Container(
      color: ColorPalette.backgroundWhite,
      child: Padding(
        padding: EdgeInsets.only(left: 16.0, right: 16, top: 16, bottom: 8),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text(
              getHeaderString(translationNotifier),
              style: TextStylePalette.greyAkko14w400,
            ),
          ],
        ),
      ),
    );
  }
}
