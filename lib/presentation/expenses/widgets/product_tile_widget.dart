import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../infrastructure/dtos/product_dto.dart';
import '../../../infrastructure/notifiers/translation_notifier.dart';
import '../../../infrastructure/utils/extensions.dart';
import '../../../infrastructure/utils/text_style_palette.dart';
import '../../../infrastructure/utils/ui_scalers.dart';

class ProductTileWidget extends StatelessWidget {
  final ProductDto productDto;

  const ProductTileWidget(this.productDto);

  @override
  Widget build(BuildContext context) {
    final translationNotifier = context.watch<TranslationNotifier>();
    return Padding(
      padding: EdgeInsets.only(top: 8.0 * y, bottom: 8.0 * y, left: 0 * x),
      child: Column(
        children: [
          Row(
            children: <Widget>[
              SizedBox(
                width: 70 * x,
                child: Column(children: <Widget>[
                  Text(
                    '${productDto.count}x',
                    style: TextStylePalette.primaryAkko14w600,
                  ),
                  Text(
                    productDto.getPrice().toStringAsFixed(2).currencyFormat(translationNotifier),
                    style: TextStylePalette.primaryAkko14w600,
                  ),
                ]),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    width: 225 * x,
                    child: Text(
                      productDto.name!,
                      style: TextStylePalette.primaryAkko14w600,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                  SizedBox(height: 5 * y),
                  SizedBox(
                    width: 225 * x,
                    child: Text(
                      productDto.coicopName ?? '',
                      style: TextStylePalette.greyAkko14w600,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                ],
              ),
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        Text(productDto.getPrice().toStringAsFixed(2).currencyFormat(translationNotifier),
                            style: TextStylePalette.redAkko16w400),
                        SizedBox(height: 5 * y),
                      ],
                    ),
                    SizedBox(width: 30 * x),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
