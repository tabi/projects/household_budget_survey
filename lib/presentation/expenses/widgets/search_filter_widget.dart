import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../infrastructure/notifiers/filter_notifier.dart';
import '../../../infrastructure/notifiers/receipt_list_notifier.dart';
import '../../../infrastructure/notifiers/translation_notifier.dart';
import '../../../infrastructure/utils/color_pallette.dart';
import '../../../infrastructure/utils/text_style_palette.dart';
import '../filter_drawer/filter_drawer.dart';

class SearchFilterWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final translationNotifier = context.watch<TranslationNotifier>();
    final receiptListNotifier = context.watch<ReceiptListNotifier>();
    final filterNotifier = context.watch<FilterNotifier>();

    return Padding(
      padding: EdgeInsets.all(16),
      child: Column(
        children: [
          Container(
            color: ColorPalette.backgroundWhite,
            height: 48,
            child: TextField(
              onTapOutside: (event) => FocusScope.of(context).unfocus(),
              controller: receiptListNotifier.searchFieldController,
              onChanged: (value) => receiptListNotifier.updateSearchField(value),
              textAlignVertical: TextAlignVertical.center,
              cursorColor: ColorPalette.textBlack,
              style: TextStylePalette.blackAkko16w400,
              autocorrect: false,
              decoration: InputDecoration(
                prefixIcon: Icon(
                  Icons.search_rounded,
                ),
                hintText: translationNotifier.getText('expensesSearchHintText'),
                suffixIcon: receiptListNotifier.searchInput.isEmpty
                    ? Icon(null)
                    : IconButton(
                        icon: Icon(
                          Icons.close_rounded,
                          size: 22,
                        ),
                        onPressed: () => receiptListNotifier.clearSearchField(),
                      ),
                contentPadding: EdgeInsets.all(14.0),
                enabledBorder: OutlineInputBorder(
                  borderSide: const BorderSide(color: ColorPalette.aqua300),
                  borderRadius: BorderRadius.circular(4),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: const BorderSide(
                    color: Colors.black,
                    width: 2,
                  ),
                  borderRadius: BorderRadius.circular(4),
                ),
              ),
            ),
          ),
          Container(
            height: 16,
          ),
          SizedBox(
            height: 48,
            child: OutlinedButton(
              style: OutlinedButton.styleFrom(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(4.0),
                ),
                foregroundColor: ColorPalette.aqua300,
                minimumSize: Size.fromHeight(44),
                side: BorderSide(
                  color: ColorPalette.aqua300,
                ),
              ),
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => FilterDrawer(),
                  ),
                );
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Icon(Icons.tune_rounded),
                  Container(
                    width: 12,
                  ),
                  Text(
                    translationNotifier.getText('expensesFilterAndSort'),
                    style: TextStylePalette.aquaAkko16w400,
                  ),
                  Container(
                    width: 12,
                  ),
                  if (filterNotifier.selectedFilterCount > 0)
                    CircleAvatar(
                      backgroundColor: ColorPalette.aqua300,
                      radius: 14,
                      child: Container(
                        padding: EdgeInsets.only(top: 2),
                        child: Text(
                          filterNotifier.selectedFilterCount.toString(),
                          style: TextStylePalette.whiteSoho12w400,
                        ),
                      ),
                    ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
