import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../infrastructure/dtos/receipt_dto.dart';
import '../../../infrastructure/notifiers/receipt_list_notifier.dart';
import '../../../infrastructure/notifiers/translation_notifier.dart';
import '../../../infrastructure/utils/color_pallette.dart';
import '../../../infrastructure/utils/extensions.dart';
import '../../../infrastructure/utils/icon_map.dart';
import '../../../infrastructure/utils/text_style_palette.dart';
import '../../../infrastructure/utils/ui_scalers.dart';

class ReceiptTileWidget extends StatelessWidget {
  final ReceiptDto receiptDto;

  const ReceiptTileWidget(this.receiptDto);

  @override
  Widget build(BuildContext context) {
    final translationNotifier = context.watch<TranslationNotifier>();
    final receiptListNotifier = context.watch<ReceiptListNotifier>();
    return Dismissible(
      background: Container(
          padding: EdgeInsets.symmetric(horizontal: 16 * x),
          alignment: Alignment.centerRight,
          child: Icon(
            Icons.delete_rounded,
            color: Colors.white,
          ),
          color: Colors.red),
      key: Key(receiptDto.uuid),
      direction: DismissDirection.endToStart,
      onDismissed: (direction) {
        receiptListNotifier.deleteReceipt(receiptDto);
      },
      child: GestureDetector(
        onTap: () {
          Navigator.pushNamed(context, 'addManualReceipt', arguments: receiptDto);
        },
        child: Container(
          color: ColorPalette.backgroundWhite,
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.all(16),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      padding: EdgeInsets.all(12 * x),
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: ColorPalette.brand,
                      ),
                      child: IconMap.getStoreIcon(receiptDto.shopCategory),
                    ),
                    Container(
                      width: 16 * x,
                    ),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            receiptDto.shopName != null
                                ? receiptDto.shopName!.capitalize()
                                : translationNotifier.getText('unknown'),
                            style: TextStylePalette.primaryAkko16w400,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              RichText(
                                text: TextSpan(
                                  children: [
                                    TextSpan(
                                      text: receiptDto.products.length.toString() +
                                          ' ' +
                                          translationNotifier.getText('productsOrServices'),
                                      style: TextStylePalette.greyAkko14w400,
                                    ),
                                    WidgetSpan(
                                        alignment: PlaceholderAlignment.baseline,
                                        baseline: TextBaseline.alphabetic,
                                        child: SizedBox(width: 10 * x)),
                                    if (receiptDto.imagePath != null)
                                      TextSpan(
                                        text: '•',
                                        style: TextStylePalette.greyAkko14w400,
                                      ),
                                    if (receiptDto.imagePath != null)
                                      WidgetSpan(
                                          alignment: PlaceholderAlignment.baseline,
                                          baseline: TextBaseline.alphabetic,
                                          child: SizedBox(width: 10 * x)),
                                    if (receiptDto.imagePath != null)
                                      WidgetSpan(
                                        alignment: PlaceholderAlignment.middle,
                                        child: Icon(
                                          Icons.camera_alt_rounded,
                                          size: 12 * f,
                                          color: ColorPalette.textGrey,
                                        ),
                                      ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    Text(
                      receiptDto.getTotalPrice().toStringAsFixed(2).currencyFormat(translationNotifier),
                      style: TextStylePalette.blackAkko16w400,
                    ),
                  ],
                ),
              ),
              Divider(
                color: ColorPalette.grey100,
                height: 1,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
