import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../../infrastructure/notifiers/translation_notifier.dart';
import '../../../../infrastructure/utils/color_pallette.dart';
import '../../../../infrastructure/utils/text_style_palette.dart';

class ProductDiscountField extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final translationNotifier = context.watch<TranslationNotifier>();
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          translationNotifier.getText('productDetailDiscountFieldTitle'),
          style: TextStylePalette.blackAkko16w600,
        ),
        Container(
          height: 8,
        ),
        SizedBox(
          height: 48,
          child: TextField(
            onTapOutside: (event) => FocusScope.of(context).unfocus(),
            //controller: transactionNotifier.dateEditingController,
            onTap: () {},
            textAlignVertical: TextAlignVertical.center,
            cursorColor: ColorPalette.textBlack,
            style: TextStylePalette.blackAkko16w400,

            autocorrect: false,
            readOnly: true,
            decoration: InputDecoration(
              hintText: translationNotifier.getText('productDetailDiscountFieldHintText'),
              contentPadding: EdgeInsets.only(bottom: 24, left: 16),
              enabledBorder: OutlineInputBorder(
                borderSide: const BorderSide(color: ColorPalette.aqua300),
                borderRadius: BorderRadius.circular(4),
              ),
              focusedBorder: OutlineInputBorder(
                borderSide: const BorderSide(color: ColorPalette.aqua300),
                borderRadius: BorderRadius.circular(4),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
