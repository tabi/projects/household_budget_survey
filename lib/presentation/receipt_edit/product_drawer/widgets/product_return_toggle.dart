import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../../infrastructure/notifiers/product_notifier.dart';
import '../../../../infrastructure/notifiers/translation_notifier.dart';
import '../../../../infrastructure/utils/color_pallette.dart';
import '../../../../infrastructure/utils/text_style_palette.dart';

class ProductReturnToggle extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final translationNotifier = context.watch<TranslationNotifier>();
    final productNotifier = context.watch<ProductNotifier>();
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        CheckboxListTile(
          title: Text(
            translationNotifier.getText('productDetailReturnCheckboxText'),
            style: TextStylePalette.blackAkko16w500,
          ),
          contentPadding: EdgeInsets.zero,
          value: productNotifier.isReturn,
          onChanged: (newValue) {
            if (newValue != null) productNotifier.setReturn(newValue);
          },
          activeColor: ColorPalette.aqua300,
          controlAffinity: ListTileControlAffinity.leading,
        ),
      ],
    );
  }
}
