import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../../infrastructure/notifiers/product_notifier.dart';
import '../../../../infrastructure/notifiers/translation_notifier.dart';
import '../../../../infrastructure/utils/color_pallette.dart';
import '../../../../infrastructure/utils/text_style_palette.dart';

class ProductPriceField extends StatelessWidget {
  bool isValidInput(String newPrice) {
    try {
      final _price = double.tryParse(newPrice);
      if (_price == null) return false;
      if (_price > -100000 && _price < 100000) {
        return true;
      }
    } on Exception {
      print('Input error: $Exception');
    }
    return false;
  }

  @override
  Widget build(BuildContext context) {
    final translationNotifier = context.watch<TranslationNotifier>();
    final productNotifier = context.watch<ProductNotifier>();
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          translationNotifier.getText('productDetailPriceFieldTitle'),
          style: TextStylePalette.blackAkko16w600,
        ),
        Container(
          height: 8,
        ),
        SizedBox(
          height: 48,
          child: TextField(
            onTapOutside: (event) => FocusScope.of(context).unfocus(),
            controller: productNotifier.priceController,
            onChanged: (String newPrice) {
              newPrice = newPrice.replaceAll(',', '.');
              if (isValidInput(newPrice)) {
                productNotifier.setPrice(double.parse(newPrice));
              } else {
                productNotifier.setPrice(null);
              }
            },
            keyboardType: TextInputType.numberWithOptions(signed: true, decimal: true),
            textAlignVertical: TextAlignVertical.center,
            cursorColor: ColorPalette.textBlack,
            style: TextStylePalette.blackAkko16w400,
            autocorrect: false,
            decoration: InputDecoration(
              prefixIcon: Padding(
                padding: EdgeInsets.only(top: 14.0),
                child: Text(
                  translationNotifier.getText('currencySymbol'),
                  textAlign: TextAlign.center,
                  style: TextStylePalette.blackAkko16w400,
                ),
              ),
              contentPadding: EdgeInsets.only(bottom: 24, left: 16),
              enabledBorder: OutlineInputBorder(
                borderSide: (!productNotifier.isComplete() && productNotifier.pressedSave)
                    ? BorderSide(color: ColorPalette.warningRed)
                    : BorderSide(color: ColorPalette.aqua300),
                borderRadius: BorderRadius.circular(4),
              ),
              focusedBorder: OutlineInputBorder(
                borderSide: const BorderSide(color: ColorPalette.aqua300),
                borderRadius: BorderRadius.circular(4),
              ),
            ),
          ),
        ),
        if (!productNotifier.isComplete() && productNotifier.pressedSave)
          Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: RichText(
              text: TextSpan(
                children: [
                  WidgetSpan(
                    child: Icon(
                      Icons.warning_rounded,
                      size: 16,
                      color: ColorPalette.warningRed,
                    ),
                  ),
                  WidgetSpan(
                      child: Container(
                    width: 6,
                  )),
                  TextSpan(
                    text: translationNotifier.getText('productDetailPriceFieldErrorText'),
                    style: TextStylePalette.redAkko16w400,
                  ),
                ],
              ),
            ),
          ),
      ],
    );
  }
}
