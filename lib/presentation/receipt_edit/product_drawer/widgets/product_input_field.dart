import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../../infrastructure/dtos/search_match_dto.dart';
import '../../../../infrastructure/notifiers/product_notifier.dart';
import '../../../../infrastructure/notifiers/translation_notifier.dart';
import '../../../../infrastructure/utils/color_pallette.dart';
import '../../../../infrastructure/utils/text_style_palette.dart';

class ProductInputField extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final translationNotifier = context.watch<TranslationNotifier>();
    final productNotifier = context.watch<ProductNotifier>();
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          translationNotifier.getText('productDetailProductFieldTitle'),
          style: TextStylePalette.blackAkko16w600,
        ),
        Container(
          height: 8,
        ),
        SizedBox(
          height: 48,
          child: TextField(
            onTapOutside: (event) => FocusScope.of(context).unfocus(),
            controller: productNotifier.productNameController,
            onTap: () async {
              final searchMatchDo = await Navigator.pushNamed(context, 'searchProductScreen') as SearchMatchDto?;
              if (searchMatchDo == null) return;
              productNotifier.setProduct(searchMatchDo.name, searchMatchDo.category, searchMatchDo.code!);
            },
            textAlignVertical: TextAlignVertical.center,
            cursorColor: ColorPalette.textBlack,
            style: TextStylePalette.blackAkko16w400,
            autocorrect: false,
            readOnly: true,
            decoration: InputDecoration(
              hintText: translationNotifier.getText('productDetailProductFieldHintText'),
              contentPadding: EdgeInsets.only(bottom: 24, left: 16),
              enabledBorder: OutlineInputBorder(
                borderSide: const BorderSide(color: ColorPalette.aqua300),
                borderRadius: BorderRadius.circular(4),
              ),
              focusedBorder: OutlineInputBorder(
                borderSide: const BorderSide(color: ColorPalette.aqua300),
                borderRadius: BorderRadius.circular(4),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
