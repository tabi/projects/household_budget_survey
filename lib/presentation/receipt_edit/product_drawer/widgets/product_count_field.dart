import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../../infrastructure/notifiers/product_notifier.dart';
import '../../../../infrastructure/utils/color_pallette.dart';
import '../../../../infrastructure/utils/text_style_palette.dart';

class ProductCountField extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final productNotifier = context.watch<ProductNotifier>();
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              flex: 1,
              child: SizedBox(
                height: 48,
                child: OutlinedButton(
                  child: Text('-'),
                  onPressed: () {
                    productNotifier.subtractItem();
                  },
                  style: OutlinedButton.styleFrom(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(4.0),
                    ),
                    foregroundColor: ColorPalette.aqua300,
                    side: BorderSide(
                      color: ColorPalette.aqua300,
                    ),
                  ),
                ),
              ),
            ),
            Container(
              width: 8,
            ),
            Expanded(
              flex: 3,
              child: SizedBox(
                height: 48,
                child: TextField(
                  onTapOutside: (event) => FocusScope.of(context).unfocus(),
                  controller: productNotifier.productDiscountController,
                  textAlignVertical: TextAlignVertical.center,
                  textAlign: TextAlign.center,
                  cursorColor: ColorPalette.textBlack,
                  style: TextStylePalette.blackAkko16w400,
                  autocorrect: false,
                  readOnly: true,
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.only(bottom: 24),
                    enabledBorder: OutlineInputBorder(
                      borderSide: const BorderSide(color: ColorPalette.aqua300),
                      borderRadius: BorderRadius.circular(4),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: const BorderSide(color: ColorPalette.aqua300),
                      borderRadius: BorderRadius.circular(4),
                    ),
                  ),
                ),
              ),
            ),
            Container(
              width: 8,
            ),
            Expanded(
              flex: 1,
              child: SizedBox(
                height: 48,
                child: OutlinedButton(
                  child: Text('+'),
                  onPressed: () {
                    productNotifier.addItem();
                  },
                  style: OutlinedButton.styleFrom(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(4.0),
                    ),
                    foregroundColor: ColorPalette.aqua300,
                    side: BorderSide(
                      color: ColorPalette.aqua300,
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ],
    );
  }
}
