import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../infrastructure/dtos/product_dto.dart';
import '../../../infrastructure/notifiers/product_notifier.dart';
import '../../../infrastructure/notifiers/transaction_notifier.dart';
import '../../../infrastructure/notifiers/translation_notifier.dart';
import '../../../infrastructure/utils/color_pallette.dart';
import '../../../infrastructure/utils/text_style_palette.dart';
import '../../../infrastructure/utils/ui_scalers.dart';
import 'widgets/product_category_field.dart';
import 'widgets/product_count_field.dart';
import 'widgets/product_input_field.dart';
import 'widgets/product_price_field.dart';

class ProductDetailScreen extends StatelessWidget {
  final TransactionManualNotifier transactionNotifier;
  final ProductDto product;

  const ProductDetailScreen(this.transactionNotifier, this.product);

  @override
  Widget build(BuildContext context) {
    final translationNotifier = context.watch<TranslationNotifier>();

    return ChangeNotifierProvider<ProductNotifier>(
      create: (context) => ProductNotifier(product),
      child: Builder(builder: (context) {
        final productNotifier = context.watch<ProductNotifier>();

        return Scaffold(
          backgroundColor: ColorPalette.backgroundWhite,
          appBar: AppBar(
            scrolledUnderElevation: 0,
            bottom: PreferredSize(
                child: Container(
                  color: ColorPalette.grey200,
                  height: 1.0,
                ),
                preferredSize: Size.fromHeight(4.0)),
            leading: IconButton(
              icon: Icon(
                Icons.close_rounded,
                color: ColorPalette.brand,
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            backgroundColor: ColorPalette.backgroundWhite,
            centerTitle: true,
            title: Text(
              translationNotifier.getText('addProductOrService'),
              style: TextStylePalette.brandSoho16w600,
            ),
          ),
          body: SafeArea(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: SingleChildScrollView(
                    child: Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          ProductInputField(),
                          Container(
                            height: 16,
                          ),
                          ProductCategoryField(),
                          // Container(
                          //   height: 16,
                          // ),
                          // ProductDiscountField(),
                          Container(
                            height: 16,
                          ),
                          ProductPriceField(),
                          // Container(
                          //   height: 16,
                          // ),
                          // ProductReturnToggle(),
                        ],
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      ProductCountField(),
                      Container(
                        height: 16,
                      ),
                      SizedBox(
                        height: 48 * x,
                        child: TextButton(
                          style: TextButton.styleFrom(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(4.0),
                            ),
                            foregroundColor: Colors.white,
                            backgroundColor: ColorPalette.aqua200,
                            minimumSize: Size.fromHeight(44),
                          ),
                          onPressed: () async {
                            productNotifier.setSavePressed(true);
                            if (productNotifier.isComplete()) {
                              transactionNotifier.upsertproduct(productNotifier.productDto);
                              FocusScope.of(context).requestFocus(FocusNode());
                              Navigator.pop(context, true);
                            }
                          },
                          child: Text(translationNotifier.getText('save'), style: TextStylePalette.whiteAkko16w400),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      }),
    );
  }
}
