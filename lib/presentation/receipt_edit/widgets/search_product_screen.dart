import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:provider/provider.dart';

import '../../../infrastructure/notifiers/search_notifier.dart';
import '../../../infrastructure/notifiers/translation_notifier.dart';
import '../../../infrastructure/repositories/product_search_repository.dart';
import '../../../infrastructure/services/string_match_service.dart';
import '../../../infrastructure/utils/color_pallette.dart';
import '../../../infrastructure/utils/text_style_palette.dart';
import '../../search/search_screen.dart';

class SearchProductScreen extends StatelessWidget {
  const SearchProductScreen();

  @override
  Widget build(BuildContext context) {
    final translationNotifier = context.watch<TranslationNotifier>();
    return ChangeNotifierProvider<SearchNotifier>(
      create: (context) => SearchNotifier(GetIt.I<StringMatchingService>(), GetIt.I<ProductSearchRepository>()),
      child: Builder(
        builder: (context) {
          return Scaffold(
            appBar: AppBar(
              scrolledUnderElevation: 0,
              bottom: PreferredSize(
                  child: Container(
                    color: ColorPalette.grey200,
                    height: 1.0,
                  ),
                  preferredSize: Size.fromHeight(4.0)),
              leading: IconButton(
                icon: Icon(
                  Icons.keyboard_arrow_left_rounded,
                  color: ColorPalette.brand,
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              backgroundColor: ColorPalette.backgroundWhite,
              centerTitle: true,
              title: Text(
                translationNotifier.getText('addProductOrService'),
                style: TextStylePalette.brandSoho16w600,
              ),
            ),
            body: SearchScreen(),
          );
        },
      ),
    );
  }
}
