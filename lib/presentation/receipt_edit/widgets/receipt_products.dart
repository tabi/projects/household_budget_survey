import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../infrastructure/dtos/product_dto.dart';
import '../../../infrastructure/dtos/search_match_dto.dart';
import '../../../infrastructure/notifiers/transaction_notifier.dart';
import '../../../infrastructure/notifiers/translation_notifier.dart';
import '../../../infrastructure/utils/color_pallette.dart';
import '../../../infrastructure/utils/text_style_palette.dart';
import '../product_drawer/product_detail_screen.dart';
import 'receipt_product_list.dart';

class ReceiptProductsWidget extends StatelessWidget {
  const ReceiptProductsWidget();

  @override
  Widget build(BuildContext context) {
    final translationNotifier = context.watch<TranslationNotifier>();
    final transactionNotifier = context.watch<TransactionManualNotifier>();

    return Container(
      padding: EdgeInsets.all(16),
      child: Column(
        children: [
          ReceiptProductsList(),
          SizedBox(
            height: 48,
            width: double.infinity,
            child: OutlinedButton(
              onPressed: () async {
                final searchMatchDo = await Navigator.pushNamed(context, 'searchProductScreen') as SearchMatchDto?;
                if (searchMatchDo == null) return;
                final product = ProductDto.create(searchMatchDo.name, searchMatchDo.category,
                    searchMatchDo.code ?? 'art', transactionNotifier.receipt.uuid, 1, null);
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) => ProductDetailScreen(transactionNotifier, product)));
              },
              style: OutlinedButton.styleFrom(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(4.0),
                ),
                foregroundColor: ColorPalette.aqua300,
                side: BorderSide(
                  color: ColorPalette.aqua300,
                ),
              ),
              child: Text(
                translationNotifier.getText('addProductOrServiceButton'),
                style: TextStylePalette.aquaAkko16w400,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
