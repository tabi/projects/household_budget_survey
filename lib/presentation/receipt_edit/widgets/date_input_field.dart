import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../infrastructure/notifiers/transaction_notifier.dart';
import '../../../infrastructure/notifiers/translation_notifier.dart';
import '../../../infrastructure/utils/color_pallette.dart';
import '../../../infrastructure/utils/date_util.dart';
import '../../../infrastructure/utils/text_style_palette.dart';

class DateInputField extends StatelessWidget {
  final textController = TextEditingController();

  Future<void> selectDate(BuildContext context, TransactionManualNotifier transactionNotifier,
      TranslationNotifier translationNotifier) async {
    var picked = await showDatePicker(
      locale: translationNotifier.locale,
      context: context,
      routeSettings: const RouteSettings(name: 'DatePicker'),
      initialDate: transactionNotifier.receipt.boughtOn,
      firstDate: DateTime.now().subtract(const Duration(days: 365)),
      lastDate: DateTime.now().add(const Duration(days: 365)),
    );
    if (picked != null) {
      transactionNotifier.updateDate(picked);
    }
  }

  @override
  Widget build(BuildContext context) {
    final translationNotifier = context.watch<TranslationNotifier>();
    final transactionNotifier = context.watch<TransactionManualNotifier>();
    textController.text = getDateString(transactionNotifier.receipt.boughtOn, translationNotifier);
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Datum',
          style: TextStylePalette.blackAkko16w600,
        ),
        Container(height: 8),
        SizedBox(
          height: 48,
          child: TextField(
            onTapOutside: (event) => FocusScope.of(context).unfocus(),
            controller: textController,
            onTap: () {
              selectDate(context, transactionNotifier, translationNotifier);
            },
            textAlignVertical: TextAlignVertical.center,
            cursorColor: ColorPalette.textBlack,
            style: TextStylePalette.blackAkko16w400,
            autocorrect: false,
            readOnly: true,
            decoration: InputDecoration(
              hintText: translationNotifier.getText('receiptDateInputHintText'),
              suffixIcon: Icon(
                Icons.calendar_month_rounded,
                size: 22,
                color: Colors.black,
              ),
              contentPadding: EdgeInsets.only(bottom: 24, left: 16),
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: ColorPalette.aqua300),
                borderRadius: BorderRadius.circular(4),
              ),
              focusedBorder: OutlineInputBorder(
                borderSide: const BorderSide(color: ColorPalette.aqua300),
                borderRadius: BorderRadius.circular(4),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
