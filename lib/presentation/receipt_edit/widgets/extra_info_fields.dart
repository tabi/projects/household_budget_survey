import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../infrastructure/notifiers/transaction_notifier.dart';
import '../../../infrastructure/notifiers/translation_notifier.dart';
import '../../../infrastructure/utils/color_pallette.dart';
import '../../../infrastructure/utils/text_style_palette.dart';

class ExtraInfoFields extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final translationNotifier = context.watch<TranslationNotifier>();
    final transactionNotifier = context.watch<TransactionManualNotifier>();

    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CheckboxListTile(
                title: Text(
                  translationNotifier.getText('receiptCheckboxAbroadExpense'),
                  style: TextStylePalette.blackAkko16w500,
                ),
                contentPadding: EdgeInsets.zero,
                value: transactionNotifier.receipt.isAbroad,
                onChanged: (newValue) => transactionNotifier.updateAbroad(newValue!),
                activeColor: ColorPalette.aqua300,
                controlAffinity: ListTileControlAffinity.leading,
              ),
              CheckboxListTile(
                title: Text(
                  translationNotifier.getText('receiptCheckboxOnlineExpense'),
                  style: TextStylePalette.blackAkko16w500,
                ),
                contentPadding: EdgeInsets.zero,
                value: transactionNotifier.receipt.isOnline,
                onChanged: (newValue) => transactionNotifier.updateOnline(newValue!),
                activeColor: ColorPalette.aqua300,
                controlAffinity: ListTileControlAffinity.leading,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
