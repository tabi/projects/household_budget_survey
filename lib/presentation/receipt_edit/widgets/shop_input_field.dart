import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../infrastructure/dtos/search_match_dto.dart';
import '../../../infrastructure/notifiers/transaction_notifier.dart';
import '../../../infrastructure/notifiers/translation_notifier.dart';
import '../../../infrastructure/utils/color_pallette.dart';
import '../../../infrastructure/utils/extensions.dart';
import '../../../infrastructure/utils/text_style_palette.dart';

class ShopInputField extends StatelessWidget {
  final textController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final translationNotifier = context.watch<TranslationNotifier>();
    final transactionNotifier = context.watch<TransactionManualNotifier>();
    if (transactionNotifier.receipt.shopName != null) {
      textController.text = transactionNotifier.receipt.shopName!.capitalize().toString();
    }
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          translationNotifier.getText('shop'),
          style: TextStylePalette.blackAkko16w600,
        ),
        Container(
          height: 8,
        ),
        SizedBox(
          height: 48,
          child: TextField(
            onTapOutside: (event) => FocusScope.of(context).unfocus(),
            controller: textController,
            onTap: () async {
              final searchMatchDo = await Navigator.pushNamed(context, 'searchStoreScreen') as SearchMatchDto?;
              if (searchMatchDo != null) {
                transactionNotifier.updateShop(searchMatchDo.name, searchMatchDo.category);
              }
            },
            textAlignVertical: TextAlignVertical.center,
            cursorColor: ColorPalette.textBlack,
            style: TextStylePalette.blackAkko16w400,
            autocorrect: false,
            readOnly: true,
            decoration: InputDecoration(
              hintText: translationNotifier.getText('shopInputFieldHintText'),
              contentPadding: EdgeInsets.only(bottom: 24, left: 16),
              enabledBorder: OutlineInputBorder(
                borderSide: (!transactionNotifier.hasShop() && transactionNotifier.showInputWarning)
                    ? BorderSide(color: ColorPalette.warningRed)
                    : BorderSide(color: ColorPalette.aqua300),
                borderRadius: BorderRadius.circular(4),
              ),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: ColorPalette.aqua300),
                borderRadius: BorderRadius.circular(4),
              ),
            ),
          ),
        ),
        if (!transactionNotifier.hasShop() && transactionNotifier.showInputWarning)
          Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: RichText(
              text: TextSpan(
                children: [
                  WidgetSpan(
                    child: Icon(
                      Icons.warning_rounded,
                      size: 16,
                      color: ColorPalette.warningRed,
                    ),
                  ),
                  WidgetSpan(
                      child: Container(
                    width: 6,
                  )),
                  TextSpan(
                    text: translationNotifier.getText('shopInputFieldErrorText'),
                    style: TextStylePalette.redAkko16w400,
                  ),
                ],
              ),
            ),
          ),
      ],
    );
  }
}
