import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../infrastructure/dtos/product_dto.dart';
import '../../../infrastructure/notifiers/transaction_notifier.dart';
import '../../../infrastructure/notifiers/translation_notifier.dart';
import '../../../infrastructure/utils/color_pallette.dart';
import '../../../infrastructure/utils/extensions.dart';
import '../../../infrastructure/utils/text_style_palette.dart';
import '../../../infrastructure/utils/ui_scalers.dart';
import '../../modal_bottomsheets/product_change_drawer.dart';

class ReceiptProductsList extends StatelessWidget {
  const ReceiptProductsList();

  @override
  Widget build(BuildContext context) {
    final transactionNotifier = context.watch<TransactionManualNotifier>();
    final translationNotifier = context.watch<TranslationNotifier>();
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          translationNotifier.getText('products') +
              ' ' +
              '(' +
              transactionNotifier.receipt.products.length.toString() +
              ')',
          style: TextStylePalette.blackSoho16w600,
        ),
        if (transactionNotifier.hasProducts() == false && transactionNotifier.showInputWarning)
          Padding(
            padding: const EdgeInsets.only(top: 16.0),
            child: Container(
              decoration: BoxDecoration(
                color: ColorPalette.warningRedOpacity,
                borderRadius: BorderRadius.circular(4),
              ),
              padding: EdgeInsets.all(16),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(2),
                    child: Icon(
                      Icons.warning_rounded,
                      size: 20,
                      color: ColorPalette.warningRed,
                    ),
                  ),
                  Container(
                    width: 16,
                  ),
                  Expanded(
                    child: Text(
                      translationNotifier.getText('productListWarningText'),
                      style: TextStylePalette.primaryAkko16w400,
                      softWrap: true,
                    ),
                  )
                ],
              ),
            ),
          ),
        if (!transactionNotifier.hasProducts())
          Padding(
            padding: const EdgeInsets.only(top: 16.0),
            child: Text(
              translationNotifier.getText('productListEmptyText'),
              style: TextStylePalette.greyAkko16w400,
              softWrap: true,
            ),
          ),
        Container(
          height: 16,
        ),
        ListView.builder(
          physics: NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          itemCount: transactionNotifier.receipt.products.length,
          itemBuilder: (_, index) {
            return _ProductTile(transactionNotifier.receipt.products[index]);
          },
        ),
      ],
    );
  }
}

class _ProductTile extends StatefulWidget {
  final ProductDto productDto;

  const _ProductTile(this.productDto);

  @override
  __ProductTileState createState() => __ProductTileState();
}

class __ProductTileState extends State<_ProductTile> {
  bool selected = false;
  late PointerDownEvent recognizeTap;

  @override
  Widget build(BuildContext context) {
    final translationNotifier = context.watch<TranslationNotifier>();
    final transactionNotifier = context.watch<TransactionManualNotifier>();

    return Container(
      width: double.infinity,
      padding: EdgeInsets.symmetric(vertical: 8 * x),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          GestureDetector(
            behavior: HitTestBehavior.opaque,
            onTap: () {
              getModalProductOption(context, translationNotifier, transactionNotifier, widget.productDto);
            },
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  padding: EdgeInsets.all(8 * x),
                  decoration: BoxDecoration(
                    shape: BoxShape.rectangle,
                    color: ColorPalette.brand,
                    borderRadius: BorderRadius.all(
                      Radius.circular(2),
                    ),
                  ),
                  child: Icon(
                    Icons.shield_rounded,
                    color: Colors.white,
                  ),
                ),
                Container(
                  width: 16 * x,
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Text(
                                  widget.productDto.name!.capitalize(),
                                  style: TextStylePalette.primaryAkko16w400,
                                ),
                                Text(
                                  widget.productDto.coicopName!.capitalize(),
                                  style: TextStylePalette.greyAkko14w400,
                                ),
                              ],
                            ),
                          ),
                          Icon(
                            Icons.more_horiz_rounded,
                            color: ColorPalette.textPrimary,
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          Expanded(
                            child: Text(
                              translationNotifier.getText('productTileAmount') +
                                  ' ' +
                                  widget.productDto.count.toString(),
                              style: TextStylePalette.primaryAkko14w400,
                            ),
                          ),
                          if (widget.productDto.isReturn)
                            Container(
                              padding: EdgeInsets.symmetric(vertical: 2 * y, horizontal: 4 * x),
                              decoration: BoxDecoration(
                                border: Border.all(
                                  color: ColorPalette.textGrey,
                                ),
                                borderRadius: BorderRadius.all(
                                  Radius.circular(2),
                                ),
                              ),
                              child: Text(
                                translationNotifier.getText('return'),
                                style: TextStylePalette.greyAkko14w400,
                              ),
                            ),
                          Container(
                            width: 16 * x,
                          ),
                          Text(
                            widget.productDto
                                .getPrice()
                                .toStringAsFixed(2)
                                .replaceAll('.', ',')
                                .currencyFormat(translationNotifier),
                            maxLines: 1,
                            overflow: TextOverflow.visible,
                            style: TextStylePalette.primaryAkko14w600,
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
          Divider(),
        ],
      ),
    );
  }
}
