import 'package:flutter/material.dart';

import 'date_input_field.dart';
import 'extra_info_fields.dart';
import 'shop_input_field.dart';

class ReceiptGeneralInfo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(16.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          DateInputField(),
          Container(height: 16),
          ShopInputField(),
          Container(height: 16),
          ExtraInfoFields(),
        ],
      ),
    );
  }
}
