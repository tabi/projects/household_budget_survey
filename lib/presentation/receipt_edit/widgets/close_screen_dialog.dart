import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../infrastructure/notifiers/translation_notifier.dart';
import '../../../infrastructure/utils/color_pallette.dart';

class CloseScreenDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final translationNotifier = context.watch<TranslationNotifier>();
    return AlertDialog(
      backgroundColor: ColorPalette.backgroundWhite,
      elevation: 0,
      title: Text(
        translationNotifier.getText('warningDialogTitle'),
        style: const TextStyle(
          fontSize: 24,
          fontWeight: FontWeight.w500,
          color: ColorPalette.textBlack,
        ),
      ),
      content: Text(
        translationNotifier.getText('removalWarningText'),
        style: const TextStyle(
          fontSize: 14,
          fontWeight: FontWeight.w400,
          color: ColorPalette.textGrey,
        ),
      ),
      actions: <Widget>[
        TextButton(
          onPressed: () => Navigator.pop(context, false),
          child: Text(
            translationNotifier.getText('cancelButton'),
            style: const TextStyle(
              fontSize: 14,
              color: ColorPalette.aqua200,
            ),
          ),
        ),
        TextButton(
          onPressed: () => Navigator.pop(context, true),
          child: Text(
            translationNotifier.getText('throwawayButton'),
            style: const TextStyle(
              fontSize: 14,
              color: ColorPalette.aqua200,
            ),
          ),
        ),
      ],
    );
  }
}
