import 'package:easy_image_viewer/easy_image_viewer.dart';
import 'package:flutter/material.dart';
import '../menu/menu_widget.dart';
import 'package:provider/provider.dart';

import '../../infrastructure/dtos/receipt_dto.dart';
import '../../infrastructure/notifiers/transaction_notifier.dart';
import '../../infrastructure/notifiers/translation_notifier.dart';
import '../../infrastructure/utils/color_pallette.dart';
import '../../infrastructure/utils/extensions.dart';
import '../../infrastructure/utils/text_style_palette.dart';
import '../../infrastructure/utils/ui_scalers.dart';
import 'widgets/close_screen_dialog.dart';
import 'widgets/receipt_general_info.dart';
import 'widgets/receipt_products.dart';

class ReceiptEditScreen extends StatelessWidget {
  const ReceiptEditScreen();

  Future<void> confirmClose(BuildContext context, TransactionManualNotifier transactionNotifier) async {
    if (transactionNotifier.isModified) {
      final closeScreen =
          await showDialog<bool>(context: context, builder: (BuildContext context) => CloseScreenDialog());
      if (closeScreen ?? false) Navigator.of(context).pop();
    } else {
      Navigator.of(context).pop();
    }
  }

  @override
  Widget build(BuildContext context) {
    final translationNotifier = context.watch<TranslationNotifier>();

    return ChangeNotifierProvider<TransactionManualNotifier>(
      create: (_) => TransactionManualNotifier(ModalRoute.of(context)?.settings.arguments as ReceiptDto?),
      child: Builder(
        builder: (context) {
          final transactionNotifier = context.watch<TransactionManualNotifier>();
          return Scaffold(
            backgroundColor: ColorPalette.backgroundWhite,
            appBar: AppBar(
              scrolledUnderElevation: 0,
              bottom: PreferredSize(
                  child: Container(
                    color: ColorPalette.grey200,
                    height: 1.0,
                  ),
                  preferredSize: Size.fromHeight(4.0)),
              leading: IconButton(
                icon: Icon(
                  Icons.close_rounded,
                  color: ColorPalette.brand,
                ),
                onPressed: () {
                  confirmClose(context, transactionNotifier);
                },
              ),
              backgroundColor: ColorPalette.backgroundWhite,
              centerTitle: true,
              title: Text(
                translationNotifier.getText('addExpense'),
                style: TextStylePalette.brandSoho16w600,
              ),
            ),
            body: SafeArea(
              child: Column(
                children: [
                  Expanded(
                    child: SingleChildScrollView(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          (transactionNotifier.receipt.imagePath != null)
                              ? GestureDetector(
                                  onTap: () {
                                    showImageViewer(context, Image.asset(transactionNotifier.receipt.imagePath!).image,
                                        swipeDismissible: true);
                                  },
                                  child: Container(
                                    alignment: Alignment.center,
                                    height: 200,
                                    child: Image.asset(
                                      transactionNotifier.receipt.imagePath!,
                                      height: 200,
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                                )
                              : Container(),
                          ReceiptGeneralInfo(),
                          Divider(),
                          ReceiptProductsWidget(),
                        ],
                      ),
                    ),
                  ),
                  Divider(
                    color: Colors.black,
                  ),
                  Container(
                    padding: EdgeInsets.all(16.0),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Text(
                              translationNotifier.getText('totalAmountPrompt'),
                              style: TextStylePalette.blackAkko16w600,
                            ),
                            Expanded(
                              child: Container(),
                            ),
                            Text(
                              transactionNotifier.receipt
                                  .getTotalPrice()
                                  .toStringAsFixed(2)
                                  .currencyFormat(translationNotifier),
                              style: TextStylePalette.blackAkko16w600,
                            ),
                          ],
                        ),
                        Container(
                          height: 16,
                        ),
                        SizedBox(
                          height: 48 * x,
                          child: TextButton(
                            style: TextButton.styleFrom(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(4.0),
                              ),
                              foregroundColor: Colors.white,
                              backgroundColor: ColorPalette.aqua200,
                              minimumSize: Size.fromHeight(44),
                            ),
                            onPressed: () async {
                              if (transactionNotifier.isComplete) {
                                await transactionNotifier.save();
                                // Navigator.of(context).pop();
                                await Navigator.of(context).pushAndRemoveUntil(
                                    MaterialPageRoute(builder: (context) => MenuWidget()),
                                    (Route<dynamic> route) => false);
                              } else {
                                transactionNotifier.updateShowInputWarning(true);
                              }
                            },
                            child: Text(
                              translationNotifier.getText('save'),
                              style: TextStylePalette.whiteAkko16w400,
                            ),
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
