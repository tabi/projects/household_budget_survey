import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

import '../../infrastructure/dtos/product_dto.dart';
import '../../infrastructure/dtos/receipt_dto.dart';
import '../../infrastructure/dtos/search_match_dto.dart';
import '../../infrastructure/notifiers/category_notifier.dart';
import '../../infrastructure/notifiers/transaction_notifier.dart';
import '../../infrastructure/notifiers/translation_notifier.dart';
import '../../infrastructure/utils/color_pallette.dart';
import '../../infrastructure/utils/extensions.dart';
import '../../infrastructure/utils/text_style_palette.dart';
import '../../infrastructure/utils/ui_scalers.dart';
import '../receipt_edit/widgets/close_screen_dialog.dart';

class CategorySelectionScreen extends StatelessWidget {
  const CategorySelectionScreen();

  Future<void> confirmClose(BuildContext context, TransactionManualNotifier transactionNotifier) async {
    if (transactionNotifier.isModified) {
      final closeScreen =
          await showDialog<bool>(context: context, builder: (BuildContext context) => CloseScreenDialog());
      if (closeScreen ?? false) Navigator.of(context).pop();
    } else {
      Navigator.of(context).pop();
    }
  }

  @override
  Widget build(BuildContext context) {
    final translationNotifier = context.watch<TranslationNotifier>();

    return ChangeNotifierProvider<TransactionManualNotifier>(
      create: (_) => TransactionManualNotifier(ModalRoute.of(context)?.settings.arguments as ReceiptDto?),
      child: Builder(
        builder: (context) {
          final transactionNotifier = context.watch<TransactionManualNotifier>();
          return Scaffold(
            backgroundColor: ColorPalette.backgroundWhite,
            appBar: AppBar(
              scrolledUnderElevation: 0,
              bottom: PreferredSize(
                  child: Container(
                    color: ColorPalette.grey200,
                    height: 1.0,
                  ),
                  preferredSize: Size.fromHeight(4.0)),
              leading: IconButton(
                icon: Icon(
                  Icons.close_rounded,
                  color: ColorPalette.brand,
                ),
                onPressed: () {
                  confirmClose(context, transactionNotifier);
                },
              ),
              backgroundColor: ColorPalette.backgroundWhite,
              centerTitle: true,
              title: Text(
                translationNotifier.getText('categorySelection'),
                style: TextStylePalette.brandSoho16w600,
              ),
            ),
            body: SafeArea(
              child: Column(
                children: [
                  Expanded(
                    child: SingleChildScrollView(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          ListView.builder(
                            physics: NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            itemCount: transactionNotifier.receipt.products.length,
                            itemBuilder: (_, index) {
                              return _ProductCategoryTile(transactionNotifier.receipt.products[index]);
                            },
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    child: Column(
                      children: [
                        Divider(),
                        Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: SizedBox(
                            height: 48 * x,
                            child: TextButton(
                              style: TextButton.styleFrom(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(4.0),
                                ),
                                foregroundColor: Colors.white,
                                backgroundColor: ColorPalette.aqua200,
                                minimumSize: Size.fromHeight(44),
                              ),
                              onPressed: () async {
                                bool forgotCategory = false;
                                for (var item in transactionNotifier.receipt.products) {
                                  if (item.coicopName == null) {
                                    forgotCategory = true;
                                  }
                                }
                                if (!forgotCategory) {
                                  for (var item in transactionNotifier.receipt.products) {
                                    transactionNotifier.upsertproduct(item);
                                  }
                                  Navigator.of(context).pop();
                                  Navigator.pushNamed(context, 'addManualReceipt',
                                      arguments: transactionNotifier.receipt);
                                }
                              },
                              child: Text(
                                translationNotifier.getText('moveOn'),
                                style: TextStylePalette.whiteAkko16w400,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}

class _ProductCategoryTile extends StatefulWidget {
  final ProductDto productDto;

  const _ProductCategoryTile(this.productDto);

  @override
  __ProductCategoryTileState createState() => __ProductCategoryTileState();
}

class __ProductCategoryTileState extends State<_ProductCategoryTile> {
  @override
  Widget build(BuildContext context) {
    final translationNotifier = context.watch<TranslationNotifier>();

    return ChangeNotifierProvider(
      create: (context) => CategoryNotifier(widget.productDto),
      child: Builder(
        builder: (context) {
          final categoryNotifier = context.watch<CategoryNotifier>();

          return Container(
            width: double.infinity,
            padding: EdgeInsets.symmetric(vertical: 8 * x, horizontal: 16 * y),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      padding: EdgeInsets.all(8 * x),
                      decoration: BoxDecoration(
                        shape: BoxShape.rectangle,
                        color: ColorPalette.grey300,
                        borderRadius: BorderRadius.all(
                          Radius.circular(2),
                        ),
                      ),
                      child: Icon(
                        Icons.question_mark_rounded,
                        color: Colors.white,
                      ),
                    ),
                    Container(
                      width: 16 * x,
                    ),
                    Text(
                      widget.productDto.name!.capitalize(),
                      style: TextStylePalette.primaryAkko16w400,
                    ),
                  ],
                ),
                Container(
                  height: 16 * y,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text(
                      translationNotifier.getText('productDetailCategoryFieldTitle'),
                      style: TextStylePalette.blackAkko16w600,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                      child: SizedBox(
                        height: 48,
                        child: TextField(
                          onTap: () async {
                            final searchMatchDo =
                                await Navigator.pushNamed(context, 'searchCategoryScreen') as SearchMatchDto?;
                            if (searchMatchDo != null) {
                              widget.productDto.coicopName = searchMatchDo.name;
                              widget.productDto.coicopCode = searchMatchDo.code;
                              categoryNotifier.setProductCategory(searchMatchDo.name);
                            }
                          },
                          onTapOutside: (event) => FocusScope.of(context).unfocus(),
                          controller: categoryNotifier.productCategoryFieldController,
                          textAlignVertical: TextAlignVertical.center,
                          cursorColor: ColorPalette.textBlack,
                          style: TextStylePalette.blackAkko16w400,
                          autocorrect: false,
                          decoration: InputDecoration(
                            hintText: translationNotifier.getText('productDetailCategoryFieldHintText'),
                            suffix: Icon(
                              Icons.keyboard_arrow_down_rounded,
                              color: ColorPalette.textBlack,
                            ),
                            contentPadding: EdgeInsets.all(14.0),
                            enabledBorder: OutlineInputBorder(
                              borderSide: (categoryNotifier.productCategoryFieldController.text.isEmpty)
                                  ? BorderSide(color: ColorPalette.warningRed)
                                  : BorderSide(color: ColorPalette.aqua300),
                              borderRadius: BorderRadius.circular(4),
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderSide: const BorderSide(
                                color: Colors.black,
                                width: 2,
                              ),
                              borderRadius: BorderRadius.circular(4),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                Container(
                  height: 8 * y,
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 1,
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Text(
                              translationNotifier.getText('productTileAmount'),
                              style: TextStylePalette.blackAkko16w600,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                              child: SizedBox(
                                height: 48,
                                child: TextField(
                                  onTapOutside: (event) => FocusScope.of(context).unfocus(),
                                  controller: categoryNotifier.productCountFieldController,
                                  keyboardType: TextInputType.number,
                                  onChanged: (value) {
                                    widget.productDto.count = int.tryParse(value) ?? 0;
                                  },
                                  inputFormatters: <TextInputFormatter>[FilteringTextInputFormatter.digitsOnly],
                                  textAlignVertical: TextAlignVertical.center,
                                  cursorColor: ColorPalette.textBlack,
                                  style: TextStylePalette.blackAkko16w400,
                                  autocorrect: false,
                                  decoration: InputDecoration(
                                    contentPadding: EdgeInsets.all(14.0),
                                    enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: ColorPalette.aqua300),
                                      borderRadius: BorderRadius.circular(4),
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: const BorderSide(
                                        color: Colors.black,
                                        width: 2,
                                      ),
                                      borderRadius: BorderRadius.circular(4),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ]),
                    ),
                    Container(
                      width: 16,
                    ),
                    Expanded(
                      flex: 1,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            translationNotifier.getText('productDetailPriceFieldTitle'),
                            style: TextStylePalette.blackAkko16w600,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                            child: SizedBox(
                              height: 48,
                              child: TextField(
                                onTapOutside: (event) => FocusScope.of(context).unfocus(),
                                controller: categoryNotifier.productPriceFieldController,
                                onChanged: (value) => widget.productDto.price = double.tryParse(value) ?? 0.0,
                                textAlignVertical: TextAlignVertical.center,
                                cursorColor: ColorPalette.textBlack,
                                style: TextStylePalette.blackAkko16w400,
                                autocorrect: false,
                                decoration: InputDecoration(
                                  isDense: true,
                                  prefix: Text('€'),
                                  contentPadding: EdgeInsets.all(14.0),
                                  enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: ColorPalette.aqua300),
                                    borderRadius: BorderRadius.circular(4),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: const BorderSide(
                                      color: Colors.black,
                                      width: 2,
                                    ),
                                    borderRadius: BorderRadius.circular(4),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                Divider(),
              ],
            ),
          );
        },
      ),
    );
  }
}
