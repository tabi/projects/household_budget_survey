import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../infrastructure/dtos/receipt_dto.dart';
import '../../../infrastructure/notifiers/receipt_list_notifier.dart';
import '../../../infrastructure/utils/color_pallette.dart';
import '../../expenses/widgets/no_receipts_image.dart';
import '../../expenses/widgets/receipt_tile_widget.dart';

class ReceiptListSingleDayWidget extends StatelessWidget {
  late final DateTime _selectedDate;

  ReceiptListSingleDayWidget(DateTime selectedDate) {
    _selectedDate = selectedDate;
  }

  List<Widget> annotatedReceiptsWithDates(List<ReceiptDto> receiptDtos) {
    return receiptDtos.expand((receiptDto) {
      final widgets = <Widget>[];
      widgets.add(ReceiptTileWidget(receiptDto));
      return widgets;
    }).toList();
  }

  @override
  Widget build(BuildContext context) {
    final receiptListNotifier = context.watch<ReceiptListNotifier>();
    if (receiptListNotifier.getReceiptsAtDate(_selectedDate).isEmpty) return NoReceiptImage();
    final annotatedReceipts = annotatedReceiptsWithDates(receiptListNotifier.getReceiptsAtDate(_selectedDate));
    return Expanded(
      child: Container(
        color: ColorPalette.backgroundOffWhite,
        child: ListView.builder(
          itemCount: annotatedReceipts.length,
          itemBuilder: (BuildContext context, int index) {
            return annotatedReceipts[index];
          },
        ),
      ),
    );
  }
}
