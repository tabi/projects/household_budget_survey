import 'package:flutter/material.dart';
import '../../infrastructure/notifiers/overview_notifiers/overview_notifier.dart';
import '../../infrastructure/notifiers/receipt_list_notifier.dart';
import '../../infrastructure/notifiers/translation_notifier.dart';
import '../../infrastructure/utils/color_pallette.dart';
import '../../infrastructure/utils/date_util.dart';
import '../../infrastructure/utils/extensions.dart';
import '../../infrastructure/utils/text_style_palette.dart';
import 'widgets/receipt_list_single_day_widget.dart';
import '../modal_bottomsheets/add_new_receipt_drawer.dart';
import '../modal_bottomsheets/overview_complete_empty_warning_drawer.dart';
import 'package:provider/provider.dart';

class ExpensesDetailPage extends StatelessWidget {
  late final DateTime _selectedDate;

  ExpensesDetailPage(DateTime selectedDate) {
    _selectedDate = selectedDate;
  }

  @override
  Widget build(BuildContext context) {
    final translationNotifier = context.watch<TranslationNotifier>();
    final overviewNotifier = context.watch<OverviewNotifier>();
    final receiptListNotifier = context.watch<ReceiptListNotifier>();

    return Scaffold(
        backgroundColor: ColorPalette.backgroundWhite,
        appBar: AppBar(
          scrolledUnderElevation: 0,
          bottom: PreferredSize(
              child: Container(
                color: ColorPalette.grey200,
                height: 1.0,
              ),
              preferredSize: Size.fromHeight(4.0)),
          leading: IconButton(
            icon: Icon(
              Icons.keyboard_arrow_left_rounded,
              size: 32,
              color: ColorPalette.brand,
            ),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          backgroundColor: ColorPalette.backgroundWhite,
          centerTitle: true,
          title: Text(
            getDateString(_selectedDate, translationNotifier),
            style: TextStylePalette.brandSoho16w600,
          ),
          actions: [
            IconButton(
              icon: Icon(
                Icons.add_rounded,
                color: ColorPalette.brand,
              ),
              onPressed: () => getModalAddDrawer(context, translationNotifier, _selectedDate),
            ),
          ],
        ),
        body: SafeArea(
          child: Column(
            children: [
              ReceiptListSingleDayWidget(_selectedDate),
              Divider(
                color: ColorPalette.grey300,
                height: 1,
              ),
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  children: [
                    Row(
                      children: [
                        Text(
                          translationNotifier.getText('totalExpense'),
                          style: TextStylePalette.blackAkko16w600,
                        ),
                        Expanded(
                          child: Container(),
                        ),
                        Text(
                          receiptListNotifier
                              .getTotalReceiptsPriceAtDate(_selectedDate)
                              .toStringAsFixed(2)
                              .currencyFormat(translationNotifier),
                          style: TextStylePalette.blackAkko16w600,
                        ),
                      ],
                    ),
                    Container(
                      height: 16,
                    ),
                    SizedBox(
                      height: 48,
                      child: OutlinedButton(
                        style: OutlinedButton.styleFrom(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(4.0),
                          ),
                          foregroundColor: (overviewNotifier.isAllowedToComplete(_selectedDate) &&
                                  overviewNotifier.isWithinSurveyPeriod(_selectedDate))
                              ? ColorPalette.aqua300
                              : ColorPalette.grey300,
                          minimumSize: Size.fromHeight(44),
                          side: BorderSide(
                            color: (overviewNotifier.isAllowedToComplete(_selectedDate) &&
                                    overviewNotifier.isWithinSurveyPeriod(_selectedDate))
                                ? ColorPalette.aqua300
                                : ColorPalette.grey300,
                          ),
                        ),
                        onPressed: () {
                          if (overviewNotifier.isWithinSurveyPeriod(_selectedDate) &&
                              overviewNotifier.isAllowedToComplete(_selectedDate)) {
                            if (receiptListNotifier.getNumberRecipesAtDate(overviewNotifier.selectedDay) > 0) {
                              overviewNotifier.completeDay(overviewNotifier.selectedDay);
                            } else {
                              getOverviewCompleteEmptyDayWarning(context, overviewNotifier, translationNotifier);
                            }
                          }
                        },
                        child: Text(
                          translationNotifier.getText('completeDay'),
                          style: (overviewNotifier.isAllowedToComplete(_selectedDate) &&
                                  overviewNotifier.isWithinSurveyPeriod(_selectedDate))
                              ? TextStylePalette.aquaAkko16w400
                              : TextStylePalette.greyAkko16w400,
                        ),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ));
  }
}
