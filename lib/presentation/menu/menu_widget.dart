import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:household_budget_survey/infrastructure/repositories/receipt_repository.dart';
import 'package:provider/provider.dart';

import '../../infrastructure/dtos/menu_dto.dart';
import '../../infrastructure/notifiers/menu_notifier.dart';
import '../../infrastructure/notifiers/translation_notifier.dart';
import '../../infrastructure/utils/color_pallette.dart';
import '../../infrastructure/utils/text_style_palette.dart';
import '../../infrastructure/utils/ui_scalers.dart';
import '../expenses/expenses_screen.dart';
import '../insights/insights_screen.dart';
import '../modal_bottomsheets/add_new_receipt_drawer.dart';
import '../overview/overview_screen.dart';
import '../settings/settings_screen.dart';

class MenuWidget extends StatelessWidget {
  final List<MenuDto> menuItems = [
    MenuDto(icon: Icons.home_rounded, text: 'overviewScreen', screen: OverviewScreen()),
    MenuDto(icon: Icons.receipt_rounded, text: 'receiptListScreen', screen: ExpensesScreen()),
    MenuDto(icon: Icons.add_box_rounded, text: 'addReceipt', screen: Container()),
    MenuDto(icon: Icons.pie_chart_rounded, text: 'insightsScreen', screen: InsightsScreen()),
    MenuDto(icon: Icons.more_horiz_rounded, text: 'settingsScreen', screen: SettingsScreen()),
  ];

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<MenuNotifier>(
      create: (_) => MenuNotifier(),
      child: Builder(
        builder: (context) {
          final menuNotifier = context.watch<MenuNotifier>();
          final translationNotifier = context.watch<TranslationNotifier>();
          return Scaffold(
            backgroundColor: ColorPalette.backgroundWhite,
            resizeToAvoidBottomInset: false,
            body: menuItems[menuNotifier.index].screen,
            bottomNavigationBar: BottomNavigationBar(
              backgroundColor: ColorPalette.backgroundWhite,
              unselectedLabelStyle: TextStylePalette.greyAkko11w400,
              selectedLabelStyle: TextStylePalette.brandAkko11w600,
              elevation: 2,
              iconSize: 30.0 * x,
              onTap: (index) {
                if (index == 2) {
                  getModalAddDrawer(context, translationNotifier);
                } else {
                  menuNotifier.index = index;
                }
                if (index == 1) GetIt.I<ReceiptRepository>().syncReceipts();
              },
              fixedColor: ColorPalette.brand,
              type: BottomNavigationBarType.fixed,
              currentIndex: menuNotifier.index,
              items: [
                for (final item in menuItems)
                  BottomNavigationBarItem(
                    label: translationNotifier.getText(item.text),
                    icon: Icon(
                      item.icon,
                      color: ColorPalette.grey300,
                    ),
                    activeIcon: Icon(
                      item.icon,
                      color: ColorPalette.brand,
                    ),
                  ),
              ],
            ),
          );
        },
      ),
    );
  }
}
