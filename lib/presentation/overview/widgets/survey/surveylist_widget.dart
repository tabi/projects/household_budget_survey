import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../../infrastructure/dtos/external_survey_dto.dart';
import '../../../../infrastructure/notifiers/overview_notifiers/surveylist_notifier.dart';
import '../../../../infrastructure/utils/color_pallette.dart';
import 'no_survey_image.dart';
import 'survey_tile.dart';

class SurveyListWidget extends StatelessWidget {
  List<Widget> makeSurveyItemList(List<ExternalSurveyDto> surveys) {
    return surveys.expand((survey) {
      final widgets = <Widget>[];
      widgets.add(SurveyTile(survey));
      return widgets;
    }).toList();
  }

  @override
  Widget build(BuildContext context) {
    final surveyListNotifier = context.watch<SurveyListNotifier>();
    if (surveyListNotifier.surveyList.isEmpty)
      return Container(
        color: ColorPalette.backgroundOffWhite,
        child: NoSurveyImage(),
      );
    final surveyItems = makeSurveyItemList(surveyListNotifier.surveyList);
    return Container(
      color: ColorPalette.backgroundOffWhite,
      child: ListView.builder(
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        itemCount: surveyItems.length,
        itemBuilder: (BuildContext context, int index) {
          return surveyItems[index];
        },
      ),
    );
  }
}
