import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../../infrastructure/notifiers/translation_notifier.dart';
import '../../../../infrastructure/utils/color_pallette.dart';
import '../../../../infrastructure/utils/text_style_palette.dart';
import '../../../../infrastructure/utils/ui_scalers.dart';

class NoSurveyImage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final translationNotifier = context.watch<TranslationNotifier>();

    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Icon(
            Icons.not_interested_rounded,
            color: ColorPalette.grey300,
            size: 48,
          ),
          SizedBox(height: 10.0 * y),
          Text(translationNotifier.getText('noSurveysFound'),
              textAlign: TextAlign.center, style: TextStylePalette.greyAkko14w400),
        ],
      ),
    );
  }
}
