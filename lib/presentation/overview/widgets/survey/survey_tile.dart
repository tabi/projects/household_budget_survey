import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../../../infrastructure/dtos/external_survey_dto.dart';
import '../../../../infrastructure/utils/color_pallette.dart';
import '../../../../infrastructure/utils/text_style_palette.dart';

class SurveyTile extends StatelessWidget {
  final ExternalSurveyDto survey;
  final dateFormat = DateFormat('d-MM-yyyy');

  SurveyTile(this.survey);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: ColorPalette.backgroundWhite,
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.all(16),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    survey.completionDate != null
                        ? Icon(
                            Icons.check_circle_rounded,
                            color: ColorPalette.green200,
                            size: 18,
                          )
                        : Icon(
                            Icons.file_copy_rounded,
                            color: ColorPalette.brand,
                            size: 18,
                          ),
                    Text(''),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 16.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        survey.surveyName,
                        style: TextStylePalette.primaryAkko16w400,
                      ),
                      Text(
                        survey.completionDate != null
                            ? dateFormat.format(survey.completionDate!).toString()
                            : dateFormat.format(survey.dueDate).toString(),
                        style: TextStylePalette.greyAkko14w400,
                      ),
                    ],
                  ),
                ),
                Expanded(child: Container()),
                InkWell(
                  child: Icon(
                    Icons.link_rounded,
                    color: ColorPalette.brand,
                  ),
                  onTap: () {},
                ),
              ],
            ),
          ),
          Divider(
            height: 1,
          ),
        ],
      ),
    );
  }
}
