import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import '../../../../../../infrastructure/utils/date_util.dart';

class FormatButton extends StatelessWidget {
  final CalendarFormat calendarFormat;
  final ValueChanged<CalendarFormat> onTap;
  final BoxDecoration decoration;
  final EdgeInsets padding;
  final Icon iconUp;
  final Icon iconDown;
  final bool showsNextFormat;
  final Map<CalendarFormat, String> availableCalendarFormats;

  const FormatButton({
    Key? key,
    required this.calendarFormat,
    required this.onTap,
    required this.decoration,
    required this.padding,
    required this.iconUp,
    required this.iconDown,
    required this.showsNextFormat,
    required this.availableCalendarFormats,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final child = Container(
      decoration: decoration,
      padding: padding,
      child: _formatButtonContent,
    );

    final platform = Theme.of(context).platform;

    return !kIsWeb && (platform == TargetPlatform.iOS || platform == TargetPlatform.macOS)
        ? CupertinoButton(
            onPressed: () => onTap(_nextFormat()),
            padding: EdgeInsets.zero,
            child: child,
          )
        : InkWell(
            onTap: () => onTap(_nextFormat()),
            child: child,
          );
  }

  Icon get _formatButtonContent {
    if (showsNextFormat) {
      if (availableCalendarFormats[_nextFormat()]! == "Month") {
        return iconDown;
      } else if (availableCalendarFormats[_nextFormat()]! == "Week") {
        return iconUp;
      } else {
        return Icon(
          Icons.question_mark_rounded,
        );
      }
    } else {
      if (availableCalendarFormats[calendarFormat]! == "Month") {
        return iconUp;
      } else if (availableCalendarFormats[calendarFormat]! == "Week") {
        return iconDown;
      } else {
        return Icon(
          Icons.question_mark_rounded,
        );
      }
    }
  }

  CalendarFormat _nextFormat() {
    final formats = availableCalendarFormats.keys.toList();
    int id = formats.indexOf(calendarFormat);
    id = (id + 1) % formats.length;

    return formats[id];
  }
}
