import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../../infrastructure/notifiers/overview_notifiers/overview_notifier.dart';
import '../../../../infrastructure/notifiers/translation_notifier.dart';
import '../../../../infrastructure/utils/color_pallette.dart';
import '../../../../infrastructure/utils/text_style_palette.dart';
import '../../../modal_bottomsheets/overview_daycounter_info_drawer.dart';

class DayCounterWaitWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final overviewNotifier = context.watch<OverviewNotifier>();
    final translationNotifier = context.watch<TranslationNotifier>();
    return Row(
      children: [
        Container(
          width: 28,
          alignment: Alignment.center,
          margin: EdgeInsets.all(2),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(4)),
            color: ColorPalette.brand,
          ),
          child: Padding(
            padding: EdgeInsets.all(6),
            child: Text(
              overviewNotifier.daysUntilStart > 9 ? overviewNotifier.daysUntilStart.toString()[0] : '0',
              style: TextStylePalette.whiteSoho20w600,
            ),
          ),
        ),
        Container(
          width: 28,
          alignment: Alignment.center,
          margin: EdgeInsets.all(2),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(4)),
            color: ColorPalette.brand,
          ),
          child: Padding(
            padding: EdgeInsets.all(6),
            child: Text(
              overviewNotifier.daysUntilStart > 9
                  ? overviewNotifier.daysUntilStart.toString()[1]
                  : overviewNotifier.daysUntilStart.toString(),
              style: TextStylePalette.whiteSoho20w600,
            ),
          ),
        ),
        Container(
          alignment: Alignment.center,
          margin: EdgeInsets.only(left: 12),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    translationNotifier.getText('still') +
                        ' ' +
                        overviewNotifier.daysUntilStart.toString() +
                        ' ' +
                        translationNotifier.getText('daysToWait'),
                    style: TextStylePalette.primaryAkko16w600,
                  ),
                  Container(
                    height: 18,
                    child: IconButton(
                      padding: EdgeInsets.zero,
                      constraints: BoxConstraints(),
                      icon: Icon(
                        Icons.info_rounded,
                        color: ColorPalette.aqua300,
                      ),
                      iconSize: 18,
                      onPressed: () {
                        getOverviewInfoDrawer(context, overviewNotifier, translationNotifier);
                      },
                    ),
                  ),
                ],
              ),
              Text(translationNotifier.getText('untilStartOfSurvey'), style: TextStylePalette.greyAkko16w400),
            ],
          ),
        ),
      ],
    );
  }
}
