import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../../infrastructure/notifiers/overview_notifiers/overview_notifier.dart';
import '../../../../infrastructure/notifiers/translation_notifier.dart';
import '../../../../infrastructure/utils/color_pallette.dart';
import '../../../../infrastructure/utils/text_style_palette.dart';
import '../../../modal_bottomsheets/overview_daycounter_info_drawer.dart';

class DayCounterProgressWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final overviewNotifier = context.watch<OverviewNotifier>();
    final translationNotifier = context.watch<TranslationNotifier>();
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 18),
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(2)),
              color: ColorPalette.green200,
            ),
            child: Padding(
              padding: EdgeInsets.all(7),
              child: Icon(
                Icons.check_rounded,
                color: Colors.white,
                size: 24,
              ),
            ),
          ),
        ),
        Expanded(
          child: Container(
            alignment: Alignment.centerLeft,
            margin: EdgeInsets.only(
              left: 12,
              top: 4,
              bottom: 4,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 4),
                      child: Text(
                        overviewNotifier.getCompletedDays().toString() +
                            translationNotifier.getText('divider') +
                            overviewNotifier.surveyDayList.length.toString() +
                            ' ' +
                            translationNotifier.getText('daysCompleted'),
                        style: TextStylePalette.primaryAkko16w600,
                      ),
                    ),
                    Container(
                      height: 18,
                      child: IconButton(
                        padding: EdgeInsets.zero,
                        constraints: BoxConstraints(),
                        icon: Icon(
                          Icons.info_rounded,
                          color: ColorPalette.aqua300,
                        ),
                        iconSize: 18,
                        onPressed: () {
                          getOverviewInfoDrawer(context, overviewNotifier, translationNotifier);
                        },
                      ),
                    ),
                  ],
                ),
                LinearProgressIndicator(
                  value: overviewNotifier.getCompletedDays() / overviewNotifier.surveyDayList.length,
                  minHeight: 8,
                  color: ColorPalette.aqua200,
                  backgroundColor: ColorPalette.grey100,
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
