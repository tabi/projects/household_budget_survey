import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../../infrastructure/notifiers/overview_notifiers/overview_notifier.dart';
import '../../../../infrastructure/notifiers/translation_notifier.dart';
import '../../../../infrastructure/utils/color_pallette.dart';
import '../../../../infrastructure/utils/text_style_palette.dart';

class DayCounterFinishedWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final overviewNotifier = context.watch<OverviewNotifier>();
    final translationNotifier = context.watch<TranslationNotifier>();
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Icon(
          Icons.check_circle,
          color: ColorPalette.green200,
          size: 18,
        ),
        Container(
          width: 16,
        ),
        Flexible(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                translationNotifier.getText('completed'),
                style: TextStylePalette.primaryAkko16w600,
              ),
              Text(
                translationNotifier.getText('completedBottomText'),
                style: TextStylePalette.primaryAkko16w400,
              ),
            ],
          ),
        ),
        Container(
          width: 16,
        ),
        InkWell(
          child: Icon(
            Icons.close_rounded,
            size: 20,
            color: ColorPalette.textPrimary,
          ),
          onTap: () {
            overviewNotifier.closedCompletionWaring = true;
          },
        ),
      ],
    );
  }
}
