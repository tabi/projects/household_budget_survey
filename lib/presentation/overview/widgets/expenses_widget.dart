import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../infrastructure/notifiers/overview_notifiers/overview_notifier.dart';
import '../../../infrastructure/notifiers/receipt_list_notifier.dart';
import '../../../infrastructure/notifiers/translation_notifier.dart';
import '../../../infrastructure/utils/color_pallette.dart';
import '../../../infrastructure/utils/date_util.dart';
import '../../../infrastructure/utils/text_style_palette.dart';
import '../../expenses_detail_page/expenses_detail_page.dart';
import '../../modal_bottomsheets/add_new_receipt_drawer.dart';
import '../../modal_bottomsheets/overview_add_receipt_warning.dart';
import '../../modal_bottomsheets/overview_complete_empty_warning_drawer.dart';

class ExpensesWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final translationNotifier = context.watch<TranslationNotifier>();
    final overviewNotifier = context.watch<OverviewNotifier>();
    final receiptListNotifier = context.watch<ReceiptListNotifier>();

    return Container(
      color: ColorPalette.backgroundWhite,
      padding: const EdgeInsets.all(16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.symmetric(vertical: 16),
            color: ColorPalette.backgroundWhite,
            child: Text(
              getDateStringExpensesTitle(overviewNotifier.selectedDay, translationNotifier),
              style: TextStylePalette.brandSoho20w600,
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                translationNotifier.getText('totalExpense'),
                style: TextStylePalette.blackAkko16w600,
              ),
              Text(
                receiptListNotifier.getNumberRecipesAtDate(overviewNotifier.selectedDay).toString() +
                    ' ' +
                    translationNotifier.getText('productsServices'),
                style: TextStylePalette.greyAkko14w400,
              ),
              Container(
                padding: EdgeInsets.only(top: 16, bottom: 16),
                child: Text(
                  translationNotifier.getText('currencySymbol') +
                      receiptListNotifier.getTotalReceiptsPriceAtDate(overviewNotifier.selectedDay).toStringAsFixed(2),
                  style: TextStylePalette.blackAkko20w600,
                ),
              ),
              Divider(),
              Padding(
                padding: const EdgeInsets.only(top: 8.0, bottom: 28.0),
                child: InkWell(
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          translationNotifier.getText('checkExpenses'),
                          style: TextStylePalette.aquaAkko16w400,
                        ),
                        Icon(
                          Icons.chevron_right_rounded,
                          color: ColorPalette.aqua300,
                          size: 24,
                        ),
                      ],
                    ),
                    onTap: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => ExpensesDetailPage(overviewNotifier.selectedDay),
                        ),
                      );
                    }),
              ),
              SizedBox(
                height: 48,
                child: TextButton(
                  style: TextButton.styleFrom(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(4.0),
                    ),
                    foregroundColor: Colors.white,
                    backgroundColor: ColorPalette.aqua200,
                    minimumSize: Size.fromHeight(44),
                  ),
                  onPressed: () {
                    getModalAddDrawer(context, translationNotifier, overviewNotifier.selectedDay);
                    if (!overviewNotifier.closedOutsideSurveyPeriodWaring &&
                        !overviewNotifier.isWithinSurveyPeriod(overviewNotifier.selectedDay)) {
                      getOverviewAddExpenseWarning(
                        context,
                        overviewNotifier,
                        translationNotifier,
                      );
                    }
                  },
                  child: Text(
                    translationNotifier.getText('addExpense'),
                    style: TextStylePalette.whiteAkko16w400,
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.all(8),
              ),
              SizedBox(
                height: 48,
                child: OutlinedButton(
                  style: OutlinedButton.styleFrom(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(4.0),
                    ),
                    foregroundColor: (overviewNotifier.isAllowedToComplete(overviewNotifier.selectedDay) &&
                            overviewNotifier.isWithinSurveyPeriod(overviewNotifier.selectedDay))
                        ? ColorPalette.aqua300
                        : ColorPalette.grey300,
                    minimumSize: Size.fromHeight(44),
                    side: BorderSide(
                      color: (overviewNotifier.isAllowedToComplete(overviewNotifier.selectedDay) &&
                              overviewNotifier.isWithinSurveyPeriod(overviewNotifier.selectedDay))
                          ? ColorPalette.aqua300
                          : ColorPalette.grey300,
                    ),
                  ),
                  onPressed: () {
                    if (overviewNotifier.isWithinSurveyPeriod(overviewNotifier.selectedDay) &&
                        overviewNotifier.isAllowedToComplete(overviewNotifier.selectedDay)) {
                      if (receiptListNotifier.getNumberRecipesAtDate(overviewNotifier.selectedDay) > 0) {
                        overviewNotifier.completeDay(overviewNotifier.selectedDay);
                      } else {
                        getOverviewCompleteEmptyDayWarning(context, overviewNotifier, translationNotifier);
                      }
                    }
                  },
                  child: Text(
                    translationNotifier.getText('completeDay'),
                    style: (overviewNotifier.isAllowedToComplete(overviewNotifier.selectedDay) &&
                            overviewNotifier.isWithinSurveyPeriod(overviewNotifier.selectedDay))
                        ? TextStylePalette.aquaAkko16w400
                        : TextStylePalette.greyAkko16w400,
                  ),
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}
