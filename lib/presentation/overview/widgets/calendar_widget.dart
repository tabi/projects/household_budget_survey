import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../infrastructure/notifiers/overview_notifiers/overview_notifier.dart';
import '../../../infrastructure/notifiers/translation_notifier.dart';
import '../../../infrastructure/utils/color_pallette.dart';
import '../../../infrastructure/utils/date_util.dart';
import 'calendar/cbs_calendar.dart';

class CalendarWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final overviewNotifier = context.watch<OverviewNotifier>();
    final translationNotifier = context.watch<TranslationNotifier>();

    return Container(
      padding: EdgeInsets.all(16),
      color: ColorPalette.backgroundWhite,
      child: TableCalendar(
        surveyDays: overviewNotifier.surveyDayList,
        translationNotifier: translationNotifier,
        overviewNotifier: overviewNotifier,
        firstDay: overviewNotifier.kFirstDay,
        lastDay: overviewNotifier.kLastDay,
        locale: translationNotifier.localeString,
        currentDay: DateTime.now(),
        focusedDay: overviewNotifier.focusedDay,
        calendarFormat: overviewNotifier.calendarFormat,
        rangeStartDay: overviewNotifier.surveyDayList[0].date,
        rangeEndDay: overviewNotifier.surveyDayList[overviewNotifier.surveyDayList.length - 1].date,
        selectedDayPredicate: (day) {
          return isSameDay(overviewNotifier.selectedDay, day);
        },
        onDaySelected: (selectedDay, focusedDay) {
          if (!isSameDay(overviewNotifier.selectedDay, selectedDay)) {
            overviewNotifier.selectedDay = selectedDay;
            overviewNotifier.focusedDay = focusedDay;
          }
        },
        onFormatChanged: (format) {
          if (overviewNotifier.calendarFormat != format) {
            overviewNotifier.calendarFormat = format;
          }
        },
        onPageChanged: (focusedDay) {
          overviewNotifier.focusedDay = focusedDay;
        },
      ),
    );
  }
}
