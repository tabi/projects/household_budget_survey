import 'package:flutter/material.dart';

import '../../../infrastructure/utils/color_pallette.dart';
import 'calendar_widget.dart';
import 'daycounter_widget.dart';
import 'expenses_widget.dart';

class DiaryTabWidget extends StatelessWidget {
  const DiaryTabWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          DayCounterWidget(),
          Container(
            color: ColorPalette.backgroundOffWhite,
            padding: EdgeInsets.all(4),
          ),
          CalendarWidget(),
          Container(
            color: ColorPalette.backgroundOffWhite,
            padding: EdgeInsets.all(4),
          ),
          ExpensesWidget(),
        ],
      ),
    );
  }
}
