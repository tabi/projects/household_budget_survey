import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../infrastructure/notifiers/translation_notifier.dart';
import '../../../infrastructure/utils/color_pallette.dart';
import '../../../infrastructure/utils/text_style_palette.dart';
import 'survey/surveylist_widget.dart';

class SurveyTabWidget extends StatelessWidget {
  const SurveyTabWidget({super.key});

  @override
  Widget build(BuildContext context) {
    final translationNotifier = context.watch<TranslationNotifier>();
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Container(
          padding: EdgeInsets.all(16),
          child: Text(
            translationNotifier.getText('surveyText'),
            style: TextStylePalette.greyAkko14w400,
          ),
          color: ColorPalette.backgroundWhite,
        ),
        Flexible(
          child: SurveyListWidget(),
        ),
      ],
    );
  }
}
