import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../infrastructure/notifiers/overview_notifiers/overview_notifier.dart';
import '../../../infrastructure/utils/color_pallette.dart';
import 'daycounter/daycounter_finished_widget.dart';
import 'daycounter/daycounter_progress_widget.dart';
import 'daycounter/daycounter_wait_widget.dart';

class DayCounterWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final overviewNotifier = context.watch<OverviewNotifier>();

    if (overviewNotifier.daysUntilStart > 0) {
      //Survey has not started yet
      return Container(
        padding: EdgeInsets.all(16),
        child: DayCounterWaitWidget(),
        color: ColorPalette.backgroundWhite,
      );
    } else if (overviewNotifier.getCompletedDays() < overviewNotifier.surveyDayList.length) {
      //Survey has started but not completed
      return Container(
        padding: EdgeInsets.all(16),
        child: DayCounterProgressWidget(),
        color: ColorPalette.backgroundWhite,
      );
    } else if (overviewNotifier.getCompletedDays() >= overviewNotifier.surveyDayList.length &&
        !overviewNotifier.closedCompletionWaring) {
      //Survey completed and warning is not closed
      return Container(
        padding: EdgeInsets.all(16),
        child: DayCounterFinishedWidget(),
        color: ColorPalette.greenOpacity,
      );
    } else {
      //Survey completed and warning is closed or otherwise
      return Container();
    }
  }
}
