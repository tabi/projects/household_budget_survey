import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../infrastructure/notifiers/overview_notifiers/surveylist_notifier.dart';
import '../../infrastructure/notifiers/translation_notifier.dart';
import '../../infrastructure/utils/color_pallette.dart';
import '../../infrastructure/utils/text_style_palette.dart';
import 'widgets/diary_tab_widget.dart';
import 'widgets/survey_tab_widget.dart';

class OverviewScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final translationNotifier = context.watch<TranslationNotifier>();
    final surveyNotifier = context.watch<SurveyListNotifier>();

    return Scaffold(
      backgroundColor: ColorPalette.backgroundWhite,
      body: SafeArea(
        child: surveyNotifier.surveyList.length > 0
            ? DefaultTabController(
                length: 2,
                child: Scaffold(
                  backgroundColor: ColorPalette.backgroundOffWhite,
                  appBar: AppBar(
                    title: Text(
                      translationNotifier.getText('overview'),
                      style: TextStylePalette.brandSoho26w600,
                    ),
                    scrolledUnderElevation: 0.0,
                    leadingWidth: 0,
                    centerTitle: false,
                    backgroundColor: ColorPalette.backgroundWhite,
                    bottom: TabBar(
                      indicatorSize: TabBarIndicatorSize.tab,
                      indicatorColor: ColorPalette.brand,
                      unselectedLabelStyle: TextStylePalette.blackAkko16w400,
                      labelStyle: TextStylePalette.brandAkko16w600,
                      tabs: [
                        Tab(
                          text: translationNotifier.getText('mainPageTabDiary'),
                        ),
                        Tab(
                          text: translationNotifier.getText('mainPageTabSurveys'),
                        ),
                      ],
                    ),
                  ),
                  body: TabBarView(
                    children: [
                      DiaryTabWidget(),
                      SurveyTabWidget(),
                    ],
                  ),
                ),
              )
            : Scaffold(
                backgroundColor: ColorPalette.backgroundOffWhite,
                appBar: AppBar(
                  title: Text(
                    translationNotifier.getText('overview'),
                    style: TextStylePalette.brandSoho26w600,
                  ),
                  centerTitle: false,
                  backgroundColor: ColorPalette.backgroundWhite,
                  scrolledUnderElevation: 0.0,
                ),
                body: SafeArea(child: DiaryTabWidget()),
              ),
      ),
    );
  }
}
