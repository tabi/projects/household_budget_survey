import 'package:flutter/material.dart';
import '../dtos/survey_day_dto.dart';
import '../repositories/survey_day_repository.dart';
import '../utils/date_util.dart';
import '../../presentation/onboarding/widgets/onboarding_diaryexplanation_page.dart';
import '../../presentation/onboarding/widgets/onboarding_email_page.dart';
import '../../presentation/onboarding/widgets/onboarding_period_confirmation_page.dart';
import '../../presentation/onboarding/widgets/onboarding_survey_start_page.dart';
import '../../presentation/onboarding/widgets/onboarding_surveyexplanation_page.dart';
import '../../presentation/onboarding/widgets/onboarding_welcome_page.dart';

class OnboardingNotifier extends ChangeNotifier {
  final SurveyDayRepository _surveyDayRepository;
  List<StartEndDateObject> suggestedDates = [];
  static const int SURVEY_LENGTH = 13;

  OnboardingNotifier(this._surveyDayRepository) {
    suggestedDates.add(StartEndDateObject(DateTime.now().next(DateTime.monday),
        DateTime.now().next(DateTime.monday).add(const Duration(days: SURVEY_LENGTH))));
    for (int i = 1; i <= 5; i++) {
      DateTime referenceDate = suggestedDates.first.startDate.add(Duration(days: 7 * i));
      suggestedDates.add(StartEndDateObject(referenceDate, referenceDate.add(const Duration(days: SURVEY_LENGTH))));
    }
  }

  PageController _controller = PageController(viewportFraction: 1, keepPage: false);
  PageController get controller => _controller;

  TextEditingController _emailController = new TextEditingController();
  TextEditingController get emailController => _emailController;

  int _index = 0;

  int get index => _index;

  set index(int _index) {
    this._index = _index;

    if (index == pages.length - 1) {
      reachedEnd = true;
    } else {
      reachedEnd = false;
    }
    notifyListeners();
  }

  bool _reachedEnd = false;

  bool get reachedEnd => _reachedEnd;

  set reachedEnd(bool _reachedEnd) {
    this._reachedEnd = _reachedEnd;
    notifyListeners();
  }

  StartEndDateObject? _selectedPeriod;

  StartEndDateObject? get selectedPeriod => _selectedPeriod;

  set selectedPeriod(StartEndDateObject? _selectedPeriod) {
    this._selectedPeriod = _selectedPeriod;
    notifyListeners();
  }

  bool isSurveyStartPage() {
    return pages[index] is OnboardingSurveyStartPage;
  }

  bool isEmailPage() {
    return pages[index] is OnboardingEmailPage;
  }

  bool surveyDateSelected() {
    return selectedPeriod != null;
  }

  void onNextButton() {
    if (index < pages.length - 1) {
      index = index + 1;
      controller.animateToPage(index, duration: Durations.medium3, curve: Easing.standardAccelerate);
    }
    notifyListeners();
  }

  List<Widget> pages = [
    //OnboardingEmailPage(),
    OnboardingWelcomePage(),
    OnboardingSurveyExplanationPage(),
    OnboardingDiaryExplanationPage(),
    OnboardingSurveyStartPage(),
    OnboardingPeriodConfirmationPage(),
  ];

  Future<void> initializeDays(DateTime startDate, int daysAmount) async {
    for (var i = 0; i <= daysAmount; i++) {
      await _surveyDayRepository
          .insertSurveyDay(SurveyDayDto.create(date: DateTime(startDate.year, startDate.month, startDate.day + i)));
    }
  }
}
