import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

enum SortOrder { dateNewOld, dateOldNew, priceHighLow, priceLowHigh }

class FilterNotifier extends ChangeNotifier {
  FilterSettings savedFilters = FilterSettings.createNewSettings();

  SortOrder _selectedOrder = SortOrder.dateNewOld;

  TextEditingController fromDateController = TextEditingController();
  TextEditingController toDateController = TextEditingController();
  DateTime? pickedFromDate;
  DateTime? pickedToDate;

  double minPrice = 0.0;
  double maxPrice = 10000.0;
  RangeValues _priceValues = RangeValues(0, 10000);
  TextEditingController fromPriceController = TextEditingController();
  TextEditingController toPriceController = TextEditingController();

  bool expenseManualChecked = false;
  bool expenseScannedChecked = false;
  bool expenseDigitalChecked = false;

  int selectedFilterCount = 0;

  SortOrder get selectedOrder => _selectedOrder;
  set selectedOrder(SortOrder newOrder) {
    this._selectedOrder = newOrder;
    notifyListeners();
  }

  RangeValues get priceValues => _priceValues;
  set priceValues(RangeValues newRange) {
    this._priceValues = newRange;
    fromPriceController.text = newRange.start.round().toString();
    toPriceController.text = newRange.end.round().toString();
    notifyListeners();
  }

  set manualChecked(bool isChecked) {
    this.expenseManualChecked = isChecked;
    notifyListeners();
  }

  set scannedChecked(bool isChecked) {
    this.expenseScannedChecked = isChecked;
    notifyListeners();
  }

  set digitalChecked(bool isChecked) {
    this.expenseDigitalChecked = isChecked;
    notifyListeners();
  }

  onTapFromDateFunction({required BuildContext context}) async {
    DateTime? pickedDate = await showDatePicker(
      context: context,
      firstDate: DateTime(2020),
      lastDate: pickedToDate != null ? pickedToDate! : DateTime(2030),
      initialDate: pickedFromDate != null ? pickedFromDate! : DateTime.now(),
    );
    if (pickedDate == null) return;
    pickedFromDate = pickedDate;
    fromDateController.text = DateFormat('yyyy-MM-dd').format(pickedDate);
    notifyListeners();
  }

  onTapToDateFunction({required BuildContext context}) async {
    DateTime? pickedDate = await showDatePicker(
      context: context,
      firstDate: pickedFromDate != null ? pickedFromDate! : DateTime(2020),
      lastDate: DateTime(2030),
      initialDate: pickedFromDate != null ? pickedFromDate! : DateTime.now(),
    );
    if (pickedDate == null) return;
    pickedToDate = pickedDate;
    toDateController.text = DateFormat('yyyy-MM-dd').format(pickedDate);
    notifyListeners();
  }

  clearAllFilters() {
    selectedOrder = SortOrder.dateNewOld;
    priceValues = RangeValues(0, 10000);
    pickedFromDate = null;
    pickedToDate = null;
    fromDateController.clear();
    toDateController.clear();
    fromPriceController.clear();
    toPriceController.clear();
    expenseManualChecked = false;
    expenseScannedChecked = false;
    expenseDigitalChecked = false;
    savedFilters = FilterSettings.createNewSettings();
    notifyListeners();
  }

  undoFiltersOnClose() {
    savedFilters.order != null ? selectedOrder = savedFilters.order! : selectedOrder = SortOrder.dateNewOld;

    savedFilters.fromDate != null
        ? fromDateController.text = DateFormat('yyyy-MM-dd').format(savedFilters.fromDate!)
        : fromDateController.clear();
    savedFilters.toDate != null ? DateFormat('yyyy-MM-dd').format(savedFilters.toDate!) : toDateController.clear();
    pickedFromDate = savedFilters.fromDate;
    pickedToDate = savedFilters.toDate;

    if (savedFilters.priceRange != null) {
      priceValues = savedFilters.priceRange!;

      fromPriceController.text = priceValues.start.round().toString();
      toPriceController.text = priceValues.end.round().toString();
    } else {
      priceValues = RangeValues(0, 10000);

      fromPriceController.clear();
      toPriceController.clear();
    }

    expenseManualChecked = savedFilters.selectedManual;
    expenseScannedChecked = savedFilters.selectedScanned;
    expenseDigitalChecked = savedFilters.selectedDigital;
    notifyListeners();
  }

  saveFilters() {
    FilterSettings newSettings = FilterSettings(this.selectedOrder, pickedFromDate, pickedToDate, priceValues,
        expenseManualChecked, expenseScannedChecked, expenseDigitalChecked);
    this.savedFilters = newSettings;
    this.selectedFilterCount = newSettings.getFilterCount();
    notifyListeners();
  }
}

class FilterSettings {
  SortOrder? order = SortOrder.dateNewOld;
  DateTime? fromDate;
  DateTime? toDate;
  RangeValues? priceRange;
  bool selectedManual = false;
  bool selectedScanned = false;
  bool selectedDigital = false;

  FilterSettings.createNewSettings() {
    this.order = SortOrder.dateNewOld;
    this.fromDate = null;
    this.toDate = null;
    this.priceRange = RangeValues(0, 10000);
    this.selectedManual = false;
    this.selectedScanned = false;
    this.selectedDigital = false;
  }

  FilterSettings(SortOrder order, DateTime? fromDate, DateTime? toDate, RangeValues? priceRange, bool selectedManual,
      bool selectedScanned, bool selectedDigital) {
    this.order = order;
    this.fromDate = fromDate;
    this.toDate = toDate;
    this.priceRange = priceRange;
    this.selectedManual = selectedManual;
    this.selectedScanned = selectedScanned;
    this.selectedDigital = selectedDigital;
  }

  int getFilterCount() {
    int i = 0;
    if (fromDate != null || toDate != null) i++;
    if (priceRange != RangeValues(0, 10000)) i++;
    if (selectedDigital != false || selectedScanned != false || selectedManual != false) i++;
    return i;
  }
}
