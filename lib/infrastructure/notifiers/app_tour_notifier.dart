import 'package:flutter/material.dart';

import '../../presentation/app_tour/widgets/complete_days_screen.dart';
import '../../presentation/app_tour/widgets/register_expenses_screen.dart';
import '../../presentation/login/login_screen.dart';

class AppTourNotifier extends ChangeNotifier {
  PageController _controller = PageController(viewportFraction: 1, keepPage: false);

  PageController get controller => _controller;

  int _index = 0;

  int get index => _index;

  set index(int _index) {
    this._index = _index;

    if (index == pages.length - 1) {
      reachedEnd = true;
    } else {
      reachedEnd = false;
    }
    notifyListeners();
  }

  bool _reachedEnd = false;

  bool get reachedEnd => _reachedEnd;

  set reachedEnd(bool _reachedEnd) {
    this._reachedEnd = _reachedEnd;
    notifyListeners();
  }

  void resetPage() {
    index = 0;
    reachedEnd = false;
    this._controller.jumpToPage(_controller.initialPage);
    notifyListeners();
  }

  void onSkipButton(BuildContext context) {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => LoginScreen(),
      ),
    );
  }

  void onNextButton() {
    if (index < pages.length - 1) {
      index = index + 1;
      controller.animateToPage(index, duration: Durations.medium3, curve: Easing.standardAccelerate);
    }
    notifyListeners();
  }

  void onLoginButton(BuildContext context) {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => LoginScreen(),
      ),
    );
  }

  List<Widget> pages = [
    AppTourRegisterScreen(),
    AppTourCompleteScreen(),
    //AppTourInsightScreen(),
  ];
}
