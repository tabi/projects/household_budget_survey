import 'package:flutter/material.dart';

class MenuNotifier extends ChangeNotifier {
  int _index = 0;

  int get index => _index;

  set index(int _index) {
    this._index = _index;
    notifyListeners();
  }
}
