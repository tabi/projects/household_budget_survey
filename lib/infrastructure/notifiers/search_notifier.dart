import 'package:flutter/widgets.dart';
import '../repositories/category_search_repository.dart';

import '../dtos/search_match_dto.dart';
import '../repositories/base_search_repository.dart';
import '../repositories/shop_search_repository.dart';
import '../services/string_match_service.dart';

class SearchNotifier extends ChangeNotifier {
  StringMatchingService _stringMatchingService;
  BaseSearchRepository _searchRepository;

  bool _recentSuggestions = true;
  var _matches = <SearchMatchDto>[];
  var _frequent = <SearchMatchDto>[];
  var _recent = <SearchMatchDto>[];
  String _prompt = '';

  SearchNotifier(this._stringMatchingService, this._searchRepository);

  List<SearchMatchDto> get matches => _matches;
  List<SearchMatchDto> get frequent => _frequent;
  List<SearchMatchDto> get recent => _recent;
  String get prompt => _prompt;
  bool get recentSuggestions => _recentSuggestions;
  bool get isStoreSearch => _searchRepository is ShopSearchRepository;
  bool get isCategorySearch => _searchRepository is CategorySearchRepository;

  set prompt(String prompt) => _prompt = prompt;
  set recentSuggestions(bool recentSuggestions) {
    _recentSuggestions = recentSuggestions;
    notifyListeners();
  }

  Future<void> search(String _prompt) async {
    prompt = _prompt;
    if (_prompt.isNotEmpty) {
      final all = await _searchRepository.getAll();
      _matches = _stringMatchingService.getBestMatches(_prompt, all);
      _matches.add(SearchMatchDto(frequency: 1, name: _prompt, category: 'Onbekend'));
      notifyListeners();
    }
  }

  Future<void> loadFrequent() async {
    _frequent = await _searchRepository.getFrequent();
    notifyListeners();
  }

  Future<void> loadRecent() async {
    _recent = await _searchRepository.getRecent();
    notifyListeners();
  }

  Future<void> add(SearchMatchDto item) async {
    await _searchRepository.add(item);
  }
}
