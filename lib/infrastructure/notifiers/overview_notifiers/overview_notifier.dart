import 'package:flutter/material.dart';

import '../../dtos/survey_day_dto.dart';
import '../../repositories/survey_day_repository.dart';
import '../../utils/date_util.dart';

class OverviewNotifier extends ChangeNotifier {
  final SurveyDayRepository _surveyDayRepository;

  int _daysUntilStart = 0;
  bool _closedCompletionWaring = false;
  bool _closedOutsideSurveyPeriodWaring = false;

  bool hasDisplayedFinale = false;

  List<SurveyDayDto> surveyDayList = [];

  DateTime kFirstDay = DateTime(DateTime.now().year, DateTime.now().month - 3, DateTime.now().day);
  DateTime kLastDay = DateTime(DateTime.now().year, DateTime.now().month + 3, DateTime.now().day);

  SurveyDayDto? _selectedSurveyDayDto;
  CalendarFormat _calendarFormat = CalendarFormat.week;
  DateTime _focusedDay = DateTime.now();
  DateTime _selectedDay = DateTime.now();

  CalendarFormat get calendarFormat => _calendarFormat;
  DateTime get focusedDay => _focusedDay;
  DateTime get selectedDay => _selectedDay;

  int get daysUntilStart => _daysUntilStart;
  bool get closedCompletionWaring => _closedCompletionWaring;
  bool get closedOutsideSurveyPeriodWaring => _closedOutsideSurveyPeriodWaring;

  SurveyDayDto? get selectedSurveyDayDto => _selectedSurveyDayDto;

  OverviewNotifier(this._surveyDayRepository) {
    loadSurveyDays();
  }

  Future<void> loadSurveyDays() async {
    surveyDayList = await _surveyDayRepository.getAllSurveyDays();
    if (surveyDayList.isNotEmpty) {
      for (var item in surveyDayList) {
        if (isSameDay(item.date, selectedDay)) {
          _selectedSurveyDayDto = item;
        }
      }
      daysUntilStart = daysBetween(DateTime.now(), surveyDayList.first.date);
    }
    notifyListeners();
  }

  set daysUntilStart(int _daysUntilStart) {
    this._daysUntilStart = _daysUntilStart;
    notifyListeners();
  }

  set closedOutsideSurveyPeriodWaring(bool _closedOutsideSurveyPeriodWaring) {
    this._closedOutsideSurveyPeriodWaring = _closedOutsideSurveyPeriodWaring;
    notifyListeners();
  }

  set closedCompletionWaring(bool _closedCompletionWaring) {
    this._closedCompletionWaring = _closedCompletionWaring;
    notifyListeners();
  }

  set calendarFormat(CalendarFormat format) {
    this._calendarFormat = format;
    notifyListeners();
  }

  set focusedDay(DateTime focused) {
    this._focusedDay = focused;
    notifyListeners();
  }

  set selectedDay(DateTime selected) {
    this._selectedDay = selected;
    for (var item in surveyDayList) {
      if (isSameDay(item.date, selectedDay)) {
        _selectedSurveyDayDto = item;
      }
    }
    notifyListeners();
  }

  bool isAllowedToComplete(DateTime selectedDate) {
    if (selectedSurveyDayDto != null) {
      return !selectedSurveyDayDto!.isCompleted && (selectedDate.isBefore(DateTime.now()) || isSameDay(DateTime.now(), selectedDate));
    } else {
      return false;
    }
  }

  bool isWithinSurveyPeriod(DateTime selectedDate) {
    return (selectedDate.isAfter(surveyDayList.first.date) && selectedDate.isBefore(surveyDayList.last.date));
  }

  void completeDay(DateTime day, [String? reason]) async {
    for (var item in surveyDayList) {
      if (isSameDay(item.date, day) && item.isCompleted == false) {
        item.isCompleted = true;
        item.reason = reason;
        await upsertSurveyDay(item);
      }
    }
    notifyListeners();
  }

  int getCompletedDays() {
    int completedAmount = 0;
    for (var item in surveyDayList) {
      if (item.isCompleted) completedAmount += 1;
    }
    return completedAmount;
  }

  Future<void> upsertSurveyDay(SurveyDayDto surveyDayDto) async {
    var index = surveyDayList.indexWhere((p) => p.surveyDayUuid == surveyDayDto.surveyDayUuid);
    if (index == -1) {
      surveyDayList.add(surveyDayDto);
    } else {
      await _surveyDayRepository.updateSurveyDay(surveyDayDto);
    }
  }
}
