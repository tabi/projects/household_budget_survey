import 'package:flutter/material.dart';

import '../dtos/tree_node_dto.dart';
import '../repositories/product_repository.dart';
import 'filter_notifier.dart';

class InsightsNotifier extends ChangeNotifier {
  final ProductRepository _productRepository;
  final FilterNotifier _filterNotifier;

  TreeNodeDto? _treeNodeDto;

  InsightsNotifier(this._productRepository, this._filterNotifier) {
    listenToTreeNodeDtoUpdates();
  }

  TreeNodeDto? get treeNodeDto => _treeNodeDto;

  Future<void> listenToTreeNodeDtoUpdates() async {
    _productRepository.streamRootTreeNode().listen((event) {
      _treeNodeDto = event;
      notifyListeners();
    });
  }

  void updateActiveTreeNodeDto(TreeNodeDto? treeNodeDto) {
    if (treeNodeDto == null) return;
    if (treeNodeDto.childNodes.isEmpty) return;
    _treeNodeDto = treeNodeDto;
    notifyListeners();
  }
}
