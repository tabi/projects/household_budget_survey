import 'package:flutter/material.dart';

import '../dtos/receipt_dto.dart';
import '../repositories/receipt_repository.dart';
import '../utils/date_util.dart';

class ReceiptListNotifier extends ChangeNotifier {
  final ReceiptRepository _receiptRepository;

  TextEditingController _searchFieldController = TextEditingController();

  TextEditingController get searchFieldController => _searchFieldController;

  String searchInput = '';

  updateSearchField(String newSearchInput) {
    searchInput = newSearchInput;
    notifyListeners();
  }

  clearSearchField() {
    searchFieldController.clear();
    searchInput = '';
    notifyListeners();
  }

  List<ReceiptDto> _transactions = [];

  ReceiptListNotifier(this._receiptRepository) {
    _listenToTransactionUpdates();
  }

  List<ReceiptDto> get transactions => _transactions;

  void _listenToTransactionUpdates() {
    _receiptRepository.streamReceipts().listen((event) {
      _transactions = event;
      notifyListeners();
    });
  }

  Future<void> deleteReceipt(ReceiptDto receiptDto) async {
    await _receiptRepository.deleteReceipt(receiptDto);
  }

  Future<void> duplicateReceipt(ReceiptDto receiptDto) async {
    await _receiptRepository.duplicateReceipt(receiptDto);
  }

  double getTotalReceiptsPriceAtDate(DateTime date) {
    final recipesDateMatched = _transactions.where((item) => isSameDay(item.boughtOn, date));
    double totalPrice = 0;
    for (var item in recipesDateMatched) {
      totalPrice += item.getTotalPrice();
    }
    return totalPrice;
  }

  int getNumberRecipesAtDate(DateTime date) {
    return _transactions.where((item) => isSameDay(item.boughtOn, date)).length;
  }

  List<ReceiptDto> getReceiptsAtDate(DateTime date) {
    return _transactions.where((item) => isSameDay(item.boughtOn, date)).toList();
  }
}
