import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

import '../enums/string_matching_config_enums.dart';

class ConfigurationNotifier with ChangeNotifier {
  //  Server Address
  late String _serverAddress;

  //  String Matching Configuration
  late StringMatchingAlgo _stringMatchingAlgo;
  late StringMatchingSorting _stringMatchingSorting;
  late int _resultsPerCategory;
  late int _maxResults;

  ConfigurationNotifier() {
    loadConfiguration();
  }

  String get serverAddress => _serverAddress;
  StringMatchingAlgo get stringMatchingAlgo => _stringMatchingAlgo;
  StringMatchingSorting get stringMatchingSorting => _stringMatchingSorting;
  int get resultsPerCategory => _resultsPerCategory;
  int get maxResults => _maxResults;

  Future<void> loadConfiguration() async {
    String jsonString = await rootBundle.loadString('assets/configuration.json');
    Map<String, dynamic> config = jsonDecode(jsonString) as Map<String, dynamic>;
    _serverAddress = config['severAddress'] as String;
    _stringMatchingAlgo = StringMatchingAlgo.values.byName(config['stringMatching']['algo'] as String);
    _stringMatchingSorting = StringMatchingSorting.values.byName(config['stringMatching']['sorting'] as String);
    _resultsPerCategory = int.parse(config['stringMatching']['resultsPerCategory'] as String);
    _maxResults = int.parse(config['stringMatching']['maxResults'] as String);
    notifyListeners();
  }
}
