import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:uuid/uuid.dart';

import '../dtos/product_dto.dart';
import '../dtos/receipt_dto.dart';
import '../repositories/receipt_repository.dart';

class TransactionManualNotifier with ChangeNotifier {
  late final ReceiptRepository _receiptRepository;
  late ReceiptDto _receiptDto;

  bool _isUpdate = false;
  bool _isModified = false;
  bool _showInputWarning = false;

  TransactionManualNotifier(ReceiptDto? updateReceipt) {
    _receiptRepository = GetIt.I<ReceiptRepository>();
    if (updateReceipt == null) {
      _receiptDto = ReceiptDto.create();
    } else {
      _isUpdate = true;
      _receiptDto = updateReceipt.copyWith();
    }
  }

  ReceiptDto get receipt => _receiptDto;
  ReceiptDto get displayReceipt => _receiptDto;
  bool get isModified => _isModified;
  bool get showInputWarning => _showInputWarning;
  bool get isComplete => hasShop() && hasProducts();

  void _updateReceipt() {
    this._isModified = true;
    notifyListeners();
  }

  void updateShowInputWarning(bool showInputWarning) {
    _showInputWarning = showInputWarning;
    notifyListeners();
  }

  void updateShop(String shopName, String shopCategory) {
    _receiptDto.shopName = shopName;
    _receiptDto.shopCategory = shopCategory;
    _updateReceipt();
  }

  void updateDate(DateTime boughtOn) {
    _receiptDto.boughtOn = boughtOn;
    _updateReceipt();
  }

  void updateAbroad(bool abroad) {
    _receiptDto.isAbroad = abroad;
    _updateReceipt();
  }

  void updateOnline(bool online) {
    _receiptDto.isOnline = online;
    _updateReceipt();
  }

  void upsertproduct(ProductDto product) {
    var index = _receiptDto.products.indexWhere((p) => p.uuid == product.uuid);
    if (index == -1) {
      _receiptDto.products.add(product);
    } else {
      _receiptDto.products[index] = product;
    }
    _updateReceipt();
  }

  void removeProduct(ProductDto product) {
    _receiptDto.products.remove(product);
    _updateReceipt();
  }

  void duplicateProduct(ProductDto product) {
    _receiptDto.products.add(product.copyWith(uuid: Uuid().v1()));
    _updateReceipt();
  }

  void updateDiscount(double? discountPercentage, double? discountAmount) {
    _receiptDto.discountAmount = 0;
    _receiptDto.discountPercentage = 0;
    if (discountPercentage != null) _receiptDto.discountPercentage = discountPercentage;
    if (discountAmount != null) _receiptDto.discountAmount = discountAmount;
    _updateReceipt();
  }

  bool hasShop() => _receiptDto.shopName != null;

  bool hasProducts() => _receiptDto.products.isNotEmpty;

  Future<void> save() async {
    if (_isUpdate) {
      _receiptRepository.updateReceipt(receipt);
    } else {
      _receiptRepository.insertReceipt(receipt);
    }
  }
}
