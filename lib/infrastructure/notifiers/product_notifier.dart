import 'package:flutter/material.dart';

import '../dtos/product_dto.dart';
import '../utils/extensions.dart';

class ProductNotifier extends ChangeNotifier {
  final ProductDto productDto;
  late bool showDiscountInput;

  late TextEditingController productNameController;
  late TextEditingController productCategoryController;
  late TextEditingController productDiscountController;
  late TextEditingController priceController;
  late bool isReturn;
  bool pressedSave = false;

  ProductNotifier(this.productDto) {
    showDiscountInput = productDto.discountAmount != null;
    fillForm();
  }

  void fillForm() {
    productNameController = TextEditingController(text: productDto.name!.capitalize());
    productCategoryController = TextEditingController(text: productDto.coicopName!.capitalize());
    priceController =
        TextEditingController(text: (productDto.price != null) ? productDto.price!.toStringAsFixed(2) : '');
    productDiscountController = TextEditingController(text: productDto.count.toString());
    isReturn = productDto.isReturn;
    notifyListeners();
  }

  void setProduct(String name, String category, String code) {
    productDto.name = name;
    productDto.coicopName = category;
    productDto.coicopCode = code;
    fillForm();
    notifyListeners();
  }

  void setCategory(String category, String code) {
    productDto.coicopName = category;
    productDto.coicopCode = code;
    fillForm();
    notifyListeners();
  }

  void setQuantity(int quantity) {
    productDto.count = quantity;
    notifyListeners();
  }

  void addItem() {
    productDto.count++;

    fillForm();
    notifyListeners();
  }

  void subtractItem() {
    if (productDto.count > 1) productDto.count--;

    fillForm();
    notifyListeners();
  }

  void setPrice(double? price) {
    productDto.price = price;
    notifyListeners();
  }

  void setDiscount(double? discount) {
    productDto.discountAmount = discount;
    notifyListeners();
  }

  void setReturn(bool newValue) {
    productDto.isReturn = newValue;
    fillForm();
    notifyListeners();
  }

  void setSavePressed(bool isPressed) {
    pressedSave = isPressed;
    notifyListeners();
  }

  void toggleDiscountInput(bool value) {
    if (value == false) setDiscount(null);
    showDiscountInput = value;
    notifyListeners();
  }

  bool isComplete() {
    return productDto.price != null;
  }
}
