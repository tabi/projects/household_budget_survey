import 'package:flutter/material.dart';
import 'package:household_budget_survey/infrastructure/repositories/sync_repository.dart';
import 'package:url_launcher/url_launcher.dart';

import '../network/auth_api.dart';
import '../repositories/device_repository.dart';

class LoginNotifier extends ChangeNotifier {
  final AuthApi _authApi;
  final DeviceRepository _deviceRepository;
  final SyncRepository _syncRepository;
  bool userNameCorrect = false;
  bool passwordCorrect = false;
  bool isTimeLocked = false;
  DateTime lastAttempt = DateTime.now();
  DateTime thisAttempt = DateTime.now();
  int attempts = 2;

  LoginNotifier(this._authApi, this._deviceRepository, this._syncRepository);

  bool _passwordVisible = false;

  bool isLoading = false;
  bool isLoginCorrect = true;
  bool hasUserFilled = true;
  bool hasPassFilled = true;

  TextEditingController usernameController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();

  bool get passwordVisible => _passwordVisible;

  set passwordVisible(bool _passwordVisible) {
    this._passwordVisible = _passwordVisible;
    notifyListeners();
  }

  void togglePassword() {
    _passwordVisible = !_passwordVisible;
    notifyListeners();
  }

  Future<bool> login() async {
    isLoginCorrect = false;
    if (usernameController.text.isEmpty) {
      hasUserFilled = false;
    } else {
      hasUserFilled = true;
    }
    if (passwordController.text.isEmpty) {
      hasPassFilled = false;
    } else {
      hasPassFilled = true;
    }

    try {
      final deviceDto = await _deviceRepository.getDeviceDTO();
      await _syncRepository.setServerTimeDiff();
      final response = await _authApi.login(usernameController.text, passwordController.text, deviceDto);
      if (response.statusCode == 401) {
        final infoString = response.message.split(',');
        userNameCorrect = infoString[0] == 'true';
        isTimeLocked = infoString[1] == 'true';
        lastAttempt = DateTime.parse(infoString[2].split('.')[0].trim());
        thisAttempt = DateTime.parse(infoString[3].split('.')[0].trim());
        attempts = int.parse(infoString[4]);
        // print(
        //     'Username is $userNameCorrect, isTimeLocked is $isTimeLocked, attempts is $attempts and last attempted to login is $lastAttempt');
      }
      if (response.payload == null) {
        return false;
      }

      isLoginCorrect = true;
      userNameCorrect = true;
      passwordCorrect = true;
    } catch (e) {
      print('Error [$e]');
      return false;
    }

    notifyListeners();
    return true;
  }

  bool isLoginSuccessful() {
    return isLoginCorrect && hasPassFilled && hasUserFilled;
  }

  Future<void> launchPhone() async {
    final url = Uri.parse('tel:045 - 505400');
    if (!await launchUrl(url)) {
      throw Exception('Could not launch $url');
    }
  }

  Future<void> launchMail() async {
    final url = Uri.parse('mailto:WINHelpdesk@cbs.nl');
    if (!await launchUrl(url)) {
      throw Exception('Could not launch $url');
    }
  }
}
