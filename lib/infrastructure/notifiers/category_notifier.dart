import 'package:flutter/material.dart';

import '../dtos/product_dto.dart';

class CategoryNotifier extends ChangeNotifier {
  late ProductDto _productDto;
  late TextEditingController _productCountFieldController;
  late TextEditingController _productPriceFieldController;
  TextEditingController _productCategoryFieldController = TextEditingController();

  CategoryNotifier(ProductDto productDto) {
    this._productDto = productDto;
    this._productCountFieldController = TextEditingController(text: productDto.count.toString());
    this._productPriceFieldController = TextEditingController(text: productDto.price.toString());
  }

  TextEditingController get productCategoryFieldController => _productCategoryFieldController;
  TextEditingController get productCountFieldController => _productCountFieldController;
  TextEditingController get productPriceFieldController => _productPriceFieldController;

  void setProductCategory(String productCategory) {
    this._productCategoryFieldController.text = productCategory;
    notifyListeners();
  }

  void setProductCount(String productCount) {
    this._productCountFieldController.text = productCount;
    notifyListeners();
  }

  void setProductPrice(String productPrice) {
    this._productPriceFieldController.text = productPrice;
    notifyListeners();
  }
}
