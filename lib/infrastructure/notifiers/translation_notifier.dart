import 'package:csv/csv.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../utils/csv_util.dart';

class TranslationNotifier extends ChangeNotifier {
  Map<String, Map<String, String>> _translations = {};
  Locale _locale = Locale('nl', 'NL');

  TranslationNotifier() {
    _loadLanguagePreference();
    _loadTranslationsFromCsv();
  }

  List<Locale> locales = [
    const Locale('nl', 'NL'),
    const Locale('en', 'UK'),
    const Locale('en', 'EN'),
  ];

  String get localeString => '${_locale.languageCode}_${_locale.countryCode}';
  String get languageCode => _locale.languageCode;
  Locale get locale => _locale;

  String getText(String key) {
    if (_translations.containsKey(localeString) && _translations[localeString]!.containsKey(key))
      return _translations[localeString]![key]!;
    return '?';
  }

  void setLocale(Locale newLocale) {
    if (_locale != newLocale) {
      _locale = newLocale;
      _loadTranslationsFromCsv();
    }
  }

  Future<void> _loadLanguagePreference() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final _language = prefs.getString('language') ?? locales.first.languageCode;
    final _country = prefs.getString('country') ?? locales.first.countryCode!;
    _locale = Locale(_language, _country);
    notifyListeners();
  }

  Future<void> _loadTranslationsFromCsv() async {
    final csvFiles = await getCsvFiles('assets/translations/');
    for (final path in csvFiles) {
      final csvString = await rootBundle.loadString(path);
      final rows = const CsvToListConverter().convert<dynamic>(csvString, fieldDelimiter: ';', eol: '\n');
      final translationMap = <String, String>{};
      for (int i = 1; i < rows.length; i++) {
        final row = rows[i];
        final key = row[0] as String;
        final value = row[1] as String;
        translationMap[key] = value;
      }
      _translations[translationMap['localizationKey']!] = translationMap;
    }
    notifyListeners();
  }
}
