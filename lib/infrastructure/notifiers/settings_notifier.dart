import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

enum SupportedLanguages { nl, en }

class SettingsNotifier extends ChangeNotifier {
  SupportedLanguages _selectedLanguage = SupportedLanguages.nl;

  SupportedLanguages get selectedLanguage => _selectedLanguage;

  set selectedLanguage(SupportedLanguages _selectedLanguage) {
    this._selectedLanguage = _selectedLanguage;
    notifyListeners();
  }

  Future<void> launchPhone() async {
    final url = Uri.parse('tel:045 - 505400');
    if (!await launchUrl(url)) {
      throw Exception('Could not launch $url');
    }
  }

  Future<void> launchMail() async {
    final url = Uri.parse('mailto:WINHelpdesk@cbs.nl');
    if (!await launchUrl(url)) {
      throw Exception('Could not launch $url');
    }
  }

  Future<void> launchPrivacy() async {
    final url = Uri.parse('https://www.cbs.nl/nl-nl/over-ons/dit-zijn-wij/onze-organisatie/privacy');
    if (!await launchUrl(url)) {
      throw Exception('Could not launch $url');
    }
  }

  Future<void> launchFAQ() async {
    final url = Uri.parse('https://www.cbs.nl/uitgaven');
    if (!await launchUrl(url)) {
      throw Exception('Could not launch $url');
    }
  }
}
