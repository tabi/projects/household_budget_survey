import '../database/database.dart';
import '../dtos/device_dto.dart';
import '../services/device_service.dart';

class DeviceRepository {
  final Database _database;
  final DeviceService _deviceService;

  DeviceRepository(this._database, this._deviceService);

  Future<bool> _hasDeviceInfo() async {
    final devices = await _database.devicesDao.getDevicesAsync();
    return devices.isNotEmpty;
  }

  Future<void> _initDeviceInfo() async {
    final deviceDTO = await _deviceService.getDeviceAsync();
    final device = Device(
        uuid: deviceDTO!.uuid,
        info: deviceDTO.info);
    await _database.devicesDao.insertDeviceAsync(device);
  }

  Future<DeviceDTO> getDeviceDTO() async {
    await _initDeviceInfo();
    if (await _hasDeviceInfo() == false) await _initDeviceInfo();
    final device = await _database.devicesDao.getDeviceAsync();
    final deviceDTO = DeviceDTO.fromDevice(device!);
    return deviceDTO;
  }
}
