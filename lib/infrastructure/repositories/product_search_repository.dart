
import '../database/database.dart';
import '../dtos/search_match_dto.dart';
import 'base_search_repository.dart';

class ProductSearchRepository extends BaseSearchRepository {
  final Database _database;

  ProductSearchRepository(this._database);

  @override
  Future<List<SearchMatchDto>> getAll() {
    final products = _database.coicopProductDao.getProducts();
    return products.then((value) => value.map((e) => SearchMatchDto.fromProduct(e)).toList());
  }

  @override
  Future<List<SearchMatchDto>> getFrequent() {
    throw UnimplementedError();
  }

  @override
  Future<List<SearchMatchDto>> getRecent() {
    throw UnimplementedError();
  }

  @override
  Future<void> add(SearchMatchDto item) {
    throw UnimplementedError();
  }
}
