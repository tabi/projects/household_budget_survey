import '../dtos/search_match_dto.dart';

abstract class BaseSearchRepository {
  Future<List<SearchMatchDto>> getAll();
  Future<List<SearchMatchDto>> getFrequent();
  Future<List<SearchMatchDto>> getRecent();
  Future<void> add(SearchMatchDto item);
}
