import 'dart:async';

import '../database/database.dart';
import '../dtos/product_with_coicop_dto.dart';
import '../dtos/tree_node_dto.dart';
import '../utils/date_util.dart';

class ProductRepository {
  final Database _database;

  ProductRepository(this._database);

  TreeNodeDto _buildEmptyCoicopTree(List<CoicopHierarchyData> coicopHierarchyData) {
    final root = TreeNodeDto(
      coicopName: 'root',
      totalCost: 0,
      expensesByDate: {},
      childNodes: {},
    );

    for (final coicopHierarchy in coicopHierarchyData) {
      TreeNodeDto currentNode = root;
      final levels = coicopHierarchy.coicop.split('.').map(int.parse).toList();
      for (int level in levels) {
        if (!currentNode.childNodes.containsKey(level)) {
          currentNode.childNodes[level] = TreeNodeDto(
            parentNode: currentNode,
            coicopName: coicopHierarchy.description,
            totalCost: 0,
            expensesByDate: {},
            childNodes: {},
          );
        }
        currentNode = currentNode.childNodes[level]!;
      }
    }

    return root;
  }

  TreeNodeDto _populateTreeWithProducts(TreeNodeDto root, List<ProductWithCoicopDto> productWithCoicopData) {
    for (final productWithCoicop in productWithCoicopData) {
      TreeNodeDto currentNode = root;

      double productCost = productWithCoicop.productData.price * productWithCoicop.productData.count;
      if (productWithCoicop.productData.isReturn) productCost = -productCost;
      // TODO: add discount information, from both receipt and product

      final levels = productWithCoicop.coicopHierarchyData.coicop.split('.').map(int.parse).toList();

      for (int level in levels) {
        if (currentNode.childNodes.containsKey(level)) {
          currentNode.totalCost += productCost;
          DateTime date = toYearMonthDay(productWithCoicop.receiptData.boughtOn);
          if (!currentNode.expensesByDate.containsKey(date)) {
            currentNode.expensesByDate[date] = [];
          }
          currentNode.expensesByDate[date]!.add(productCost);

          currentNode = currentNode.childNodes[level]!;
        }
      }
    }
    return root;
  }

  Stream<TreeNodeDto> streamRootTreeNode() {
    return _database.productDao.watchProductWithCoicopDtos().asyncMap((productWithCoicopData) async {
      final coicopHierarchyData = await _database.coicopHierarchyDao.getCoicopHierarchy();
      final emptyRoot = _buildEmptyCoicopTree(coicopHierarchyData);
      return _populateTreeWithProducts(emptyRoot, productWithCoicopData);
    });
  }
}
