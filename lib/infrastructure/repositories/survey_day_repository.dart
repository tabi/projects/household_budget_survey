import '../database/database.dart';
import '../dtos/survey_day_dto.dart';
import '../network/survey_day_api.dart';

class SurveyDayRepository {
  final Database _database;
  final SurveyDayApi _surveyDayApi;

  SurveyDayRepository(this._database, this._surveyDayApi);

  Future<List<SurveyDayDto>> getAllSurveyDays() async {
    final surveydays = _database.surveyDayDao.getSurveyDays();
    return surveydays.then((value) => value.map((e) => SurveyDayDto.fromSurveyDayData(e)).toList());
  }

  Future<void> insertSurveyDay(SurveyDayDto surveyDay) async {
    await _database.surveyDayDao.insertSurveyDay(surveyDay.toData());
    await _surveyDayApi.insertSurveyDay(surveyDay);
  }

  Future<void> updateSurveyDay(SurveyDayDto surveyDay) async {
    await _database.surveyDayDao.updateSurveyDay(
      surveyDay
          .copyWith(
            surveyDayUuid: surveyDay.surveyDayUuid,
            isCompleted: surveyDay.isCompleted,
            date: surveyDay.date,
            createdOn: surveyDay.createdOn,
            updatedOn: DateTime.now(),
            reason: surveyDay.reason,
          )
          .toData(),
    );
    await _surveyDayApi.updateSurveyDay(surveyDay);
  }
}
