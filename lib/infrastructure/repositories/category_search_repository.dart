import '../database/database.dart';
import '../dtos/search_match_dto.dart';
import 'base_search_repository.dart';

class CategorySearchRepository extends BaseSearchRepository {
  final Database _database;

  CategorySearchRepository(this._database);

  @override
  Future<List<SearchMatchDto>> getAll() {
    final coicopHierarchy = _database.coicopHierarchyDao.getCoicopHierarchy();
    return coicopHierarchy.then((value) => value.map((e) => SearchMatchDto.fromCoicopHierarchy(e)).toList());
  }

  @override
  Future<List<SearchMatchDto>> getFrequent() {
    throw UnimplementedError();
  }

  @override
  Future<List<SearchMatchDto>> getRecent() {
    throw UnimplementedError();
  }

  @override
  Future<void> add(SearchMatchDto item) {
    throw UnimplementedError();
  }
}
