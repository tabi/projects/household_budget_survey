import 'package:async/async.dart';
import 'package:household_budget_survey/infrastructure/network/sync_api.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:uuid/uuid.dart';

import '../database/database.dart';
import '../dtos/receipt_dto.dart';
import '../network/receipt_api.dart';
import 'sync_repository.dart';

class ReceiptRepository {
  final Database _database;
  final SyncRepository _syncRepository;
  final ReceiptApi _receiptApi;
  final SyncApi _syncApi;
  late final SharedPreferences sharedPreferences;
  DateTime _lastSyncTime = DateTime.fromMillisecondsSinceEpoch(0);

  ReceiptRepository(this._database, this._syncRepository, this._receiptApi, this._syncApi) {
    setup();
  }

  Future<void> setup() async {
    sharedPreferences = await SharedPreferences.getInstance();
  }

  Stream<List<ReceiptDto>> streamReceipts() {
    var streams = [_database.receiptDao.streamReceipts()];
    return StreamZip<List<ReceiptDto>>(streams).map((values) => values.expand((x) => x).toList());
  }

  Future<void> deleteReceipt(ReceiptDto receipt) async {
    final receiptUuid = Uuid().v4();
    final newReceipt = receipt.copyWith(
        uuid: receiptUuid,
        previousUuid: receipt.uuid,
        fromThisDevice: true,
        products: receipt.products.map((e) => e.copyWith(uuid: Uuid().v4(), receiptUuid: receiptUuid)).toList(),
        deletedOn: _syncRepository.getServerAdjustedNowTime());
    await insertReceipt(newReceipt);
  }

  Future<void> updateReceipt(ReceiptDto receipt) async {
    final receiptUuid = Uuid().v4();
    final newReceipt = receipt.copyWith(
      uuid: receiptUuid,
      fromThisDevice: true,
      previousUuid: receipt.uuid,
      products: receipt.products.map((e) => e.copyWith(uuid: Uuid().v4(), receiptUuid: receiptUuid)).toList(),
    );
    await insertReceipt(newReceipt);
  }

  Future<void> duplicateReceipt(ReceiptDto receipt) async {
    final receiptUuid = Uuid().v4();
    final newReceipt = receipt.copyWith(
      uuid: receiptUuid,
      fromThisDevice: true,
      groupUuid: Uuid().v4(),
      previousUuid: receipt.uuid,
      products: receipt.products.map((e) => e.copyWith(uuid: Uuid().v4(), receiptUuid: receiptUuid)).toList(),
    );
    insertReceipt(newReceipt);
  }

  Future<void> insertReceipt(ReceiptDto receipt) async {
    receipt.createdOn = _syncRepository.getServerAdjustedNowTime();
    _database.receiptDao.insertReceipt(receipt.toData(await _syncRepository.getIncreasedDeviceSyncOrder()));
    _database.productDao.insertProduct(receipt.products.map((e) => e.toData()).toList());
    await _receiptApi.upsertReceipt(receipt);
  }

  Future<void> syncReceipts() async {
    if (DateTime.now().difference(_lastSyncTime).inSeconds < 1) {
      print('syncReceipts function is rate limited');
      return;
    }
    _lastSyncTime = DateTime.now();

    await _syncRepository.setServerTimeDiff();
    int syncOrder = await _syncApi.getDeviceSyncOrderFromBackend("receipt");
    final receipts = await _database.receiptDao.getUnsyncedReceipts(syncOrder);
    for (final receipt in receipts) {
      await _receiptApi.upsertReceipt(receipt);
    }
    getReceiptsFromBackend();
  }

  Future<void> getReceiptsFromBackend() async {
    final backendSyncOrder = await getBackendSyncOrderOnDevice();
    final receipts = await _receiptApi.getReceipts(backendSyncOrder);
    for (final receipt in receipts) {
      final existingReceipt = await _database.receiptDao.getReceiptByGroupUuid(receipt.groupUuid);
      if (existingReceipt != null && existingReceipt.createdOn!.isAfter(receipt.createdOn!)) continue;
      _database.receiptDao.insertReceipt(receipt.toData(await _syncRepository.getIncreasedDeviceSyncOrder()));
      _database.productDao.insertProduct(receipt.products.map((e) => e.toData()).toList());
      setBackendSyncOrderOnDevice(receipt.syncOrder!);
    }
  }

  Future<int> getBackendSyncOrderOnDevice() async => sharedPreferences.getInt('backendSyncOrderOnDevice') ?? 0;

  Future<bool> setBackendSyncOrderOnDevice(int syncOrder) async => await sharedPreferences.setInt('backendSyncOrderOnDevice', syncOrder);
}
