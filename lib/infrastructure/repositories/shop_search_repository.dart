
import '../database/database.dart';
import '../dtos/search_match_dto.dart';
import 'base_search_repository.dart';

class ShopSearchRepository extends BaseSearchRepository {
  final Database _database;

  ShopSearchRepository(this._database);

  @override
  Future<List<SearchMatchDto>> getAll() {
    final shops = _database.shopDao.getShops();
    return shops.then((value) => value.map((e) => SearchMatchDto.fromShop(e)).toList());
  }

  @override
  Future<List<SearchMatchDto>> getFrequent() {
    throw UnimplementedError();
  }

  @override
  Future<List<SearchMatchDto>> getRecent() {
    throw UnimplementedError();
  }

  @override
  Future<void> add(SearchMatchDto item) {
    throw UnimplementedError();
  }
}
