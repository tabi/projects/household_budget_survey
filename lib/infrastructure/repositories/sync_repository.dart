import 'package:household_budget_survey/infrastructure/network/sync_api.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:uuid/uuid.dart';

class SyncRepository {
  final SyncApi _syncApi;

  late SharedPreferences prefs;
  late int _deviceSyncOrder;
  late int _backendSyncOrder;
  late String _deviceUuid;

  String get deviceUuid => _deviceUuid;
  int get backendSyncOrder => _backendSyncOrder;

  SyncRepository(this._syncApi) {
    _initialize();
  }

  Future<void> _initialize() async {
    prefs = await SharedPreferences.getInstance();
    _deviceSyncOrder = prefs.getInt('deviceSyncOrder') ?? 0;
    _backendSyncOrder = prefs.getInt('backendSyncOrder') ?? 0;
    _deviceUuid = prefs.getString('deviceUuid') ?? Uuid().v4();
    await prefs.setString('deviceUuid', deviceUuid);
  }

  Future<int> getIncreasedDeviceSyncOrder() async {
    await prefs.setInt('deviceSyncOrder', ++_deviceSyncOrder);
    return _deviceSyncOrder;
  }

  Future<void> setServerTimeDiff() async {
  final deviceTimePre = DateTime.now();
  final serverTime = await _syncApi.getServerTime();
  final deviceTimePost = DateTime.now();
    final deviceTimeAverage = deviceTimePre.add(
    deviceTimePost.difference(deviceTimePre) ~/ 2,
  );
  final diff = serverTime.difference(deviceTimeAverage);
  await prefs.setInt('serverTimeDiff', diff.inMilliseconds);
  print('Server time diff: ${diff.inMilliseconds}');
}


  DateTime getServerAdjustedNowTime() {
    final diff = Duration(milliseconds: prefs.getInt('serverTimeDiff') ?? 0);
    return DateTime.now().add(diff);
  }
}
