import '../database/database.dart';
import '../dtos/error_log_dto.dart';
import '../dtos/para_data_log_dto.dart';
import '../network/error_log_api.dart';
import '../network/para_data_log_api.dart';

class LogRepository {
  final int hourInMilliseconds = 900000;
  final ParaDataLogApi _paraDataLogApi;
  final ErrorLogApi _errorLogApi;
  final Database _database;
  // final DeviceRepository _deviceRepository;

  LogRepository(this._paraDataLogApi, this._errorLogApi, this._database);

  Future postErrorLogAsync(String message, String description) async {
    // await _deviceRepository.checkIfDeviceIsStored();
    final logDTO = ErrorLogDTO(
        message: message,
        description: description,
        type: 'ErrorLog',
        createdOn: DateTime.now().millisecondsSinceEpoch,
        synced: true);

    await _errorLogApi.postLog(logDTO);
  }

  Future postParaDataLogAsync(String action, String description, String page) async {
    // await _deviceRepository.checkIfDeviceIsStored();
    final logDTO = ParaDataLogDTO(
        action: action,
        description: description,
        page: page,
        createdOn: DateTime.now().millisecondsSinceEpoch,
        synced: true);

    await _paraDataLogApi.postLog(logDTO);
  }

  // Future<void> syncLogs({int limit = 5000}) async {
  //   final unsyncedLogs = await _database.logsDao.getUnSyncedLogs(limit);
  //   if (unsyncedLogs.isNotEmpty) {
  //     final _response = await _logApi.postLogs(LogDTO.fromList(unsyncedLogs));
  //     if (_response.isOk) {
  //       final synced = unsyncedLogs.map((e) => e.copyWith(synced: Value(true)));
  //       _database.logsDao.replaceBulkLog(synced);
  //     }
  //   }
  // }

  Future<int> getSyncedErrorCount() async => _database.errorLogsDao.getSyncedCount();

  Future<int> getUnsyncedErrorCount() async => _database.errorLogsDao.getUnsyncedCount();

  Future<int> getSyncedParaDataCount() async => _database.paraDataLogsDao.getSyncedCount();

  Future<int> getUnsyncedParaDataCount() async => _database.paraDataLogsDao.getUnsyncedCount();
}

// Future<void> log(String message, dynamic description, LogType logType) async {
//   final convertedDescripion = description is String ? description : '';
//   final db = container.read(database);
//   final log = Log(
//       message: message,
//       description: convertedDescripion,
//       synced: false,
//       type: logType.toString(),
//       date: DateTime.now());

//   await db.logsDao.insertLog(log);
// }

// Future<void> logAuth(String description) async {
//   final api = container.read(logApi);

//   await api.postLogs(LogDTO.fromList([
//     Log(
//         message: 'AuthNotifier::authenticate',
//         description: description,
//         synced: false,
//         type: LogType.Flow.toString(),
//         date: DateTime.now())
//   ]));
// }
