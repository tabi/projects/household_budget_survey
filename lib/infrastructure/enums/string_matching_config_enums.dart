import 'package:edit_distance/edit_distance.dart';

enum StringMatchingAlgo {
  JaroWinkler,
  Levenshtein,
  LongestCommonSubsequence,
  Jaccard,
}

extension mappers on StringMatchingAlgo {
  NormalizedStringDistance get interface {
    switch (this) {
      case StringMatchingAlgo.JaroWinkler:
        return JaroWinkler();
      case StringMatchingAlgo.Levenshtein:
        return Levenshtein();
      case StringMatchingAlgo.LongestCommonSubsequence:
        return LongestCommonSubsequence();
      case StringMatchingAlgo.Jaccard:
        return Jaccard();
    }
  }
}

enum StringMatchingSorting {
  ScoreFrequencyWordLength,
  ScoreWordLengthFrequency,
}
