import 'dart:io';

import 'package:camera/camera.dart';
import 'package:path_provider/path_provider.dart';

class ImageStorageDirectory {
  Future<String> getStorageDirectory() async {
    return (await getApplicationDocumentsDirectory()).path;
  }

  Future<String> saveImage(XFile imageFile) async {
    String dir = await getStorageDirectory();

    File directory = new File('$dir');
    if (directory.exists() != true) {
      directory.create();
    }

    await imageFile.saveTo('$directory/image.jpg');
    return '$directory/image.jpg';
  }

  Future<XFile?> getImageFromStorage(String imagePath) async {
    String dir = await getStorageDirectory();
    XFile? image;

    File directory = new File('$dir');
    if (directory.exists() == true) {
      if (await File(imagePath).exists()) {
        image = XFile(imagePath);
      }
    }

    return image;
  }
}
