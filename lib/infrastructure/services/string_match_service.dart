import 'dart:math';

import '../dtos/search_match_dto.dart';
import '../enums/string_matching_config_enums.dart';
import '../notifiers/configuration_notifier.dart';
import '../utils/extensions.dart';

class StringMatchingService {
  ConfigurationNotifier _configurationNotifier;

  StringMatchingService(this._configurationNotifier);

  double _getEditDistance(String productName, String userInput, StringMatchingAlgo stringDistanceMetric) {
    var score = 0.0;

    productName = productName.toLowerCase().withoutDiacriticalMarks;
    userInput = userInput.toLowerCase().withoutDiacriticalMarks;

    if (userInput.contains(' ')) {
      score = stringDistanceMetric.interface.normalizedDistance(userInput, productName);
    } else {
      final wordScores = <double>[];
      for (final productWord in productName.split(' ')) {
        final _wordScore = stringDistanceMetric.interface.normalizedDistance(userInput, productWord);
        wordScores.add(_wordScore);
      }
      score = wordScores.reduce(min);
    }
    return score;
  }

  void _sortMatches(List<SearchMatchDto> products, StringMatchingSorting sortOrder) {
    if (sortOrder == StringMatchingSorting.ScoreWordLengthFrequency) {
      products.sort((a, b) {
        final editDistanceRank = a.editDistance.compareTo(b.editDistance);
        if (editDistanceRank != 0) {
          return editDistanceRank;
        } else {
          final wordLengthRank = a.name.length.compareTo(b.name.length);
          if (wordLengthRank != 0) {
            return wordLengthRank;
          } else {
            final frequencyRank = b.frequency.compareTo(a.frequency);
            return frequencyRank;
          }
        }
      });
    } else {
      products.sort((a, b) {
        final editDistanceRank = a.editDistance.compareTo(b.editDistance);
        if (editDistanceRank != 0) {
          return editDistanceRank;
        } else {
          final frequencyRank = b.frequency.compareTo(a.frequency);
          if (frequencyRank != 0) {
            return frequencyRank;
          } else {
            final wordLengthRank = a.name.length.compareTo(b.name.length);
            return wordLengthRank;
          }
        }
      });
    }
  }

  int _countOccurences(List<String> list, String category) => list.where((e) => e == category).length;

  List<SearchMatchDto> getBestMatches(String prompt, List<SearchMatchDto> matches) {
    var filtered = <SearchMatchDto>[];
    var results = <SearchMatchDto>[];

    // Filter out matches that are too far away from the prompt
    for (final m in matches) {
      double distance = _getEditDistance(m.name, prompt, _configurationNotifier.stringMatchingAlgo);
      if (distance >= 1) continue;
      m.editDistance = distance;
      filtered.add(m);
    }

    // Sort the results
    _sortMatches(filtered, _configurationNotifier.stringMatchingSorting);

    //Take n amount of results, as defined by the configuration
    List<String> _categories = [];
    for (SearchMatchDto match in filtered) {
      if (results.length < _configurationNotifier.maxResults) {
        if (_countOccurences(_categories, match.category) < _configurationNotifier.resultsPerCategory) {
          _categories.add(match.category);
          results.add(match);
        }
      }
    }

    return results;
  }
}
