import 'dart:io' show Platform;

import 'package:device_info_plus/device_info_plus.dart';
import 'package:flutter/services.dart';
import 'package:uuid/uuid.dart';

import '../dtos/device_dto.dart';

class DeviceService {
  static const platform = MethodChannel('com.example.flutter/device_info');

  Future<DeviceDTO?> getDeviceAsync() async {
    late final deviceInfo;
    if (Platform.isAndroid) {
      deviceInfo = await DeviceInfoPlugin().androidInfo;
    } else if (Platform.isIOS) {
      deviceInfo = await DeviceInfoPlugin().iosInfo;
    } else {
      deviceInfo = await DeviceInfoPlugin().webBrowserInfo;
    }

    final deviceDTO = DeviceDTO(
      uuid: Uuid().v4(),
      info: deviceInfo.data.toString(),
    );
    return deviceDTO;
  }
  // Future<int?> getBatteryLevelAsync() async {
  //   try {
  //     if (Platform.isAndroid) {
  //       final batteryLevel = await platform.invokeMethod<int>('getBatteryLevel');

  //       return batteryLevel;
  //     } else if (Platform.isIOS) {
  //       final batteryLevel = await (BatteryInfoPlugin().iosBatteryInfo);
  //       return batteryLevel?.batteryLevel;
  //     }
  //   } catch (error) {
  //     await log('DeviceService::getBatteryLevelAsync', error.toString(), LogType.Error);
  //     return 0;
  //   }
  //   return null;
  // }
}
