import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:uuid/uuid.dart';

import '../database/database.dart';
import 'product_dto.dart';

part 'receipt_dto.g.dart';

@JsonSerializable()
class ReceiptDto {
  String uuid;
  String groupUuid;
  String? previousUuid;
  DateTime? createdOn;
  DateTime? deletedOn;
  DateTime boughtOn;
  String? shopName;
  String? shopCategory;
  bool? isAbroad;
  bool? isOnline;
  double discountAmount;
  double discountPercentage;
  String? imagePath;
  List<ProductDto> products;
  bool? fromThisDevice;
  int? syncOrder;

  @protected
  ReceiptDto({
    required this.uuid,
    required this.groupUuid,
    this.previousUuid,
    this.createdOn,
    this.deletedOn,
    required this.boughtOn,
    this.shopName,
    this.shopCategory,
    this.isAbroad,
    this.isOnline,
    required this.discountAmount,
    required this.discountPercentage,
    this.imagePath,
    required this.products,
    required this.fromThisDevice,
    this.syncOrder,
  });

  factory ReceiptDto.create({DateTime? boughtOn}) {
    final _boughtOn = boughtOn ?? DateTime.now();
    return ReceiptDto(
      uuid: Uuid().v4(),
      groupUuid: Uuid().v4(),
      previousUuid: null,
      shopName: null,
      shopCategory: null,
      boughtOn: _boughtOn,
      createdOn: null,
      deletedOn: null,
      isOnline: false,
      isAbroad: false,
      discountAmount: 0,
      discountPercentage: 0,
      products: [],
      fromThisDevice: true,
      syncOrder: null,
    );
  }

  ReceiptDto copyWith({
    String? uuid,
    String? groupUuid,
    String? previousUuid,
    DateTime? createdOn,
    DateTime? deletedOn,
    DateTime? boughtOn,
    String? shopName,
    String? shopCategory,
    bool? isAbroad,
    bool? isOnline,
    double? discountAmount,
    double? discountPercentage,
    String? imagePath,
    List<ProductDto>? products,
    bool? fromThisDevice,
    int? syncOrder,
  }) {
    return ReceiptDto(
      uuid: uuid ?? this.uuid,
      groupUuid: groupUuid ?? this.groupUuid,
      previousUuid: previousUuid ?? this.previousUuid,
      shopName: shopName ?? this.shopName,
      shopCategory: shopCategory ?? this.shopCategory,
      boughtOn: boughtOn ?? this.boughtOn,
      createdOn: createdOn ?? this.createdOn,
      deletedOn: deletedOn ?? this.deletedOn,
      isOnline: isOnline ?? this.isOnline,
      isAbroad: isAbroad ?? this.isAbroad,
      discountAmount: discountAmount ?? this.discountAmount,
      discountPercentage: discountPercentage ?? this.discountPercentage,
      imagePath: imagePath ?? this.imagePath,
      products: products ?? List<ProductDto>.from(this.products.map((product) => product.copyWith())),
      fromThisDevice: fromThisDevice ?? this.fromThisDevice,
      syncOrder: syncOrder ?? this.syncOrder,
    );
  }

  factory ReceiptDto.fromData(ReceiptData receiptData, List<ProductData> products) {
    return ReceiptDto(
      uuid: receiptData.uuid,
      groupUuid: receiptData.groupUuid,
      previousUuid: receiptData.previousUuid,
      createdOn: receiptData.createdOn,
      deletedOn: receiptData.deletedOn,
      boughtOn: receiptData.boughtOn,
      shopName: receiptData.shopName,
      shopCategory: receiptData.shopCategory,
      isAbroad: receiptData.isAbroad,
      isOnline: receiptData.isOnline,
      discountAmount: receiptData.discountAmount,
      discountPercentage: receiptData.discountPercentage,
      imagePath: receiptData.imagePath,
      products: products.map(ProductDto.fromData).toList(),
      fromThisDevice: receiptData.fromThisDevice,
      syncOrder: receiptData.syncOrder,
    );
  }

  factory ReceiptDto.fromMap(Map<String, dynamic> map) => _$ReceiptDtoFromJson(map);

  factory ReceiptDto.fromJson(Map<String, dynamic> json) => _$ReceiptDtoFromJson(json);


  Map<String, dynamic> toJson() {
    var receiptJson = _$ReceiptDtoToJson(this);
    receiptJson['imageUuid'] = receiptJson['imagePath']?.split('/').last;
    receiptJson.remove('imagePath');
    receiptJson['products'] = receiptJson['products'].map((e) {
      final productJson = e.toJson();
      return productJson;
    }).toList();
    return receiptJson;
  }

  ReceiptData toData(int syncOrder) {
    return ReceiptData(
      uuid: uuid,
      groupUuid: groupUuid,
      previousUuid: previousUuid,
      createdOn: createdOn!,
      deletedOn: deletedOn,
      syncOrder: syncOrder,
      boughtOn: boughtOn,
      shopName: shopName!,
      shopCategory: shopCategory!,
      isAbroad: isAbroad!,
      isOnline: isOnline!,
      discountAmount: discountAmount,
      discountPercentage: discountPercentage,
      fromThisDevice: fromThisDevice ?? false,
      imagePath: imagePath,
    );
  }

  double getTotalPrice() {
    var price = 0.0;
    for (final product in products) {
      price += product.getPrice();
    }
    return (price - discountAmount) / 100 * (100 - discountPercentage);
  }
}
