import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:uuid/uuid.dart';

part 'external_survey_dto.g.dart';

@JsonSerializable()
class ExternalSurveyDto {
  String uuid;
  DateTime dueDate;
  String surveyName;
  String url;
  DateTime? completionDate;

  @protected
  ExternalSurveyDto({
    required this.uuid,
    required this.dueDate,
    required this.surveyName,
    required this.url,
    this.completionDate,
  });

  factory ExternalSurveyDto.create({
    required DateTime dueDate,
    required String surveyName,
    required String url,
  }) {
    return ExternalSurveyDto(
      uuid: Uuid().v4(),
      dueDate: dueDate,
      surveyName: surveyName,
      url: url,
      completionDate: null,
    );
  }

  factory ExternalSurveyDto.fromJson(Map<String, dynamic> json) => _$ExternalSurveyDtoFromJson(json);
}
