const noInternetCode = 404;

class ParsedResponse<R> {
  int statusCode;
  String message;
  R? payload;

  ParsedResponse({required this.statusCode, required this.message, required this.payload});

  bool get isOk => statusCode >= 200 && statusCode < 300;
}
