import 'package:json_annotation/json_annotation.dart';
import 'package:uuid/uuid.dart';

import '../database/database.dart';

part 'product_dto.g.dart';

@JsonSerializable()
class ProductDto {
  String uuid = Uuid().v1();
  String receiptUuid;
  String? name;
  String? coicopName;
  String? coicopCode;
  int count;
  double? price;
  double? discountAmount;
  bool isReturn = false;

  ProductDto({
    required this.uuid,
    required this.receiptUuid,
    required this.name,
    required this.coicopName,
    required this.coicopCode,
    required this.count,
    required this.price,
    required this.discountAmount,
    required this.isReturn,
  });

  factory ProductDto.create(
    String name,
    String? category,
    String? code,
    String receiptUuid,
    int count,
    double? price,
  ) =>
      ProductDto(
        uuid: Uuid().v1(),
        receiptUuid: receiptUuid,
        name: name,
        coicopName: category,
        coicopCode: code,
        count: count,
        price: price,
        discountAmount: null,
        isReturn: false,
      );

  factory ProductDto.fromJson(Map<String, dynamic> json) => _$ProductDtoFromJson(json);

  factory ProductDto.fromData(ProductData data) => ProductDto(
        uuid: data.uuid,
        receiptUuid: data.receiptUuid,
        name: data.name,
        coicopName: data.coicopName,
        coicopCode: data.coicopCode,
        count: data.count,
        price: data.price,
        discountAmount: data.discountAmount,
        isReturn: data.isReturn,
      );

  factory ProductDto.fromMap(Map<String, dynamic> map) => _$ProductDtoFromJson(map);

  Map<String, dynamic> toJson() => _$ProductDtoToJson(this);

  ProductDto copyWith({
    String? uuid,
    String? receiptUuid,
    String? name,
    String? category,
    String? code,
    int? count,
    double? price,
    double? discount,
    bool? isReturn,
  }) {
    return ProductDto(
      uuid: uuid ?? this.uuid,
      receiptUuid: receiptUuid ?? this.receiptUuid,
      name: name ?? this.name,
      coicopName: category ?? this.coicopName,
      coicopCode: code ?? this.coicopCode,
      count: count ?? this.count,
      price: price ?? this.price,
      discountAmount: discount ?? this.discountAmount,
      isReturn: isReturn ?? this.isReturn,
    );
  }

  double getPrice() {
    if (price == null) return 0;
    if (discountAmount == null) return price! * count * (isReturn ? -1 : 1);
    return (price! - discountAmount!) * count * (isReturn ? -1 : 1);
  }

  ProductData toData() => ProductData(
        uuid: uuid,
        name: name!,
        coicopName: coicopName!,
        coicopCode: coicopCode!,
        count: count,
        price: price!,
        discountAmount: discountAmount,
        isReturn: isReturn,
        receiptUuid: receiptUuid,
      );
}
