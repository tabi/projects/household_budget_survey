import 'package:json_annotation/json_annotation.dart';

part 'user_dto.g.dart';

@JsonSerializable()
class UserDTO {
  String username;
  String password;

  UserDTO(this.username, this.password);

  factory UserDTO.fromMap(Map<String, dynamic> map) => _$UserDTOFromJson(map);

  Map<String, dynamic> toMap() => _$UserDTOToJson(this);
}
