import 'dart:core';

import 'package:json_annotation/json_annotation.dart';
import 'package:uuid/uuid.dart';

import '../database/database.dart';

part 'survey_day_dto.g.dart';

@JsonSerializable()
class SurveyDayDto {
  String surveyDayUuid;
  DateTime date;
  DateTime createdOn;
  DateTime updatedOn;
  bool isCompleted;
  String? reason;

  SurveyDayDto({
    required this.surveyDayUuid,
    required this.date,
    required this.isCompleted,
    required this.createdOn,
    required this.updatedOn,
    this.reason,
  });

  factory SurveyDayDto.create({required DateTime date}) {
    return SurveyDayDto(
      surveyDayUuid: Uuid().v4(),
      date: date,
      createdOn: DateTime.now(),
      updatedOn: DateTime.now(),
      isCompleted: false,
      reason: null,
    );
  }

  factory SurveyDayDto.fromSurveyDayData(SurveyDayData surveyData) => SurveyDayDto(
        surveyDayUuid: surveyData.surveyDayUuid,
        date: surveyData.date,
        createdOn: surveyData.createdOn,
        updatedOn: surveyData.updatedOn,
        isCompleted: surveyData.isCompleted,
        reason: surveyData.reason,
      );

  factory SurveyDayDto.fromMap(Map<String, dynamic> map) => _$SurveyDayDtoFromJson(map);

  Map<String, dynamic> toJson() => _$SurveyDayDtoToJson(this);

  factory SurveyDayDto.fromJson(Map<String, dynamic> json) => _$SurveyDayDtoFromJson(json);

  SurveyDayData toData() => SurveyDayData(
        surveyDayUuid: surveyDayUuid,
        date: date,
        createdOn: createdOn,
        updatedOn: updatedOn,
        isCompleted: isCompleted,
        reason: reason,
      );

  SurveyDayDto copyWith({
    String? surveyDayUuid,
    DateTime? date,
    DateTime? createdOn,
    DateTime? updatedOn,
    bool? isCompleted,
    String? reason,
  }) {
    return SurveyDayDto(
      surveyDayUuid: surveyDayUuid ?? this.surveyDayUuid,
      date: date ?? this.date,
      createdOn: createdOn ?? this.createdOn,
      updatedOn: updatedOn ?? this.updatedOn,
      isCompleted: isCompleted ?? this.isCompleted,
      reason: reason ?? this.reason,
    );
  }
}
