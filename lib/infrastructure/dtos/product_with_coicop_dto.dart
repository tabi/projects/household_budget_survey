import '../database/database.dart';

class ProductWithCoicopDto {
  final ProductData productData;
  final CoicopHierarchyData coicopHierarchyData;
  final ReceiptData receiptData;

  ProductWithCoicopDto(this.productData, this.coicopHierarchyData, this.receiptData);
}
