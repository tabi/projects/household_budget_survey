import 'package:json_annotation/json_annotation.dart';

import '../database/database.dart';

part 'error_log_dto.g.dart';

@JsonSerializable()
class ErrorLogDTO {
  @JsonKey(name: 'Message')
  String? message;
  @JsonKey(name: 'Description')
  String? description;
  @JsonKey(name: 'Type')
  String? type;
  @JsonKey(name: 'CreatedOn')
  int? createdOn;
  @JsonKey(name: 'Synced')
  bool? synced;

  ErrorLogDTO(
      {required this.message,
      required this.description,
      required this.type,
      required this.createdOn,
      required this.synced});

  factory ErrorLogDTO.fromMap(Map<String, dynamic> map) => _$ErrorLogDTOFromJson(map);

  factory ErrorLogDTO.fromLog(ErrorLog log) => ErrorLogDTO(
        message: log.message,
        description: log.description,
        type: log.type,
        createdOn: log.createdOn.millisecondsSinceEpoch,
        synced: log.synced,
      );

  Map<String, dynamic> toJson() => _$ErrorLogDTOToJson(this);

  static List<ErrorLogDTO> fromList(List<ErrorLog> list) {
    return list.map((log) => ErrorLogDTO.fromLog(log)).toList();
  }

  static List<Map<String, dynamic>> toList(List<ErrorLogDTO> list) {
    return list.map((log) => log.toJson()).toList();
  }
}
