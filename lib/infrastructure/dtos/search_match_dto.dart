import '../database/database.dart';

class SearchMatchDto {
  final String? code;
  late final int frequency;
  late final String name;
  late final String category;

  late double editDistance;

  SearchMatchDto({
    this.code,
    required this.frequency,
    required this.name,
    required this.category,
  });

  factory SearchMatchDto.fromShop(ShopData shop) =>
      SearchMatchDto(frequency: 0, name: shop.shop, category: shop.shopType);

  factory SearchMatchDto.fromProduct(CoicopProductData product) => SearchMatchDto(
        code: product.coicop,
        frequency: product.frequency,
        name: product.product,
        category: product.category,
      );

  factory SearchMatchDto.fromCoicopHierarchy(CoicopHierarchyData product) => SearchMatchDto(
        code: product.coicop,
        frequency: 0,
        name: product.description,
        category: product.coicop,
      );
}
