import 'package:json_annotation/json_annotation.dart';

import '../database/database.dart';

part 'para_data_log_dto.g.dart';

@JsonSerializable()
class ParaDataLogDTO {
  @JsonKey(name: 'Action')
  String? action;
  @JsonKey(name: 'Description')
  String? description;
  @JsonKey(name: 'Page')
  String? page;
  @JsonKey(name: 'CreatedOn')
  int? createdOn;
  @JsonKey(name: 'Synced')
  bool? synced;

  ParaDataLogDTO(
      {required this.action,
      required this.description,
      required this.page,
      required this.createdOn,
      required this.synced});

  factory ParaDataLogDTO.fromMap(Map<String, dynamic> map) => _$ParaDataLogDTOFromJson(map);

  factory ParaDataLogDTO.fromLog(ParaDataLog log) => ParaDataLogDTO(
      action: log.action,
      description: log.description,
      page: log.page,
      createdOn: log.createdOn.microsecondsSinceEpoch,
      synced: log.synced);

  Map<String, dynamic> toJson() => _$ParaDataLogDTOToJson(this);

  static List<ParaDataLogDTO> fromList(List<ParaDataLog> list) {
    return list.map((log) => ParaDataLogDTO.fromLog(log)).toList();
  }

  static List<Map<String, dynamic>> toList(List<ParaDataLogDTO> list) {
    return list.map((log) => log.toJson()).toList();
  }
}
