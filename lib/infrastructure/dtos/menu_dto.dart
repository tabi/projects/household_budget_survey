import 'package:flutter/material.dart';

class MenuDto {
  final IconData? icon;
  final String text;
  final Widget screen;

  MenuDto({this.icon, required this.text, required this.screen});
}
