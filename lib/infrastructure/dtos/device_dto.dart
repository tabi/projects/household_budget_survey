import 'package:json_annotation/json_annotation.dart';

import '../database/database.dart';

part 'device_dto.g.dart';

@JsonSerializable()
class DeviceDTO {
  String uuid;
  String info;

  DeviceDTO({required this.uuid, required this.info});

  factory DeviceDTO.fromMap(Map<String, dynamic> map) => _$DeviceDTOFromJson(map);

  factory DeviceDTO.fromJson(Map<String, dynamic> json) => _$DeviceDTOFromJson(json);

  factory DeviceDTO.fromDevice(Device device) => DeviceDTO(
        uuid: device.uuid,
        info: device.info,
      );

  Map<String, dynamic> toJson() => _$DeviceDTOToJson(this);

  static List<DeviceDTO> fromList(List<Map<String, String>> list) => list.map(DeviceDTO.fromMap).toList();
}
