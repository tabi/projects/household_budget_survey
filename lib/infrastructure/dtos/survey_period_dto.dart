import 'package:json_annotation/json_annotation.dart';

part 'survey_period_dto.g.dart';

@JsonSerializable()
class SurveyPeriodDTO {
  DateTime startOn;
  DateTime endOn;

  SurveyPeriodDTO(this.startOn, this.endOn);

  factory SurveyPeriodDTO.fromMap(Map<String, dynamic> map) => _$SurveyPeriodDTOFromJson(map);

  Map<String, dynamic> toMap() => _$SurveyPeriodDTOToJson(this);
}
