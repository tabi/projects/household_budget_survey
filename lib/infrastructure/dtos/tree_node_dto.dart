class TreeNodeDto {
  String coicopName;
  double totalCost;
  Map<DateTime, List<double>> expensesByDate;
  TreeNodeDto? parentNode;
  Map<int, TreeNodeDto> childNodes;

  TreeNodeDto({
    required this.coicopName,
    required this.totalCost,
    required this.expensesByDate,
    this.parentNode,
    required this.childNodes,
  });

  String get getCoicopName => coicopName;

  Map<int, TreeNodeDto> get getChildNodes => childNodes;

  double get getTotalCost => totalCost;

  double getFilteredCost({DateTime? minDate, DateTime? maxDate, double? minPrice, double? maxPrice}) {
    double totalCost = 0;
    expensesByDate.forEach((date, expenses) {
      if (minDate != null && date.isBefore(minDate)) return;
      if (maxDate != null && date.isAfter(maxDate)) return;
      expenses.forEach((element) {
        if (minPrice != null && element < minPrice) return;
        if (maxPrice != null && element > maxPrice) return;
        totalCost += element;
      });
    });
    return totalCost;
  }
}
