import 'package:shared_preferences/shared_preferences.dart';

import '../database/database.dart';
import '../dtos/device_dto.dart';
import '../dtos/login_payload.dart';
import '../dtos/parsed_response.dart';
import '../dtos/token_dto.dart';
import 'base_api.dart';

class AuthApi extends BaseApi {
  AuthApi(Database database) : super('Auth', database);

  Future<ParsedResponse<TokenDTO?>> login(String username, String password, DeviceDTO deviceDTO) async {
    final map = LoginPayload(
      username: username,
      password: password,
      deviceUuid: deviceDTO.uuid,
      deviceInfo: deviceDTO.info,
    ).toMap();

    final parsedResponse = await this.getParsedResponse<TokenDTO, TokenDTO>('/login', TokenDTO.fromMap, payload: map);
    if (parsedResponse.payload == null) {
      return parsedResponse;
    }

    final token = Token(authToken: parsedResponse.payload!.token);

    await this.database.tokensDao.clean();
    await this.database.tokensDao.insertTokenAsync(token);
    (await SharedPreferences.getInstance()).setString('username', username);
    (await SharedPreferences.getInstance()).setInt('login_unix_timestamp', DateTime.now().millisecondsSinceEpoch);
    return parsedResponse;
  }
}
