import '../database/database.dart';
import '../dtos/parsed_response.dart';
import '../dtos/product_dto.dart';
import 'base_api.dart';

class ProductApi extends BaseApi {
  ProductApi(Database database) : super('product', database);

  Future<ParsedResponse<ProductDto?>> insertDevice(ProductDto productDto) async =>
      this.getParsedResponse<ProductDto, ProductDto>('', ProductDto.fromMap, payload: productDto.toJson());

  Future<ParsedResponse<ProductDto?>> sync(ProductDto productDto) async =>
      this.getParsedResponse<ProductDto, ProductDto>(
        'upsert',
        ProductDto.fromJson,
        payload: productDto,
      );

  Future<ParsedResponse<List<ProductDto?>>> syncProducts(List<ProductDto> products) async {
    final payload = products.map((product) => product.toJson()).toList();
    return this.getParsedResponse<List<ProductDto>, ProductDto>(
      'bulkUpsert',
      ProductDto.fromJson,
      payload: payload,
    );
  }
}
