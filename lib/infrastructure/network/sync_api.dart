import '../database/database.dart';
import 'base_api.dart';

class SyncApi extends BaseApi {
  SyncApi(Database database) : super('Sync', database);

  Future<int> getDeviceSyncOrderFromBackend(String syncType) async => await this.simplePost('/deviceSyncOrder', {"SyncType": syncType});

  Future<void> setDeviceSyncOrderToBackend(String syncType, int syncOrder) async =>
      await this.simplePost('/deviceSyncOrder', {"SyncType": syncType, "SyncOrder": syncOrder});

  Future<DateTime> getServerTime() async {
    final dateString = await this.simpleGet('/serverTime');
    return DateTime.parse(dateString);
  }
}
