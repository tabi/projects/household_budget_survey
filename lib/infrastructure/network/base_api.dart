import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:http/io_client.dart';

import '../database/database.dart';
import '../dtos/parsed_response.dart';

import 'package:device_info_plus/device_info_plus.dart';

var serverSocketAddress = 'https://10.0.2.2:7088'; // Emulator
// var serverSocketAddress = 'https://192.168.1.7:7088'; // Tom local
var bearerToken = 'Authorization';

class BaseApi {
  final String _path;
  final Database database;
  final HttpClient _httpClient;

  BaseApi(this._path, this.database) : _httpClient = _createHttpClient() {
    // setupConnectionString();
  }

  // TODO: used for testing on physical device and emulator + localhost
  void setupConnectionString() async {
    final deviceInfo = DeviceInfoPlugin();
    var androidInfo = await deviceInfo.androidInfo;
    if (androidInfo.isPhysicalDevice) {
      serverSocketAddress = 'https://192.168.1.7:7088';
    } else {
      serverSocketAddress = 'https://10.0.2.2:7088';
    }
  }

  static HttpClient _createHttpClient() {
    final HttpClient client = HttpClient()..badCertificateCallback = (X509Certificate cert, String host, int port) => true;
    return client;
  }

  Future<ParsedResponse<R>> getParsedResponse<R, T>(String route, T Function(Map<String, dynamic>) responseParser, {dynamic payload}) async {
    final response = await _getResponse<R, T>(route, responseParser, body: payload);
    return response;
  }

  Future simplePost(String route, dynamic payload) async {
    final authHeader = await _getAutHeader();
    final uri = Uri.parse('$serverSocketAddress/api/${this._path}$route');
    final ioClient = IOClient(_httpClient);
    final response =
        await ioClient.post(uri, body: jsonEncode(payload), headers: {'Content-Type': 'application/json', HttpHeaders.authorizationHeader: authHeader});
    print(response.statusCode);
    print(response.body);
    return json.decode(response.body);
  }

    Future simpleGet(String route) async {
    final authHeader = await _getAutHeader();
    final uri = Uri.parse('$serverSocketAddress/api/${this._path}$route');
    final ioClient = IOClient(_httpClient);
    final response =
        await ioClient.get(uri, headers: {'Content-Type': 'application/json', HttpHeaders.authorizationHeader: authHeader});
    print(response.statusCode);
    print(response.body);
    return json.decode(response.body);
  }

  Future<ParsedResponse<R>> _getResponse<R, T>(String route, T Function(Map<String, dynamic>) responseParser, {dynamic body}) async {
    final authHeader = await _getAutHeader();
    final uri = Uri.parse('$serverSocketAddress/api/${this._path}$route');

    late http.Response response;
    final ioClient = IOClient(_httpClient);

    try {
      if (_isHttpGet(body)) {
        response = await ioClient.get(uri, headers: {'Content-Type': 'application/json', HttpHeaders.authorizationHeader: authHeader});
      } else {
        var test = jsonEncode(body);
        print(test);
        response = await ioClient.post(uri, body: jsonEncode(body), headers: {'Content-Type': 'application/json'});
      }

      print(response.body);

      if (_isOk(response)) {
        final deserializedResponse = json.decode(response.body) as Map<String, dynamic>;
        final dynamic serializedPayload = deserializedResponse;

        dynamic payload;

        if (serializedPayload is Map) {
          payload = responseParser(serializedPayload as Map<String, dynamic>);
        }

        if (serializedPayload is Iterable) {
          if (R is Map) {
            throw Exception('Wrong casting in parsedResponse function');
          }

          final payloadList = <T>[];
          for (final serializedPayloadItem in serializedPayload) {
            payloadList.add(responseParser(serializedPayloadItem as Map<String, dynamic>));
          }
          payload = payloadList;
        }

        final parsedResponse = ParsedResponse<R>(
            statusCode: response.statusCode,
            message: deserializedResponse['Message'] == null ? '' : deserializedResponse['Message'] as String,
            payload: payload as R);

        return parsedResponse;
      }

      if (response.statusCode == 401) {
        final deserializedResponse = json.decode(response.body) as Map<String, dynamic>;
        final deserializedBody = json.decode(deserializedResponse['Body'].toString()) as Map<String, dynamic>;

        final parsedResponse = ParsedResponse<R>(
            statusCode: response.statusCode, message: deserializedBody['Message'] == null ? '' : deserializedBody['Message'] as String, payload: null);
        return parsedResponse;
      }
    } on Exception catch (e) {
      print(e);
    }

    return ParsedResponse(statusCode: 500, message: '', payload: null);
  }

  Future<String> _getAutHeader() async {
    final token = await database.tokensDao.getTokenAsync();
    if (_bearerTokenExists(token)) {
      bearerToken = 'Bearer ${token!.authToken}';
      return 'Bearer ${token.authToken}';
    } else {
      return 'Bearer ';
    }
  }

  bool _bearerTokenExists(Token? token) => token?.authToken != null;

  bool _isHttpGet(dynamic body) => body == null;

  bool _isOk(http.Response response) {
    if (response.statusCode < 200 || response.statusCode >= 300) return false;
    return true;
  }
}
