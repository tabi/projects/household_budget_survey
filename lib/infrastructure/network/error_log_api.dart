import '../database/database.dart';
import '../dtos/error_log_dto.dart';
import '../dtos/parsed_response.dart';
import 'base_api.dart';

class ErrorLogApi extends BaseApi {
  ErrorLogApi(Database database) : super('errorlogs', database);

  Future<ParsedResponse<ErrorLogDTO?>> postLog(ErrorLogDTO logDTO) async =>
      this.getParsedResponse<ErrorLogDTO, ErrorLogDTO>('', ErrorLogDTO.fromMap);

  Future<ParsedResponse<List<ErrorLogDTO>?>> postLogs(List<ErrorLogDTO> logs) async => this
      .getParsedResponse<List<ErrorLogDTO>, ErrorLogDTO>('', ErrorLogDTO.fromMap, payload: ErrorLogDTO.toList(logs));
}
