import '../database/database.dart';
import '../dtos/para_data_log_dto.dart';
import '../dtos/parsed_response.dart';
import 'base_api.dart';

class ParaDataLogApi extends BaseApi {
  ParaDataLogApi(Database database) : super('paradata', database);

  Future<ParsedResponse<ParaDataLogDTO?>> postLog(ParaDataLogDTO logDTO) async =>
      this.getParsedResponse<ParaDataLogDTO, ParaDataLogDTO>('', ParaDataLogDTO.fromMap);

  Future<ParsedResponse<List<ParaDataLogDTO>?>> postLogs(List<ParaDataLogDTO> logs) async =>
      this.getParsedResponse<List<ParaDataLogDTO>, ParaDataLogDTO>('', ParaDataLogDTO.fromMap,
          payload: ParaDataLogDTO.toList(logs));
}
