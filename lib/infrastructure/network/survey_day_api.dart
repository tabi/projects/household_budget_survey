import '../database/database.dart';
import '../dtos/parsed_response.dart';
import '../dtos/survey_day_dto.dart';
import 'base_api.dart';

class SurveyDayApi extends BaseApi {
  SurveyDayApi(Database database) : super('surveyday', database);

  Future<ParsedResponse<List<SurveyDayDto>?>> getSurveyDays() {
    return this.getParsedResponse<List<SurveyDayDto>, SurveyDayDto>('', SurveyDayDto.fromMap);
  }

  Future<ParsedResponse<SurveyDayDto?>> insertSurveyDay(SurveyDayDto surveyDayDTO) async => this
      .getParsedResponse<SurveyDayDto, SurveyDayDto>('addday', SurveyDayDto.fromMap, payload: surveyDayDTO.toJson());

  Future<ParsedResponse<SurveyDayDto?>> updateSurveyDay(SurveyDayDto surveyDayDTO) async =>
      this.getParsedResponse<SurveyDayDto, SurveyDayDto>(
        'updateday',
        SurveyDayDto.fromJson,
        payload: surveyDayDTO,
      );
}
