import '../database/database.dart';
import '../dtos/receipt_dto.dart';
import 'base_api.dart';

class ReceiptApi extends BaseApi {
  ReceiptApi(Database database) : super('Receipt', database);

  Future<void> upsertReceipt(ReceiptDto receiptDto) async {
    var receiptJson = receiptDto.toJson();
    await this.simplePost('/upsertReceipt', receiptJson);
  }

  Future<List<ReceiptDto>> getReceipts(int backendSyncOrder) async {
    var response = await this.simplePost('/getReceipts', {'backendSyncOrder': backendSyncOrder});
    final receipts = (response as List).map((e) => ReceiptDto.fromJson(e)).toList();
    return receipts;
  }
}
