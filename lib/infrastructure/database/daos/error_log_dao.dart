import 'package:drift/drift.dart';

import '../database.dart';
import '../tables/error_log_table.dart';

part 'error_log_dao.g.dart';

@DriftAccessor(tables: [ErrorLogs])
class ErrorLogsDao extends DatabaseAccessor<Database> with _$ErrorLogsDaoMixin {
  ErrorLogsDao(Database db) : super(db);

  Future<List<ErrorLog>> get() => select(errorLogs).get();

  Future<List<ErrorLog>> getUnSyncedLogs(limit) => (select(errorLogs)
        ..where((l) => l.synced.equals(false))
        ..limit(limit))
      .get();

  Stream<List<ErrorLog>> watchAllLogs() => select(errorLogs).watch();
  Future insertLog(ErrorLog log) => into(errorLogs).insert(log);
  Future updateLog(ErrorLog log) => update(errorLogs).replace(log);
  Future deleteLog(ErrorLog log) => delete(errorLogs).delete(log);

  Future<void> replaceBulkLog(Iterable<ErrorLog> logList) async {
    await batch((batch) {
      batch.replaceAll(errorLogs, logList);
    });
  }

  Future<int> getSyncedCount() async => (await (select(errorLogs)..where((l) => l.synced.equals(true))).get()).length;

  Future<int> getUnsyncedCount() async =>
      (await (select(errorLogs)..where((l) => l.synced.equals(false))).get()).length;
}
