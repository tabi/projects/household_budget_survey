import 'package:drift/drift.dart';

import '../../dtos/product_with_coicop_dto.dart';
import '../database.dart';
import '../tables/coicop_hierarchy_table.dart';
import '../tables/product_table.dart';
import '../tables/receipt_table.dart';

part 'product_dao.g.dart';

@DriftAccessor(tables: [Product, CoicopHierarchy, Receipt])
class ProductDao extends DatabaseAccessor<Database> with _$ProductDaoMixin {
  ProductDao(Database db) : super(db);

  Future<void> insertProduct(List<ProductData> productData) async {
    await batch((batch) {
      batch.insertAll(product, productData);
    });
  }

  Stream<List<ProductWithCoicopDto>> watchProductWithCoicopDtos() {
    final query = select(product).join([
      leftOuterJoin(coicopHierarchy, coicopHierarchy.coicop.equalsExp(product.coicopCode)),
      leftOuterJoin(receipt, receipt.uuid.equalsExp(product.receiptUuid)),
    ])
      ..where(receipt.deletedOn.isNull());

    return query
        .watch()
        .map((rows) => rows.map((row) => ProductWithCoicopDto(row.readTable(product), row.readTable(coicopHierarchy), row.readTable(receipt))).toList());
  }
}
