import 'package:drift/drift.dart';

import '../database.dart';
import '../tables/coicop_product_table.dart';

part 'coicop_product_dao.g.dart';

@DriftAccessor(tables: [CoicopProduct])
class CoicopProductDao extends DatabaseAccessor<Database> with _$CoicopProductDaoMixin {
  CoicopProductDao(Database db) : super(db);

  Future<List<CoicopProductData>> getProducts() => select(coicopProduct).get();
}
