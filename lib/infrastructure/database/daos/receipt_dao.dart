import 'package:drift/drift.dart';

import '../../dtos/receipt_dto.dart';
import '../database.dart';
import '../tables/product_table.dart';
import '../tables/receipt_table.dart';

part 'receipt_dao.g.dart';

@DriftAccessor(tables: [Receipt, Product])
class ReceiptDao extends DatabaseAccessor<Database> with _$ReceiptDaoMixin {
  ReceiptDao(Database db) : super(db);

  Future<void> insertReceipt(ReceiptData receiptData) async => into(receipt).insert(receiptData);

  Stream<List<ReceiptDto>> streamReceipts() {
    final query = select(receipt).join([
      leftOuterJoin(product, product.receiptUuid.equalsExp(receipt.uuid)),
    ])
      ..orderBy([
        OrderingTerm(expression: receipt.groupUuid),
        OrderingTerm(expression: receipt.syncOrder, mode: OrderingMode.desc),
      ]);

    return query.watch().map((rows) {
      Map<String, TypedResult> latestReceipts = {};
      Map<String, List<ProductData>> productsByReceipt = {};

      for (TypedResult row in rows) {
        ReceiptData receiptData = row.readTable(receipt);
        ProductData? productData = row.readTableOrNull(product);

        if (productData != null) {
          final list = productsByReceipt.putIfAbsent(receiptData.uuid, () => []);
          list.add(productData);
        }
        if (!latestReceipts.containsKey(receiptData.groupUuid) ||
            (latestReceipts[receiptData.groupUuid]?.readTable(receipt).syncOrder ?? -1) < receiptData.syncOrder) {
          latestReceipts[receiptData.groupUuid] = row;
        }
      }

      List<ReceiptDto> result = [];
      for (var receiptResult in latestReceipts.values.where((r) => r.readTable(receipt).deletedOn == null)) {
        ReceiptData receiptData = receiptResult.readTable(receipt);
        List<ProductData> productDataList = productsByReceipt[receiptData.uuid] ?? [];
        result.add(ReceiptDto.fromData(receiptData, productDataList));
      }
      return result;
    });
  }

  Future<List<ReceiptDto>> getUnsyncedReceipts(int syncOrder) async {
    final query = select(receipt).join([
      leftOuterJoin(product, product.receiptUuid.equalsExp(receipt.uuid)),
    ])
      ..where(receipt.syncOrder.isBiggerThanValue(syncOrder))
      ..where(receipt.fromThisDevice.equals(true));

    final rows = await query.get();

    Map<String, List<ProductData>> productsByReceipt = {};

    for (TypedResult row in rows) {
      ReceiptData receiptData = row.readTable(receipt);
      ProductData? productData = row.readTableOrNull(product);

      if (productData != null) {
        final list = productsByReceipt.putIfAbsent(receiptData.uuid, () => []);
        list.add(productData);
      }
    }

    List<ReceiptDto> result = [];
    for (var row in rows) {
      ReceiptData receiptData = row.readTable(receipt);
      List<ProductData> productDataList = productsByReceipt[receiptData.uuid] ?? [];
      result.add(ReceiptDto.fromData(receiptData, productDataList));
    }
    return result;
  }

  Future<ReceiptDto?> getReceiptByGroupUuid(String groupUuid) async {
    final query = select(receipt).join([
      leftOuterJoin(product, product.receiptUuid.equalsExp(receipt.uuid)),
    ])
      ..where(receipt.groupUuid.equals(groupUuid))
      ..orderBy([
        OrderingTerm(expression: receipt.syncOrder, mode: OrderingMode.desc),
      ]);

    final rows = await query.get();

    Map<String, List<ProductData>> productsByReceipt = {};

    for (TypedResult row in rows) {
      ReceiptData receiptData = row.readTable(receipt);
      ProductData? productData = row.readTableOrNull(product);

      if (productData != null) {
        final list = productsByReceipt.putIfAbsent(receiptData.uuid, () => []);
        list.add(productData);
      }
    }

    for (var row in rows) {
      ReceiptData receiptData = row.readTable(receipt);
      if (receiptData.deletedOn == null) {
        List<ProductData> productDataList = productsByReceipt[receiptData.uuid] ?? [];
        return ReceiptDto.fromData(receiptData, productDataList);
      }
    }
    return null;
  }
}
