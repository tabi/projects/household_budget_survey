import 'package:drift/drift.dart';

import '../database.dart';
import '../tables/shop_table.dart';

part 'shop_dao.g.dart';

@DriftAccessor(tables: [Shop])
class ShopDao extends DatabaseAccessor<Database> with _$ShopDaoMixin {
  ShopDao(Database db) : super(db);

  Future<List<ShopData>> getShops() => select(shop).get();
}
