import 'package:drift/drift.dart';

import '../database.dart';
import '../tables/survey_day_table.dart';

part 'survey_day_dao.g.dart';

@DriftAccessor(tables: [SurveyDay])
class SurveyDayDao extends DatabaseAccessor<Database> with _$SurveyDayDaoMixin {
  SurveyDayDao(Database db) : super(db);

  Future<List<SurveyDayData>> getSurveyDays() => select(surveyDay).get();

  Future<void> insertSurveyDay(SurveyDayData surveyDayData) async => into(surveyDay).insert(surveyDayData);

  Future<void> updateSurveyDay(SurveyDayData surveyDayData) async => update(surveyDay).replace(surveyDayData);
}
