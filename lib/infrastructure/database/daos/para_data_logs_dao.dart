import 'package:drift/drift.dart';

import '../database.dart';
import '../tables/para_data_table.dart';

part 'para_data_logs_dao.g.dart';

@DriftAccessor(tables: [ParaDataLogs])
class ParaDataLogsDao extends DatabaseAccessor<Database> with _$ParaDataLogsDaoMixin {
  ParaDataLogsDao(Database db) : super(db);

  Future<List<ParaDataLog>> get() => select(paraDataLogs).get();

  Future<List<ParaDataLog>> getUnSyncedLogs(limit) => (select(paraDataLogs)
        ..where((l) => l.synced.equals(false))
        ..limit(limit))
      .get();

  Stream<List<ParaDataLog>> watchAllLogs() => select(paraDataLogs).watch();
  Future insertLog(ParaDataLog log) => into(paraDataLogs).insert(log);
  Future updateLog(ParaDataLog log) => update(paraDataLogs).replace(log);
  Future deleteLog(ParaDataLog log) => delete(paraDataLogs).delete(log);

  Future<void> replaceBulkLog(Iterable<ParaDataLog> logList) async {
    await batch((batch) {
      batch.replaceAll(paraDataLogs, logList);
    });
  }

  Future<int> getSyncedCount() async =>
      (await (select(paraDataLogs)..where((l) => l.synced.equals(true))).get()).length;

  Future<int> getUnsyncedCount() async =>
      (await (select(paraDataLogs)..where((l) => l.synced.equals(false))).get()).length;
}
