import 'package:drift/drift.dart';

import '../database.dart';
import '../tables/device_table.dart';

part 'device_dao.g.dart';

@DriftAccessor(tables: [Devices])
class DevicesDao extends DatabaseAccessor<Database> with _$DevicesDaoMixin {
  DevicesDao(Database db) : super(db);

  Future<Device?> getDeviceAsync() => (select(devices)..limit(1)).getSingleOrNull();
  Future<List<Device>> getDevicesAsync() => select(devices).get();
  Stream<List<Device>> watchAllDevicesAsync() => select(devices).watch();
  Future insertDeviceAsync(Device device) => into(devices).insert(device);
  Future updateDeviceAsync(Device device) => update(devices).replace(device);
  Future deleteDeviceAsync(Device device) => delete(devices).delete(device);
}
