import 'package:drift/drift.dart';

import '../database.dart';
import '../tables/coicop_hierarchy_table.dart';

part 'coicop_hierachy_dao.g.dart';

@DriftAccessor(tables: [CoicopHierarchy])
class CoicopHierarchyDao extends DatabaseAccessor<Database> with _$CoicopHierarchyDaoMixin {
  CoicopHierarchyDao(Database db) : super(db);

  Future<List<CoicopHierarchyData>> getCoicopHierarchy() => select(coicopHierarchy).get();
}
