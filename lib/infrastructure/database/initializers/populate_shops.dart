import 'package:drift/drift.dart';

import '../../utils/csv_util.dart';
import '../database.dart';

Future<void> populateShops(Database db) async {
  // TODO: make whatever file is used configurable
  final filePath = 'assets/shops/nl_NL-shops.csv';
  final rows = await getCsvRows(filePath);

  final shops = <ShopCompanion>[];
  for (int i = 1; i < rows.length; i++) {
    final row = rows[i] as List<dynamic>;
    final shop = ShopCompanion(
      shop: Value(row[0] as String),
      shopType: Value(row[1] as String),
    );
    shops.add(shop);
  }

  await db.batch((batch) {
    batch.insertAll(db.shop, shops);
  });
}
