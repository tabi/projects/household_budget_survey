import 'package:drift/drift.dart';

import '../../utils/csv_util.dart';
import '../../utils/extensions.dart';
import '../database.dart';

Future<void> populateCoicopProducts(Database db) async {
  // TODO: make whatever file is used configurable
  final filePath = 'assets/coicop_products/nl_NL-coicop_products.csv';
  final rows = await getCsvRows(filePath);

  final columnIndex = <String, int>{};
  for (int i = 0; i < (rows[0] as List<dynamic>).length; i++) columnIndex[rows[0][i] as String] = i;

  final coicopProducts = <CoicopProductCompanion>[];
  for (int i = 1; i < rows.length; i++) {
    final row = rows[i] as List<dynamic>;
    final coicopSearch = CoicopProductCompanion(
      coicop: Value(normalizeCoicop(row[columnIndex['code']!].toString())),
      product: Value(row[columnIndex['product']!] as String),
      frequency: Value(row[columnIndex['frequency']!] as int),
      category: Value(row[columnIndex['category']!] as String),
    );
    coicopProducts.add(coicopSearch);
  }

  await db.batch((batch) {
    batch.insertAll(db.coicopProduct, coicopProducts);
  });
}
