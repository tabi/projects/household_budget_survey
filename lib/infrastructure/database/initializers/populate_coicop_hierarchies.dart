import 'package:drift/drift.dart';

import '../../utils/csv_util.dart';
import '../../utils/extensions.dart';
import '../database.dart';


Future<void> populateCoicopHierarchies(Database db) async {
  // TODO: make whatever file is used configurable
  final filePath = 'assets/coicop_hierarchy/nl_NL-coicop_hierarchy.csv';
  final rows = await getCsvRows(filePath);

  final coicopHierarchies = <CoicopHierarchyCompanion>[];
  for (int i = 1; i < rows.length; i++) {
    final row = rows[i] as List<dynamic>;
    final coicopHierarchy = CoicopHierarchyCompanion(
      coicop: Value(normalizeCoicop(row[0].toString())),
      description: Value(row[1] as String),
    );

    coicopHierarchies.add(coicopHierarchy);
  }

  await db.batch((batch) {
    batch.insertAll(db.coicopHierarchy, coicopHierarchies);
  });
}
