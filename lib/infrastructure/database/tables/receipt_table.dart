import 'package:drift/drift.dart';
import 'package:uuid/uuid.dart';

@TableIndex(name: 'idx_syncOrder', columns: {#syncOrder})
class Receipt extends Table {
  TextColumn get uuid => text().clientDefault(() => Uuid().v4())();
  TextColumn get groupUuid => text()();
  TextColumn get previousUuid => text().nullable()();
  DateTimeColumn get createdOn => dateTime()();
  DateTimeColumn get deletedOn => dateTime().nullable()();
  BoolColumn get fromThisDevice => boolean()();
  IntColumn get syncOrder => integer()();

  DateTimeColumn get boughtOn => dateTime()();
  TextColumn get shopName => text()();
  TextColumn get shopCategory => text()();
  BoolColumn get isAbroad => boolean()();
  BoolColumn get isOnline => boolean()();
  RealColumn get discountAmount => real()();
  RealColumn get discountPercentage => real()();
  TextColumn get imagePath => text().nullable()();

  @override
  Set<Column> get primaryKey => {uuid};
}
