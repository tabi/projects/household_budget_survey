import 'package:drift/drift.dart';

class Devices extends Table {
  TextColumn get uuid => text()();
  TextColumn get info => text()();
}
