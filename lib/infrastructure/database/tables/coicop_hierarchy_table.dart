import 'package:drift/drift.dart';

class CoicopHierarchy extends Table {
  IntColumn get id => integer().autoIncrement()();
  TextColumn get coicop => text()();
  TextColumn get description => text()();
}
