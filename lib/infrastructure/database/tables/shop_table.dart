import 'package:drift/drift.dart';

class Shop extends Table {
  IntColumn get id => integer().autoIncrement()();
  TextColumn get shop => text()();
  TextColumn get shopType => text()();
}
