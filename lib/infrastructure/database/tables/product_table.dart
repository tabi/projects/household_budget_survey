import 'package:drift/drift.dart';
import 'package:uuid/uuid.dart';

import 'receipt_table.dart';

class Product extends Table {
  TextColumn get uuid => text().clientDefault(() => Uuid().v4())();
  TextColumn get receiptUuid => text().references(Receipt, #uuid)();
  TextColumn get name => text()();
  RealColumn get price => real()();
  IntColumn get count => integer()();
  TextColumn get coicopCode => text()();
  TextColumn get coicopName => text()();
  RealColumn? get discountAmount => real().nullable()();
  BoolColumn get isReturn => boolean()();

  @override
  Set<Column> get primaryKey => {uuid};
}
