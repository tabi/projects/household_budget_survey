import 'package:drift/drift.dart';

class Shop extends Table {
  IntColumn get id => integer().autoIncrement()();
  DateTimeColumn get startOn => dateTime()();
  DateTimeColumn get endOn => dateTime()();
}
