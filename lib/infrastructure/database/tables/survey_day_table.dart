import 'package:drift/drift.dart';
import 'package:uuid/uuid.dart';

class SurveyDay extends Table {
  TextColumn get surveyDayUuid => text().clientDefault(() => Uuid().v4())();
  DateTimeColumn get date => dateTime()();
  BoolColumn get isCompleted => boolean().withDefault(const Constant(false))();
  TextColumn? get reason => text().nullable()();
  DateTimeColumn get createdOn => dateTime()();
  DateTimeColumn get updatedOn => dateTime()();

  @override
  Set<Column> get primaryKey => {surveyDayUuid};
}
