import 'package:drift/drift.dart';

class CoicopProduct extends Table {
  IntColumn get id => integer().autoIncrement()();
  TextColumn get coicop => text()();
  TextColumn get product => text().withLength(min: 1, max: 100)();
  TextColumn get category => text()();
  IntColumn get frequency => integer()();
}
