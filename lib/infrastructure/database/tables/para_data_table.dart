import 'package:drift/drift.dart';
import 'package:uuid/uuid.dart';

class ParaDataLogs extends Table {
  TextColumn get uuid => text().clientDefault(() => Uuid().v4())();
  TextColumn get action => text()();
  TextColumn get description => text()();
  TextColumn get page => text()();
  DateTimeColumn get createdOn => dateTime()();
  BoolColumn get synced => boolean().withDefault(const Constant(false))();

  @override
  Set<Column> get primaryKey => {uuid};
}
