import 'package:drift/drift.dart';
import 'package:uuid/uuid.dart';

class ErrorLogs extends Table {
  TextColumn get uuid => text().clientDefault(() => Uuid().v4())();
  TextColumn get message => text()();
  TextColumn get description => text()();
  TextColumn get type => text()();
  DateTimeColumn get createdOn => dateTime()();
  BoolColumn get synced => boolean().withDefault(const Constant(false))();

  @override
  Set<Column> get primaryKey => {uuid};
}
