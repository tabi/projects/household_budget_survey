import 'dart:io';

import 'package:drift/drift.dart';
import 'package:drift/native.dart';
import 'package:path/path.dart' as p;
import 'package:path_provider/path_provider.dart';
import 'package:uuid/uuid.dart';

import 'daos/coicop_hierachy_dao.dart';
import 'daos/coicop_product_dao.dart';
import 'daos/device_dao.dart';
import 'daos/error_log_dao.dart';
import 'daos/para_data_logs_dao.dart';
import 'daos/product_dao.dart';
import 'daos/receipt_dao.dart';
import 'daos/shop_dao.dart';
import 'daos/survey_day_dao.dart';
import 'daos/token_dao.dart';
import 'initializers/populate_coicop_hierarchies.dart';
import 'initializers/populate_coicop_products.dart';
import 'initializers/populate_shops.dart';
import 'tables/coicop_hierarchy_table.dart';
import 'tables/coicop_product_table.dart';
import 'tables/device_table.dart';
import 'tables/error_log_table.dart';
import 'tables/para_data_table.dart';
import 'tables/product_table.dart';
import 'tables/receipt_table.dart';
import 'tables/shop_table.dart';
import 'tables/survey_day_table.dart';
import 'tables/token_table.dart';

part 'database.g.dart';

@DriftDatabase(
  tables: [
    CoicopHierarchy,
    CoicopProduct,
    ErrorLogs,
    ParaDataLogs,
    Receipt,
    Shop,
    Tokens,
    Devices,
    Product,
    SurveyDay,
  ],
  daos: [
    CoicopHierarchyDao,
    CoicopProductDao,
    ErrorLogsDao,
    ParaDataLogsDao,
    ShopDao,
    DevicesDao,
    TokensDao,
    ProductDao,
    ReceiptDao,
    SurveyDayDao,
  ],
)
class Database extends _$Database {
  Database() : super(_openConnection());

  @override
  int get schemaVersion => 1;

  static LazyDatabase _openConnection() {
    return LazyDatabase(
      () async {
        final dbFolder = await getApplicationDocumentsDirectory();
        final file = File(p.join(dbFolder.path, 'db.sqlite'));
        return NativeDatabase.createInBackground(file);
        // return NativeDatabase.createInBackground(file, logStatements: true);
      },
    );
  }

  @override
  MigrationStrategy get migration => MigrationStrategy(
        onCreate: (Migrator m) async {
          await m.createAll();
          await populateCoicopHierarchies(this);
          await populateCoicopProducts(this);
          await populateShops(this);
        },
      );
}
