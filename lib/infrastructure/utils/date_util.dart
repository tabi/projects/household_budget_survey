import 'package:flutter/widgets.dart';
import 'extensions.dart';
import 'package:intl/intl.dart';

import '../notifiers/translation_notifier.dart';

/// Signature for a function that creates a widget for a given `day`.
typedef DayBuilder = Widget? Function(BuildContext context, DateTime day);

/// Signature for a function that creates a widget for a given `day`.
/// Additionally, contains the currently focused day.
typedef FocusedDayBuilder = Widget? Function(BuildContext context, DateTime day, DateTime focusedDay);

/// Signature for a function returning text that can be localized and formatted with `DateFormat`.
typedef TextFormatter = String Function(DateTime date, dynamic locale);

/// Gestures available for the calendar.
enum AvailableGestures { none, verticalSwipe, horizontalSwipe, all }

/// Formats that the calendar can display.
enum CalendarFormat { month, week }

/// Days of the week that the calendar can start with.
enum StartingDayOfWeek {
  monday,
  tuesday,
  wednesday,
  thursday,
  friday,
  saturday,
  sunday,
}

/// Returns a numerical value associated with given `weekday`.
///
/// Returns 1 for `StartingDayOfWeek.monday`, all the way to 7 for `StartingDayOfWeek.sunday`.
int getWeekdayNumber(StartingDayOfWeek weekday) {
  return StartingDayOfWeek.values.indexOf(weekday) + 1;
}

/// Returns `date` in UTC format, without its time part.
DateTime normalizeDate(DateTime date) {
  return DateTime.utc(date.year, date.month, date.day);
}

DateTime toYearMonthDay(DateTime date) => DateTime(date.year, date.month, date.day);

/// Checks if two DateTime objects are the same day.
/// Returns `false` if either of them is null.
bool isSameDay(DateTime? a, DateTime? b) {
  if (a == null || b == null) {
    return false;
  }

  return a.year == b.year && a.month == b.month && a.day == b.day;
}

String getDateString(DateTime date, TranslationNotifier translationNotifier) {
  final receiptDate = DateFormat('yyyy-MM-dd').format(date);
  final today = DateFormat('yyyy-MM-dd').format(DateTime.now());
  final yesterday = DateFormat('yyyy-MM-dd').format(DateTime.now().subtract(const Duration(days: 1)));
  final tomorrow = DateFormat('yyyy-MM-dd').format(DateTime.now().add(const Duration(days: 1)));

  if (receiptDate == today) {
    return translationNotifier.getText('todayOption') +
        ' ' +
        DateFormat('d MMMM yyyy', translationNotifier.languageCode).format(date);
  } else if (receiptDate == yesterday) {
    return translationNotifier.getText('yesterdayOption') +
        ' ' +
        DateFormat('d MMMM yyyy', translationNotifier.languageCode).format(date);
  } else if (receiptDate == tomorrow) {
    return translationNotifier.getText('tomorrowOption') +
        ' ' +
        DateFormat('d MMMM yyyy', translationNotifier.languageCode).format(date);
  } else {
    return DateFormat('d MMMM yyyy', translationNotifier.languageCode).format(date);
  }
}

String getDateStringNoLocale(DateTime date) {
  final receiptDate = DateFormat('dd-MM-yyyy').format(date);
  return receiptDate;
}

String getDateStringExpensesTitle(DateTime date, TranslationNotifier translationNotifier) {
  return isSameDay(date, DateTime.now())
      ? translationNotifier.getText('todayOption')
      : DateFormat("EEEE d MMMM", translationNotifier.localeString).format(date).capitalize();
}

String getDateStringOnboarding(DateTime date, TranslationNotifier translationNotifier) {
  return DateFormat("EEEE d MMMM yyyy", translationNotifier.localeString).format(date);
}

int daysBetween(DateTime from, DateTime to) {
  from = DateTime(from.year, from.month, from.day);
  to = DateTime(to.year, to.month, to.day);
  return (to.difference(from).inHours / 24).round();
}

extension DateTimeExtension on DateTime {
  DateTime next(int day) {
    return this.add(
      Duration(
        days: (day - this.weekday) % DateTime.daysPerWeek,
      ),
    );
  }
}

class StartEndDateObject {
  final DateTime startDate;
  final DateTime endDate;

  StartEndDateObject(this.startDate, this.endDate);
}
