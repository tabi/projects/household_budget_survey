import 'dart:io';

import 'package:flutter/widgets.dart';

late double x, y, f;

void initUIScalers(BuildContext context) {
  x = MediaQuery.of(context).size.width / 411;
  y = MediaQuery.of(context).size.height / 683;
  f = (x + y) / 2;
  if (Platform.isIOS) f = f * 0.9;
}
