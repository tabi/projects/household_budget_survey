import '../notifiers/translation_notifier.dart';

extension StringExtension on String {
  String capitalize() {
    if (this.length > 0)
      return "${this[0].toUpperCase()}${this.substring(1)}";
    else
      return '';
  }

  String currencyFormat(TranslationNotifier translationNotifier) {
    final currencySymbol = translationNotifier.getText('currencySymbol');
    if (translationNotifier.getText('currenncyPosition') == 'postfix') {
      return '${this.replaceAll('.', ',')} $currencySymbol';
    } else {
      return '$currencySymbol${this.replaceAll('.', ',')}';
    }
  }
}

extension DiacriticsAwareString on String {
  static const diacritics = 'àáâãäåòóôõöøèéêëðçìíîïùúûüñšÿýž';
  static const nonDiacritics = 'aaaaaaooooooeeeeeciiiiuuuunsyyz';

  String get withoutDiacriticalMarks => this.splitMapJoin('',
      onNonMatch: (char) =>
          char.isNotEmpty && diacritics.contains(char) ? nonDiacritics[diacritics.indexOf(char)] : char);
}

String normalizeCoicop(String coicop) {
  final values = coicop.split('.');
  return values.map((value) => int.parse(value).toString()).join('.');
}
