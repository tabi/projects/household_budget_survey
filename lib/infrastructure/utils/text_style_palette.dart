import 'package:flutter/material.dart';

import 'color_pallette.dart';

class TextStylePalette {
  static const TextStyle greyAkko11w400 =
      TextStyle(color: ColorPalette.textGrey, fontFamily: 'AkkoPro', fontSize: 11.0, fontWeight: FontWeight.w400);
  static const TextStyle greyAkko14w400 =
      TextStyle(color: ColorPalette.textGrey, fontFamily: 'AkkoPro', fontSize: 14.0, fontWeight: FontWeight.w400);
  static const TextStyle greyAkko16w400 =
      TextStyle(color: ColorPalette.textGrey, fontFamily: 'AkkoPro', fontSize: 16.0, fontWeight: FontWeight.w400);
  static const TextStyle greyAkko11w600 =
      TextStyle(color: ColorPalette.textGrey, fontFamily: 'AkkoPro', fontSize: 11.0, fontWeight: FontWeight.w600);
  static const TextStyle greyAkko14w600 =
      TextStyle(color: ColorPalette.textGrey, fontFamily: 'AkkoPro', fontSize: 14.0, fontWeight: FontWeight.w600);
  static const TextStyle greyAkko16w600 =
      TextStyle(color: ColorPalette.textGrey, fontFamily: 'AkkoPro', fontSize: 16.0, fontWeight: FontWeight.w600);

  static const TextStyle primaryAkko14w400 =
      TextStyle(color: ColorPalette.textPrimary, fontFamily: 'AkkoPro', fontSize: 14.0, fontWeight: FontWeight.w400);
  static const TextStyle primaryAkko14w500 =
      TextStyle(color: ColorPalette.textPrimary, fontFamily: 'AkkoPro', fontSize: 14.0, fontWeight: FontWeight.w500);
  static const TextStyle primaryAkko14w600 =
      TextStyle(color: ColorPalette.textPrimary, fontFamily: 'AkkoPro', fontSize: 14.0, fontWeight: FontWeight.w600);
  static const TextStyle primaryAkko16w400 =
      TextStyle(color: ColorPalette.textPrimary, fontFamily: 'AkkoPro', fontSize: 16.0, fontWeight: FontWeight.w400);
  static const TextStyle primaryAkko16w500 =
      TextStyle(color: ColorPalette.textPrimary, fontFamily: 'AkkoPro', fontSize: 16.0, fontWeight: FontWeight.w500);
  static const TextStyle primaryAkko16w600 =
      TextStyle(color: ColorPalette.textPrimary, fontFamily: 'AkkoPro', fontSize: 16.0, fontWeight: FontWeight.w600);
  static const TextStyle primarySoho16w400 =
      TextStyle(color: ColorPalette.textPrimary, fontFamily: 'SohoPro', fontSize: 16.0, fontWeight: FontWeight.w400);
  static const TextStyle primarySoho16w500 =
      TextStyle(color: ColorPalette.textPrimary, fontFamily: 'SohoPro', fontSize: 16.0, fontWeight: FontWeight.w500);
  static const TextStyle primarySoho16w600 =
      TextStyle(color: ColorPalette.textPrimary, fontFamily: 'SohoPro', fontSize: 16.0, fontWeight: FontWeight.w600);

  static const TextStyle primaryAkko16w400h15 = TextStyle(
      color: ColorPalette.textPrimary, fontFamily: 'AkkoPro', fontSize: 16.0, fontWeight: FontWeight.w400, height: 1.5);
  static const TextStyle primaryAkko16w500h15 = TextStyle(
      color: ColorPalette.textPrimary, fontFamily: 'AkkoPro', fontSize: 16.0, fontWeight: FontWeight.w500, height: 1.5);
  static const TextStyle primaryAkko16w600h15 = TextStyle(
      color: ColorPalette.textPrimary, fontFamily: 'AkkoPro', fontSize: 16.0, fontWeight: FontWeight.w600, height: 1.5);

  static const TextStyle brandAkko11w600 =
      TextStyle(color: ColorPalette.brand, fontFamily: 'AkkoPro', fontSize: 11.0, fontWeight: FontWeight.w600);
  static const TextStyle brandAkko16w500 =
      TextStyle(color: ColorPalette.brand, fontFamily: 'AkkoPro', fontSize: 16.0, fontWeight: FontWeight.w500);
  static const TextStyle brandAkko16w600 =
      TextStyle(color: ColorPalette.brand, fontFamily: 'AkkoPro', fontSize: 16.0, fontWeight: FontWeight.w600);

  static const TextStyle brandSoho16w500 =
      TextStyle(color: ColorPalette.brand, fontFamily: 'SohoPro', fontSize: 16.0, fontWeight: FontWeight.w500);
  static const TextStyle brandSoho16w600 =
      TextStyle(color: ColorPalette.brand, fontFamily: 'SohoPro', fontSize: 16.0, fontWeight: FontWeight.w600);
  static const TextStyle brandSoho20w500 =
      TextStyle(color: ColorPalette.brand, fontFamily: 'SohoPro', fontSize: 20.0, fontWeight: FontWeight.w500);
  static const TextStyle brandSoho20w600 =
      TextStyle(color: ColorPalette.brand, fontFamily: 'SohoPro', fontSize: 20.0, fontWeight: FontWeight.w600);
  static const TextStyle brandSoho26w500 =
      TextStyle(color: ColorPalette.brand, fontFamily: 'SohoPro', fontSize: 26.0, fontWeight: FontWeight.w500);
  static const TextStyle brandSoho26w600 =
      TextStyle(color: ColorPalette.brand, fontFamily: 'SohoPro', fontSize: 26.0, fontWeight: FontWeight.w600);

  static const TextStyle aquaAkko14w400 =
      TextStyle(color: ColorPalette.aqua300, fontFamily: 'AkkoPro', fontSize: 14.0, fontWeight: FontWeight.w400);
  static const TextStyle aquaAkko16w400 =
      TextStyle(color: ColorPalette.aqua300, fontFamily: 'AkkoPro', fontSize: 16.0, fontWeight: FontWeight.w400);
  static const TextStyle aquaAkko14w400underlined = TextStyle(
    color: ColorPalette.aqua300,
    fontFamily: 'AkkoPro',
    fontSize: 14.0,
    fontWeight: FontWeight.w400,
    decoration: TextDecoration.underline,
  );
  static const TextStyle aquaAkko16w400underlined = TextStyle(
    color: ColorPalette.aqua300,
    fontFamily: 'AkkoPro',
    fontSize: 16.0,
    fontWeight: FontWeight.w400,
    decoration: TextDecoration.underline,
  );

  static const TextStyle whiteAkko16w400 =
      TextStyle(color: ColorPalette.textWhite, fontFamily: 'AkkoPro', fontSize: 16.0, fontWeight: FontWeight.w400);
  static const TextStyle whiteSoho20w600 =
      TextStyle(color: ColorPalette.textWhite, fontFamily: 'SohoPro', fontSize: 20.0, fontWeight: FontWeight.w600);
  static const TextStyle whiteSoho12w400 = TextStyle(
    fontFamily: 'SohoPro',
    fontSize: 12,
    color: Colors.white,
  );

  static const TextStyle blackAkko16w400 =
      TextStyle(color: ColorPalette.textBlack, fontFamily: 'AkkoPro', fontSize: 16.0, fontWeight: FontWeight.w400);
  static const TextStyle blackAkko16w500 =
      TextStyle(color: ColorPalette.textBlack, fontFamily: 'AkkoPro', fontSize: 16.0, fontWeight: FontWeight.w500);
  static const TextStyle blackAkko16w600 =
      TextStyle(color: ColorPalette.textBlack, fontFamily: 'AkkoPro', fontSize: 16.0, fontWeight: FontWeight.w600);
  static const TextStyle blackAkko20w400 =
      TextStyle(color: ColorPalette.textBlack, fontFamily: 'AkkoPro', fontSize: 20.0, fontWeight: FontWeight.w400);
  static const TextStyle blackAkko20w500 =
      TextStyle(color: ColorPalette.textBlack, fontFamily: 'AkkoPro', fontSize: 20.0, fontWeight: FontWeight.w500);
  static const TextStyle blackAkko20w600 =
      TextStyle(color: ColorPalette.textBlack, fontFamily: 'AkkoPro', fontSize: 20.0, fontWeight: FontWeight.w600);

  static const TextStyle blackAkko16w400h15 = TextStyle(
      color: ColorPalette.textBlack, fontFamily: 'AkkoPro', fontSize: 16.0, fontWeight: FontWeight.w400, height: 1.5);
  static const TextStyle blackAkko16w500h15 = TextStyle(
      color: ColorPalette.textBlack, fontFamily: 'AkkoPro', fontSize: 16.0, fontWeight: FontWeight.w500, height: 1.5);
  static const TextStyle blackAkko16w600h15 = TextStyle(
      color: ColorPalette.textBlack, fontFamily: 'AkkoPro', fontSize: 16.0, fontWeight: FontWeight.w600, height: 1.5);

  static const TextStyle blackSoho16w400 =
      TextStyle(color: ColorPalette.textBlack, fontFamily: 'SohoPro', fontSize: 16.0, fontWeight: FontWeight.w400);
  static const TextStyle blackSoho16w500 =
      TextStyle(color: ColorPalette.textBlack, fontFamily: 'SohoPro', fontSize: 16.0, fontWeight: FontWeight.w500);
  static const TextStyle blackSoho16w600 =
      TextStyle(color: ColorPalette.textBlack, fontFamily: 'SohoPro', fontSize: 16.0, fontWeight: FontWeight.w600);

  static const TextStyle redAkko16w400 =
      TextStyle(color: ColorPalette.warningRed, fontFamily: 'AkkoPro', fontSize: 16.0, fontWeight: FontWeight.w400);
}
