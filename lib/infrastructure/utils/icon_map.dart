import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'color_pallette.dart';
import 'ui_scalers.dart';

class IconMap {
  static Map<String, List<dynamic>> icon = {
    'Benzinestation': <dynamic>[Icons.local_gas_station, ColorPalette.warningOrange, false],
    'Warenhuis (V&D, Hema, e.d.)': <dynamic>[FontAwesomeIcons.building, ColorPalette.warningOrange, true],
    'Warenhuis': <dynamic>[FontAwesomeIcons.building, ColorPalette.warningOrange, true],
    'Winkel met uitgebreid assortiment (Blokker, Xenos, e.d.)': <dynamic>[
      FontAwesomeIcons.bagShopping,
      ColorPalette.warningOrange,
      true
    ],
    'Supermarkt': <dynamic>[FontAwesomeIcons.cartShopping, ColorPalette.warningOrange, true],
    'Bakker / banketbakker': <dynamic>[FontAwesomeIcons.breadSlice, ColorPalette.warningOrange, true],
    'Groenten- en fruitwinkel': <dynamic>[FontAwesomeIcons.carrot, ColorPalette.warningOrange, true],
    'Slagerij / poelier': <dynamic>[FontAwesomeIcons.drumstickBite, ColorPalette.warningOrange, true],
    'Viswinkel': <dynamic>[FontAwesomeIcons.fish, ColorPalette.warningOrange, true],
    'Zuivelwinkel': <dynamic>[FontAwesomeIcons.cheese, ColorPalette.warningOrange, true],
    'Slijterij, drankhandel': <dynamic>[FontAwesomeIcons.wineBottle, ColorPalette.warningOrange, true],
    'Slijterij': <dynamic>[FontAwesomeIcons.wineBottle, ColorPalette.warningOrange, true],
    'Overige winkels voor levensmiddelen (gespecialiseerd)': <dynamic>[Icons.store, ColorPalette.warningOrange, false],
    'Tabakswinkel': <dynamic>[FontAwesomeIcons.smoking, ColorPalette.warningOrange, true],
    'Kledingwinkel, textielwinkels': <dynamic>[FontAwesomeIcons.shirt, ColorPalette.warningOrange, true],
    'Kledingwinkel of textielwinkels': <dynamic>[FontAwesomeIcons.shirt, ColorPalette.warningOrange, true],
    'Schoenenwinkel': <dynamic>[FontAwesomeIcons.shoePrints, ColorPalette.warningOrange, true],
    'Woninginrichting (meubelen, vloerbedekking, gordijnen, e.d.)': <dynamic>[
      FontAwesomeIcons.couch,
      ColorPalette.warningOrange,
      true
    ],
    'Winkel voor verlichting': <dynamic>[FontAwesomeIcons.lightbulb, ColorPalette.warningOrange, true],
    'Elektronicawinkel, witgoedwinkel': <dynamic>[Icons.tv, ColorPalette.warningOrange, false],
    'Elektronicawinkel of witgoedwinkel': <dynamic>[Icons.tv, ColorPalette.warningOrange, false],
    'Computerwinkel': <dynamic>[Icons.computer, ColorPalette.warningOrange, false],
    'Huishoudelijke artikelen': <dynamic>[FontAwesomeIcons.broom, ColorPalette.warningOrange, true],
    'Bouwmarkt': <dynamic>[FontAwesomeIcons.hammer, ColorPalette.warningOrange, true],
    'Bloemenwinkel, tuincentrum': <dynamic>[FontAwesomeIcons.seedling, ColorPalette.warningOrange, true],
    'Fiets- of bromfietswinkel': <dynamic>[FontAwesomeIcons.bicycle, ColorPalette.warningOrange, true],
    'Speelgoedwinkel': <dynamic>[Icons.gamepad, ColorPalette.warningOrange, false],
    'Foto- en filmzaak': <dynamic>[Icons.camera_alt, ColorPalette.warningOrange, false],
    'Optiekwinkel/brillenzaak, juwelier': <dynamic>[FontAwesomeIcons.glasses, ColorPalette.warningOrange, true],
    'Kantoor-/boekhandel, kiosk': <dynamic>[FontAwesomeIcons.book, ColorPalette.warningOrange, true],
    'Sportartikelen en buitenrecreatie': <dynamic>[FontAwesomeIcons.football, ColorPalette.warningOrange, true],
    'Apotheek': <dynamic>[FontAwesomeIcons.prescriptionBottleMedical, ColorPalette.warningOrange, true],
    'Drogisterij, parfumerie': <dynamic>[Icons.spa, ColorPalette.warningOrange, false],
    'Drogisterij of parfumerie': <dynamic>[Icons.spa, ColorPalette.warningOrange, false],
    'Souvenir-, cadeauwinkel': <dynamic>[FontAwesomeIcons.gifts, ColorPalette.warningOrange, true],
    'Overige winkels (niet voor levensmiddelen)': <dynamic>[Icons.store, ColorPalette.warningOrange, false],
    'Telefoonwinkel': <dynamic>[FontAwesomeIcons.mobileScreenButton, ColorPalette.warningOrange, true],
    'Platenzaak/cd-winkel': <dynamic>[FontAwesomeIcons.compactDisc, ColorPalette.warningOrange, true],
    'Garage': <dynamic>[FontAwesomeIcons.warehouse, ColorPalette.warningOrange, true],
    '2e hands gekocht bij bedrijf of winkel': <dynamic>[Icons.sync, ColorPalette.warningOrange, false],
    '2e hands gekocht van particulier': <dynamic>[Icons.sync, ColorPalette.warningOrange, false],
    'Winkel voor lederwaren/reisartikelen': <dynamic>[Icons.beach_access, ColorPalette.warningOrange, false],
    'Antiekwinkel': <dynamic>[FontAwesomeIcons.chessKnight, ColorPalette.warningOrange, true],
    'Doe-het-zelfwinkel': <dynamic>[FontAwesomeIcons.screwdriverWrench, ColorPalette.warningOrange, true],
    'Winkel voor ijzerwaren/verf/hout e.d.': <dynamic>[FontAwesomeIcons.fillDrip, ColorPalette.warningOrange, true],
    'Dierenwinkel': <dynamic>[Icons.pets, ColorPalette.warningOrange, false],
    'Markt, braderie, op straat gekocht': <dynamic>[Icons.shopping_basket, ColorPalette.warningOrange, false],
    'Postorderbedrijf': <dynamic>[FontAwesomeIcons.cube, ColorPalette.warningOrange, true],
    'Aan de deur, rijdende winkel': <dynamic>[FontAwesomeIcons.truck, ColorPalette.warningOrange, true],
    'Groothandel': <dynamic>[FontAwesomeIcons.cubes, ColorPalette.warningOrange, true],
    'Boer of tuinder': <dynamic>[FontAwesomeIcons.tractor, ColorPalette.warningOrange, true],
    'Bedrijfskantine': <dynamic>[Icons.business, ColorPalette.warningOrange, false],
    'Sportkantine': <dynamic>[Icons.local_dining, ColorPalette.warningOrange, false],
    'Horeca (hotel, café, restaurant, ijssalon, snackbar, e.d.)': <dynamic>[
      Icons.restaurant,
      ColorPalette.warningOrange,
      false
    ],
    'Horeca': <dynamic>[Icons.restaurant, ColorPalette.warningOrange, false],
    'Alle overige aankopen en diensten': <dynamic>[Icons.category, ColorPalette.warningOrange, false],
    'Verkocht': <dynamic>[Icons.person, ColorPalette.warningOrange, false],
    'Gift': <dynamic>[FontAwesomeIcons.gift, ColorPalette.warningOrange, true],
    'Default': <dynamic>[FontAwesomeIcons.store, ColorPalette.warningOrange, false],
  };

  static Icon getStoreIcon(String? storeType) {
    if (IconMap.icon.containsKey(storeType)) {
      return Icon(icon[storeType]![0] as IconData, color: Colors.white, size: 16 * x);
    }
    return Icon(Icons.question_mark_rounded, size: 25 * x, color: Colors.white);
  }
}
