import 'dart:convert';

import 'package:csv/csv.dart';
import 'package:flutter/services.dart';

Future<List<String>> getCsvFiles(String folderPath) async {
  final fileNames = <String>[];
  final manifestContent = await rootBundle.loadString('AssetManifest.json');
  final manifestMap = json.decode(manifestContent) as Map<String, dynamic>;
  manifestMap.keys.forEach((String key) {
    if (key.contains('assets/translations/') && key.endsWith('.csv')) fileNames.add(key);
  });
  return fileNames;
}

Future<List<dynamic>> getCsvRows(String filePath) async {
  final csvString = await rootBundle.loadString(filePath);
  final rows = const CsvToListConverter().convert<dynamic>(csvString, fieldDelimiter: ';', eol: '\n');
  return rows;
}
