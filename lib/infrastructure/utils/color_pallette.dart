import 'package:flutter/material.dart';

class ColorPalette {
  //General
  static const Color aqua200 = Color(0xFF0580A1); //Aqua Midden
  static const Color aqua300 = Color(0xFF005470); //Aqua Donker
  static const Color grey100 = Color(0xFFE9E9E9);
  static const Color grey200 = Color(0xFFBDBCBC);
  static const Color grey300 = Color(0xFF878787);

  static const Color brand = Color(0xFF271D6C);

  //Text
  static const Color textWhite = Color(0xFFFFFFFF);
  static const Color textGrey = Color(0xFF6E6E6E);
  static const Color textBlack = Color(0xFF000000);
  static const Color textPrimary = Color(0xFF091D23); //Primair Zwart

  //Buttons

  //Warnings
  static const Color warningOrange = Color(0xFFDA5914);
  static const Color warningOrangeOpacity = Color(0x1ADA5914);

  static const Color warningRed = Color(0xFFC90C0F); //Rood midden
  static const Color warningRedOpacity = Color(0x1AC90C0F);

  static const Color green200 = Color(0xFF53A31D);
  static const Color greenOpacity = Color(0x1A53A31D);

  //Page
  static const Color backgroundOffWhite = Color(0xFFF8F8F8);
  static const Color backgroundWhite = Color(0xFFFFFFFF);
  static const Color backgroundBlack = Color(0xFF000000);
}
