import 'package:get_it/get_it.dart';
import 'package:household_budget_survey/infrastructure/network/sync_api.dart';

import 'infrastructure/database/database.dart';
import 'infrastructure/network/auth_api.dart';
import 'infrastructure/network/receipt_api.dart';
import 'infrastructure/network/survey_day_api.dart';
import 'infrastructure/notifiers/app_tour_notifier.dart';
import 'infrastructure/notifiers/auth_notifier.dart';
import 'infrastructure/notifiers/configuration_notifier.dart';
import 'infrastructure/notifiers/filter_notifier.dart';
import 'infrastructure/notifiers/login_notifier.dart';
import 'infrastructure/notifiers/onboarding_notifier.dart';
import 'infrastructure/notifiers/overview_notifiers/overview_notifier.dart';
import 'infrastructure/notifiers/overview_notifiers/surveylist_notifier.dart';
import 'infrastructure/notifiers/receipt_list_notifier.dart';
import 'infrastructure/notifiers/settings_notifier.dart';
import 'infrastructure/notifiers/translation_notifier.dart';
import 'infrastructure/repositories/category_search_repository.dart';
import 'infrastructure/repositories/device_repository.dart';
import 'infrastructure/repositories/product_repository.dart';
import 'infrastructure/repositories/product_search_repository.dart';
import 'infrastructure/repositories/receipt_repository.dart';
import 'infrastructure/repositories/shop_search_repository.dart';
import 'infrastructure/repositories/survey_day_repository.dart';
import 'infrastructure/repositories/sync_repository.dart';
import 'infrastructure/services/device_service.dart';
import 'infrastructure/services/string_match_service.dart';

void registerDependencies() {
  // Database
  GetIt.I.registerSingleton<Database>(Database());

  // Repositories
  GetIt.I.registerSingleton<ReceiptApi>(ReceiptApi(GetIt.I<Database>()));
  GetIt.I.registerSingleton<SyncApi>(SyncApi(GetIt.I<Database>()));
  GetIt.I.registerSingleton<SyncRepository>(SyncRepository(GetIt.I<SyncApi>()));
  GetIt.I.registerSingleton<ProductSearchRepository>(ProductSearchRepository(GetIt.I<Database>()));
  GetIt.I.registerSingleton<ShopSearchRepository>(ShopSearchRepository(GetIt.I<Database>()));
  GetIt.I.registerSingleton<CategorySearchRepository>(CategorySearchRepository(GetIt.I<Database>()));
  GetIt.I.registerSingleton<ReceiptRepository>(ReceiptRepository(GetIt.I<Database>(), GetIt.I<SyncRepository>(), GetIt.I<ReceiptApi>(), GetIt.I<SyncApi>()));
  GetIt.I.registerSingleton<ProductRepository>(ProductRepository(GetIt.I<Database>()));
  GetIt.I.registerSingleton<SurveyDayRepository>(SurveyDayRepository(GetIt.I<Database>(), SurveyDayApi(GetIt.I<Database>())));
  GetIt.I.registerSingleton<DeviceRepository>(DeviceRepository(GetIt.I<Database>(), DeviceService()));

  // Notifiers
  GetIt.I.registerSingleton<ConfigurationNotifier>(ConfigurationNotifier());
  GetIt.I.registerSingleton<TranslationNotifier>(TranslationNotifier());
  GetIt.I.registerSingleton<SettingsNotifier>(SettingsNotifier());
  GetIt.I.registerSingleton<OverviewNotifier>(OverviewNotifier(GetIt.I<SurveyDayRepository>()));
  GetIt.I.registerSingleton<OnboardingNotifier>(OnboardingNotifier(GetIt.I<SurveyDayRepository>()));
  GetIt.I.registerSingleton<LoginNotifier>(LoginNotifier(AuthApi(GetIt.I<Database>()), GetIt.I<DeviceRepository>(), GetIt.I<SyncRepository>()));
  GetIt.I.registerSingleton<AppTourNotifier>(AppTourNotifier());
  GetIt.I.registerSingleton<AuthNotifier>(AuthNotifier(GetIt.I<Database>()));
  GetIt.I.registerSingleton<FilterNotifier>(FilterNotifier());
  GetIt.I.registerSingleton<SurveyListNotifier>(SurveyListNotifier());
  GetIt.I.registerSingleton<ReceiptListNotifier>(ReceiptListNotifier(GetIt.I<ReceiptRepository>()));

  // Services
  GetIt.I.registerSingleton<StringMatchingService>(StringMatchingService(GetIt.I<ConfigurationNotifier>()));
  GetIt.I.registerSingleton<DeviceService>(DeviceService());
}
